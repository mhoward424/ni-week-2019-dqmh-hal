﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="16008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="DMM_Agilent 34410A.lvlib" Type="Library" URL="../Agilent 34410A_Class/DMM_Agilent 34410A.lvlib"/>
		<Item Name="DMM_Agilent 34401A.lvlib" Type="Library" URL="../Agilent 34401A_Class/DMM_Agilent 34401A.lvlib"/>
		<Item Name="DMM_PXI-4072.lvlib" Type="Library" URL="../PXI-4072_Class/DMM_PXI-4072.lvlib"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="DMM.lvlibp" Type="LVLibp" URL="../../../../Dev Components/DMM.lvlibp">
				<Item Name="Demo" Type="Folder">
					<Item Name="DMM_Demo.vi" Type="VI" URL="../../../../Dev Components/DMM.lvlibp/DMM_Demo.vi"/>
				</Item>
				<Item Name="Helper Functions" Type="Folder">
					<Item Name="GetDMMClassFromPath.vi" Type="VI" URL="../../../../Dev Components/DMM.lvlibp/GetDMMClassFromPath.vi"/>
				</Item>
				<Item Name="DMM.lvclass" Type="LVClass" URL="../../../../Dev Components/DMM.lvlibp/DMM.lvclass"/>
				<Item Name="DialogType.ctl" Type="VI" URL="../../../../Dev Components/DMM.lvlibp/1abvi3w/vi.lib/Utility/error.llb/DialogType.ctl"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="../../../../Dev Components/DMM.lvlibp/1abvi3w/vi.lib/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="../../../../Dev Components/DMM.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="../../../../Dev Components/DMM.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Set String Value.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="../../../../Dev Components/DMM.lvlibp/1abvi3w/vi.lib/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="../../../../Dev Components/DMM.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="../../../../Dev Components/DMM.lvlibp/1abvi3w/vi.lib/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="../../../../Dev Components/DMM.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="../../../../Dev Components/DMM.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="../../../../Dev Components/DMM.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="../../../../Dev Components/DMM.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="../../../../Dev Components/DMM.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Format Message String.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="../../../../Dev Components/DMM.lvlibp/1abvi3w/vi.lib/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="../../../../Dev Components/DMM.lvlibp/1abvi3w/vi.lib/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="../../../../Dev Components/DMM.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="../../../../Dev Components/DMM.lvlibp/1abvi3w/vi.lib/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="../../../../Dev Components/DMM.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="../../../../Dev Components/DMM.lvlibp/1abvi3w/vi.lib/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="../../../../Dev Components/DMM.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="../../../../Dev Components/DMM.lvlibp/1abvi3w/vi.lib/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="../../../../Dev Components/DMM.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="../../../../Dev Components/DMM.lvlibp/1abvi3w/vi.lib/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="../../../../Dev Components/DMM.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="../../../../Dev Components/DMM.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="../../../../Dev Components/DMM.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="../../../../Dev Components/DMM.lvlibp/1abvi3w/vi.lib/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="../../../../Dev Components/DMM.lvlibp/1abvi3w/vi.lib/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="../../../../Dev Components/DMM.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="../../../../Dev Components/DMM.lvlibp/1abvi3w/vi.lib/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="../../../../Dev Components/DMM.lvlibp/1abvi3w/vi.lib/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="../../../../Dev Components/DMM.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../../../../Dev Components/DMM.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Get LV Class Default Value.vi" Type="VI" URL="../../../../Dev Components/DMM.lvlibp/1abvi3w/vi.lib/Utility/LVClass/Get LV Class Default Value.vi"/>
			</Item>
			<Item Name="System" Type="VI" URL="System">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="nidmm_32.dll" Type="Document" URL="nidmm_32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="DMM_niDMM.lvlibp" Type="LVLibp" URL="../../../../Dev Components/DMM_niDMM.lvlibp">
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../../../../Dev Components/DMM_niDMM.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="niDMM Auto Zero.ctl" Type="VI" URL="../../../../Dev Components/DMM_niDMM.lvlibp/1abvi3w/instr.lib/niDMM/nidmm.llb/niDMM Auto Zero.ctl"/>
				<Item Name="niDMM Close.vi" Type="VI" URL="../../../../Dev Components/DMM_niDMM.lvlibp/1abvi3w/instr.lib/niDMM/nidmm.llb/niDMM Close.vi"/>
				<Item Name="niDMM Configure Auto Zero.vi" Type="VI" URL="../../../../Dev Components/DMM_niDMM.lvlibp/1abvi3w/instr.lib/niDMM/nidmm.llb/niDMM Configure Auto Zero.vi"/>
				<Item Name="niDMM Configure Measurement Digits.vi" Type="VI" URL="../../../../Dev Components/DMM_niDMM.lvlibp/1abvi3w/instr.lib/niDMM/nidmm.llb/niDMM Configure Measurement Digits.vi"/>
				<Item Name="niDMM Configure Multi Point.vi" Type="VI" URL="../../../../Dev Components/DMM_niDMM.lvlibp/1abvi3w/instr.lib/niDMM/nidmm.llb/niDMM Configure Multi Point.vi"/>
				<Item Name="niDMM Configure Offset Comp Ohms.vi" Type="VI" URL="../../../../Dev Components/DMM_niDMM.lvlibp/1abvi3w/instr.lib/niDMM/nidmm.llb/niDMM Configure Offset Comp Ohms.vi"/>
				<Item Name="niDMM Configure Trigger Slope.vi" Type="VI" URL="../../../../Dev Components/DMM_niDMM.lvlibp/1abvi3w/instr.lib/niDMM/nidmm.llb/niDMM Configure Trigger Slope.vi"/>
				<Item Name="niDMM Configure Trigger.vi" Type="VI" URL="../../../../Dev Components/DMM_niDMM.lvlibp/1abvi3w/instr.lib/niDMM/nidmm.llb/niDMM Configure Trigger.vi"/>
				<Item Name="niDMM Fetch Multi Point.vi" Type="VI" URL="../../../../Dev Components/DMM_niDMM.lvlibp/1abvi3w/instr.lib/niDMM/nidmm.llb/niDMM Fetch Multi Point.vi"/>
				<Item Name="niDMM Function To IVI Constant.vi" Type="VI" URL="../../../../Dev Components/DMM_niDMM.lvlibp/1abvi3w/instr.lib/niDMM/nidmm.llb/niDMM Function To IVI Constant.vi"/>
				<Item Name="niDMM Function.ctl" Type="VI" URL="../../../../Dev Components/DMM_niDMM.lvlibp/1abvi3w/instr.lib/niDMM/nidmm.llb/niDMM Function.ctl"/>
				<Item Name="niDMM Initialize.vi" Type="VI" URL="../../../../Dev Components/DMM_niDMM.lvlibp/1abvi3w/instr.lib/niDMM/nidmm.llb/niDMM Initialize.vi"/>
				<Item Name="niDMM Initiate.vi" Type="VI" URL="../../../../Dev Components/DMM_niDMM.lvlibp/1abvi3w/instr.lib/niDMM/nidmm.llb/niDMM Initiate.vi"/>
				<Item Name="niDMM IVI Error Converter.vi" Type="VI" URL="../../../../Dev Components/DMM_niDMM.lvlibp/1abvi3w/instr.lib/niDMM/nidmm.llb/niDMM IVI Error Converter.vi"/>
				<Item Name="niDMM Offset Comp Ohms.ctl" Type="VI" URL="../../../../Dev Components/DMM_niDMM.lvlibp/1abvi3w/instr.lib/niDMM/nidmm.llb/niDMM Offset Comp Ohms.ctl"/>
				<Item Name="niDMM Read Multi Point.vi" Type="VI" URL="../../../../Dev Components/DMM_niDMM.lvlibp/1abvi3w/instr.lib/niDMM/nidmm.llb/niDMM Read Multi Point.vi"/>
				<Item Name="niDMM Read.vi" Type="VI" URL="../../../../Dev Components/DMM_niDMM.lvlibp/1abvi3w/instr.lib/niDMM/nidmm.llb/niDMM Read.vi"/>
				<Item Name="niDMM Resolution in Digits.ctl" Type="VI" URL="../../../../Dev Components/DMM_niDMM.lvlibp/1abvi3w/instr.lib/niDMM/nidmm.llb/niDMM Resolution in Digits.ctl"/>
				<Item Name="niDMM Sample Trigger To IVI Constant.vi" Type="VI" URL="../../../../Dev Components/DMM_niDMM.lvlibp/1abvi3w/instr.lib/niDMM/nidmm.llb/niDMM Sample Trigger To IVI Constant.vi"/>
				<Item Name="niDMM Sample Trigger.ctl" Type="VI" URL="../../../../Dev Components/DMM_niDMM.lvlibp/1abvi3w/instr.lib/niDMM/nidmm.llb/niDMM Sample Trigger.ctl"/>
				<Item Name="niDMM Send Software Trigger.vi" Type="VI" URL="../../../../Dev Components/DMM_niDMM.lvlibp/1abvi3w/instr.lib/niDMM/nidmm.llb/niDMM Send Software Trigger.vi"/>
				<Item Name="niDMM Slope To IVI Constant.vi" Type="VI" URL="../../../../Dev Components/DMM_niDMM.lvlibp/1abvi3w/instr.lib/niDMM/nidmm.llb/niDMM Slope To IVI Constant.vi"/>
				<Item Name="niDMM Slope.ctl" Type="VI" URL="../../../../Dev Components/DMM_niDMM.lvlibp/1abvi3w/instr.lib/niDMM/nidmm.llb/niDMM Slope.ctl"/>
				<Item Name="niDMM Trigger Source To IVI Constant.vi" Type="VI" URL="../../../../Dev Components/DMM_niDMM.lvlibp/1abvi3w/instr.lib/niDMM/nidmm.llb/niDMM Trigger Source To IVI Constant.vi"/>
				<Item Name="niDMM Trigger.ctl" Type="VI" URL="../../../../Dev Components/DMM_niDMM.lvlibp/1abvi3w/instr.lib/niDMM/nidmm.llb/niDMM Trigger.ctl"/>
				<Item Name="niDMM.lvclass" Type="LVClass" URL="../../../../Dev Components/DMM_niDMM.lvlibp/niDMM.lvclass"/>
			</Item>
			<Item Name="DMM_Agilent.lvlibp" Type="LVLibp" URL="../../../../Dev Components/DMM_Agilent.lvlibp">
				<Item Name="Agilent 34401.lvlib" Type="Library" URL="../../../../Dev Components/DMM_Agilent.lvlibp/1abvi3w/vi.lib/idnet/Agilent 34401/Agilent 34401.lvlib"/>
				<Item Name="Agilent.lvclass" Type="LVClass" URL="../../../../Dev Components/DMM_Agilent.lvlibp/Agilent.lvclass"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../../../../Dev Components/DMM_Agilent.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
			</Item>
			<Item Name="HAL Utility.lvlibp" Type="LVLibp" URL="../../../../Dev Components/HAL Utility.lvlibp">
				<Item Name="HAL Utility.lvclass" Type="LVClass" URL="../../../../Dev Components/HAL Utility.lvlibp/HAL Utility.lvclass"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="../../../../Dev Components/HAL Utility.lvlibp/1abvi3w/vi.lib/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="Agilent 34410A Packed Library" Type="Packed Library">
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{4C923804-4FFD-4F27-8233-FF36200990F7}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Agilent 34410A Packed Library</Property>
				<Property Name="Bld_excludeDependentPPLs" Type="Bool">true</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/C/Dev Components</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{54908B7F-6898-4EA7-B62A-B6A9D3427866}</Property>
				<Property Name="Bld_version.build" Type="Int">8</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">DMM_34410A.lvlibp</Property>
				<Property Name="Destination[0].path" Type="Path">/C/Dev Components/DMM_34410A.lvlibp</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/C/Dev Components</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="PackedLib_callersAdapt" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{2023794D-273A-4F07-8091-41B0D7C56F7F}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/DMM_Agilent 34410A.lvlib</Property>
				<Property Name="Source[1].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[1].Library.atomicCopy" Type="Bool">true</Property>
				<Property Name="Source[1].Library.LVLIBPtopLevel" Type="Bool">true</Property>
				<Property Name="Source[1].preventRename" Type="Bool">true</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">Library</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_enableDebugging" Type="Bool">true</Property>
				<Property Name="TgtF_fileDescription" Type="Str">Agilent 34410A Packed Library</Property>
				<Property Name="TgtF_internalName" Type="Str">Agilent 34410A Packed Library</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2017</Property>
				<Property Name="TgtF_productName" Type="Str">Agilent 34410A Packed Library</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{806032F4-08AF-4BB1-9701-909AFC910FE6}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">DMM_34410A.lvlibp</Property>
			</Item>
			<Item Name="Agilent 34401A Packed Library" Type="Packed Library">
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{0302A915-867E-42B1-BABF-6E141E4DA8D0}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Agilent 34401A Packed Library</Property>
				<Property Name="Bld_excludeDependentPPLs" Type="Bool">true</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/C/Dev Components</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{0FC4904D-4835-4384-BC07-09D72D0D5DB2}</Property>
				<Property Name="Bld_version.build" Type="Int">9</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">DMM_34401A.lvlibp</Property>
				<Property Name="Destination[0].path" Type="Path">/C/Dev Components/DMM_34401A.lvlibp</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/C/Dev Components</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="PackedLib_callersAdapt" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{2023794D-273A-4F07-8091-41B0D7C56F7F}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/DMM_Agilent 34410A.lvlib</Property>
				<Property Name="Source[1].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[1].type" Type="Str">Library</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/DMM_Agilent 34401A.lvlib</Property>
				<Property Name="Source[2].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[2].Library.atomicCopy" Type="Bool">true</Property>
				<Property Name="Source[2].Library.LVLIBPtopLevel" Type="Bool">true</Property>
				<Property Name="Source[2].preventRename" Type="Bool">true</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[2].type" Type="Str">Library</Property>
				<Property Name="SourceCount" Type="Int">3</Property>
				<Property Name="TgtF_enableDebugging" Type="Bool">true</Property>
				<Property Name="TgtF_fileDescription" Type="Str">Agilent 34410A Packed Library</Property>
				<Property Name="TgtF_internalName" Type="Str">Agilent 34410A Packed Library</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2017</Property>
				<Property Name="TgtF_productName" Type="Str">Agilent 34410A Packed Library</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{BC4C4118-B331-4FE2-A7AA-427C22E66FD7}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">DMM_34401A.lvlibp</Property>
			</Item>
			<Item Name="DMM_PXI-4072 Packed Library" Type="Packed Library">
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{F6A9C776-AF5F-4BC4-A1FA-699678F3476A}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">DMM_PXI-4072 Packed Library</Property>
				<Property Name="Bld_excludeDependentPPLs" Type="Bool">true</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/C/Dev Components</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{9B6489E2-028E-42BA-AA4C-3725DB39B5A9}</Property>
				<Property Name="Bld_version.build" Type="Int">9</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">DMM_PXI-4072.lvlibp</Property>
				<Property Name="Destination[0].path" Type="Path">/C/Dev Components/DMM_PXI-4072.lvlibp</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/C/Dev Components</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="PackedLib_callersAdapt" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{2023794D-273A-4F07-8091-41B0D7C56F7F}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/DMM_Agilent 34410A.lvlib</Property>
				<Property Name="Source[1].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[1].type" Type="Str">Library</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/DMM_Agilent 34401A.lvlib</Property>
				<Property Name="Source[2].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[2].type" Type="Str">Library</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/DMM_PXI-4072.lvlib</Property>
				<Property Name="Source[3].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[3].Library.atomicCopy" Type="Bool">true</Property>
				<Property Name="Source[3].Library.LVLIBPtopLevel" Type="Bool">true</Property>
				<Property Name="Source[3].preventRename" Type="Bool">true</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[3].type" Type="Str">Library</Property>
				<Property Name="SourceCount" Type="Int">4</Property>
				<Property Name="TgtF_enableDebugging" Type="Bool">true</Property>
				<Property Name="TgtF_fileDescription" Type="Str">Agilent 34410A Packed Library</Property>
				<Property Name="TgtF_internalName" Type="Str">Agilent 34410A Packed Library</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2017</Property>
				<Property Name="TgtF_productName" Type="Str">Agilent 34410A Packed Library</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{1F8EA276-9F3C-4066-99E6-4D336C28A4E9}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">DMM_PXI-4072.lvlibp</Property>
			</Item>
		</Item>
	</Item>
</Project>
