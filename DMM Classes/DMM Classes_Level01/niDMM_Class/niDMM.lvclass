﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="16008000">
	<Property Name="EndevoGOOP_ClassItemIcon" Type="Str">BrownFull</Property>
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">13290186</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorProtected" Type="UInt">16766041</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">16754986</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.ContainingLib" Type="Str">niDMM.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../niDMM.lvlib</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,E!!!*Q(C=T:3R&lt;B."%)&lt;`3SB3)##]!%H;.%D4)CK`1FR$-[]1+H,OL)AKX5AUN.;^A2PIZQW1B@)#;2%&amp;G/`/;TPH*(9$5H9^PLP`HZX^P,M_K7M(UB0.&gt;\84L7XFT^P\?8=F&amp;N_X`:,3#KP0UC`D&amp;P[NPP)84`.VZ8\^J&lt;^D`PPZ&gt;\587^OG,WXT.2K/2HL)RWW2?BFLP\A&lt;'3N`\3YS.PVBTW`O_K.VB;:JBP@Y*;.J(P4*'$6&lt;`7)`0H`5]\NL@`W(QZ80TVSE^@&gt;PO0"PO:P\XW7=.GP\TPE:^NS\0OW7?[`@;TO0.__"?P?@Y0]WK6,\)C+3#-)*5WP.%TX2%TX2%TX2!TX1!TX1!TX1(&gt;X2(&gt;X2(&gt;X2$&gt;X1$&gt;X1$&gt;X1OYYO&gt;+%,H66**E]G3IIG":*E5*2]*4Q*4]+4]0#IB#@B38A3HI3(&amp;#5]#5`#E`!E0!R4QJ0Q*$Q*4]*$K9YEOYY/4]*$?15]!5`!%`!%0%SJA#=!##9,#A&gt;&amp;Q&amp;"A"D="4]!4](#LA#@A#8A#HI!(7Q&amp;0Q"0Q"$Q"$U/[69G/JOXI]&amp;"'$I`$Y`!Y0!Y0J?8Q/$Q/D]0D]$#&gt;("[(RY&amp;Q*H3+AS"HE*0A0$A]$A]8/4Q/D]0D]$A]7.U/?&lt;=S,5X&lt;U?%R?!Q?A]@A-8AI)90(Y$&amp;Y$"[$B\)S?!Q?A]@A-8C93A;0Q70Q'#$'J%QPIZARU%AS")/(4X&gt;;L.OF[%CM[V)ZP#K(5O7QK2QCF=/BMOEKG[GS33K,L\+I+IOFMAAK0UY&amp;7A6':2+6Q7WC&lt;PC?%6.C1IS*=_+-'"!HR'%\^"]HXNT=;$;&lt;;4K&gt;;D+:;$Q?[`T]8'&gt;H:RI-"DIZ/&gt;(BY;'7\9C_;.8SP62T8X__0+Z@@H``[7LSZ@,K]PCS@5:@_7XM@;XKA^&gt;6@@3UKON^./,&gt;M[L_],;K0`[J[IN\9D[`LF`^`F5``XF&gt;\``Y6OO#=7_K:?X(]7\5HB&lt;8V2L^"706&amp;WI!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">369131520</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"EW5F.31QU+!!.-6E.$4%*76Q!!&amp;/1!!!23!!!!)!!!&amp;-1!!!!?!!!!!ANO;52.43ZM&gt;GRJ9AVO;52.43ZM&gt;G.M98.T!!!!!!#A&amp;A#!!!!Q!!!)!!1!!!!!"!!$!$Q!P!!@1)!#!!!!!!%!!1!'`````Q!!!!!!!!!!!!!!!(A&gt;Z6P04[&gt;"MHEY\Z9&gt;I&lt;)!!!!-!!!!%!!!!!!ZN5L7*MU02&lt;!L"0LI0-BQV"W-W9]!MA4JA!G9\0B#@A!!%!!!!!!!RO69&lt;F9.ZEW22M\E$A%0JQ%!!!$`````V"W-W9]!MA4JA!G9\0B#@A!!!"#'?#^=6WS(7?J&amp;]?H/#3A+!!!!"!!!!!!!!!!H!!&amp;-6E.$!!!!!1!#6EF-1A!!!!"16%AQ!!!!"1!"!!%!!!!!!A!$!!!!!!)!!1!!!!!!*Q!!!#2YH'0A:'"K9,D!!-3-1-T5Q01$S0Y!YD-)=)")"A9!M2)*I1!!!!")!!!"'(C=9W$!"0_"!%AR-D!Q81$3,'DC9"L'JC&lt;!:3YOO[$CT&amp;!XME*%'9&amp;C4(O!$#;1(&amp;1.V!^-:Y$Y",IZT&amp;D-"A"`[3AC!!!!$!!"6EF%5Q!!!!!!!Q!!!:%!!!,A?*S&lt;Q-D!E'FM9@9!3$-$M2B$!U.S@EIK&amp;Q/1TQ!"*ZA93!9"50V;;/+'"Q[H!9%?PXQ,G.`]BK@&lt;257AO5:&amp;AKF5J.N(2;442Y7FEU8FR:````]X(_%ZX/W2=^T2"K3WGQ-I@NR&amp;B10%!&gt;)M)0J`9!:)&amp;=S]!+"J(!U6SAQF,)9(IAY@&lt;T"BB&amp;A--T)+X8Y"I![:\B#AR5US1"=$H;$1W;5!:%%&gt;!L3)"WIB"]AMM#GN"`CH(/$@&gt;G)(3)4@^7$T%1'A%VR!ZH%!42!Y_*#FOV%$+.=\%51#B8A[1TAEDLNQ[)A"_9QH/I(/\_3"_:)$\PYQE!%F+A+&gt;*C#,7?!7&gt;L-&gt;&gt;^!!/].""%*F1+A+#&amp;5!IH;!@83%)_YQ0,T8PL[X#R2/S&amp;&amp;I!-1A^8I-D!S-9$F'BFKIH!W1T116A]5&gt;C#U!$59B*$UQ7T31R'$"&lt;1#V%S4T"KI/R0Y%:90M:Y/+W1,V4)#S89$M"#D&lt;']B_!'5(!&gt;E#5(9EE+X!#'((1&gt;H/`C[OS/E-FG9"*&amp;+#3A!!!!!!!!Q7!)!1!!!%-49O-!!!!!!-&amp;A#!!!!!"$%W,D!!!!!!$"9!A"!!!!1R.CYQ!!!!!!Q7!)!!!!!%-49O-!!!!!!-&amp;A#!%!!!"$%W,D!!!!!!&amp;!%!!!$V6T7#?3;CD#ZT5EY'34G&gt;!!!!$1!!!!!!!!!!!!!!!!!!!!!!!!#!`````Y!!!!'!H)IBA",&lt;99S3KK'+EIIBCJS+)9!!!!(`````A!!!!9!!!!'!!M!"A!AQ!9!A$!'!A!-"A!!$!9"A$A'!?$Y"A(\_!9"``A'!@`Y"A(`_!9"``A'!@`Y"A0``Y9!@`@G!$`@BA!0@Q9!!@Q'!!$Q"A!!!!@````]!!!)!``````````````````````T-T-T-T-T-T-T-T-T-T-`]T-T-`-``T0T-`0T-`-T0`-T-T-T0T0T`T`T`T`T-T`T-`]T]T]T]`0T]`0T]T-`]T0T]`-`-`0T-`0T-`-T0`-T]`0T0`]T]T0T]T0T-T`T-T-T-T-T-T-T-T-T-T-``````````````````````]T-T-T-T-T-T-T-T-T-T0`-T-T-T-T-Q!T-T-T-T-T`T-T-T-T-QX&gt;U$-T-T-T-`]T-T-T-QX=T-X1-T-T-T0`-T-T-QX=T-T-T&gt;!T-T-T`T-T-TX=T-T-T-T.UT-T-`]T-T-^X-T-T-T-T^-T-T0`-T-T0&gt;X=T-T-T``4-T-T`T-T-TX&gt;X&gt;T-T```UT-T-`]T-T-^X&gt;X&gt;X@```^-T-T0`-T-T0&gt;X&gt;X&gt;`````4-T-T`T-T-TX&gt;X&gt;X@````UT-T-`]T-T-^X&gt;X&gt;X````^-T-T0`-T-T0&gt;X&gt;X&gt;`````4-T-T`T-T-TX&gt;X&gt;X@````UT-T-`]T-T-^X&gt;X&gt;X````&gt;``-T0`-T-T-^X&gt;X&gt;```&gt;X```]T`T-T-T-TX&gt;X@`&gt;X```]T-`]T-T-T-T0&gt;X&gt;T````T-T0`-T-T-T-T-^T````T-T-T`T-T-T-T-T-T0``T-T-T-`]T-T-T-T-T-T-T-T-T-T0`````````````````````]!!!1!````````````````````````````````````````````+SML+SML+SML+SML+SML+SML+SML+SML+SML+SML``]L+SML+SML`SML````+SP`+SML`SP`+SML`SML+SP``SML+SML+SML+SP`+SP`+```+```+```+```+SML+```+SML``]L+`]L+`]L+`]L`SP`+`]L`SP`+`]L+SML``]L+SP`+`]L`SML`SML`SP`+SML`SP`+SML`SML+SP``SML+`]L`SP`+SP```]L+`]L+SP`+`]L+SP`+SML+```+SML+SML+SML+SML+SML+SML+SML+SML+SML+SML`````````````````````````````````````````````R!1%"!1%"!1%"!1%"!1%"!1%"!1%"!1%"!1%"!1%0``%"!1%"!1%"!1%"!1%"!!!"!1%"!1%"!1%"!1%"!1``]1%"!1%"!1%"!1%"!!A6;"A1!1%"!1%"!1%"!1%"$``R!1%"!1%"!1%"!!A69L+SMLA9%!%"!1%"!1%"!1%0``%"!1%"!1%"!!A69L+SML+SML+Y'"!"!1%"!1%"!1``]1%"!1%"!1A69L+SML+SML+SML+SO"A2!1%"!1%"$``R!1%"!1%""76CML+SML+SML+SML+[S"%"!1%"!1%0``%"!1%"!1%&amp;;"A69L+SML+SML+[SML&amp;91%"!1%"!1``]1%"!1%"!16I'"A9&amp;7+SML+[SML+SM6B!1%"!1%"$``R!1%"!1%""7A9'"A9'"6I'ML+SML+R7%"!1%"!1%0``%"!1%"!1%&amp;;"A9'"A9'"L+SML+SML&amp;91%"!1%"!1``]1%"!1%"!16I'"A9'"A9'ML+SML+SM6B!1%"!1%"$``R!1%"!1%""7A9'"A9'"A;SML+SML+R7%"!1%"!1%0``%"!1%"!1%&amp;;"A9'"A9'"L+SML+SML&amp;91%"!1%"!1``]1%"!1%"!16I'"A9'"A9'ML+SML+SM6B!1%"!1%"$``R!1%"!1%"#"A9'"A9'"A;SML+SML)'"L+SM%"!1%0``%"!1%"!1%""76I'"A9'"L+SML)'"6KSML+SML"!1``]1%"!1%"!1%"!16I'"A9'ML)'"6KSML+SML"!1%"$``R!1%"!1%"!1%"!1%&amp;;"A9'"+[SML+SML+Q1%"!1%0``%"!1%"!1%"!1%"!1%""7+[SML+SML+Q1%"!1%"!1``]1%"!1%"!1%"!1%"!1%"!1%+SML+Q1%"!1%"!1%"$``R!1%"!1%"!1%"!1%"!1%"!1%"!1%"!1%"!1%"!1%0```````````````````````````````````````````Q!!!!Q!!5:13&amp;!!!!!!!!-!!!"M&amp;A#!!!!!!!%!"!!!!!%!!!!!!!)!!!!.:'FT='RB?5:J&lt;(2F=B9!A!!!!!!"!!5!"Q!!!1!!!!%1!Q!!!!!!!!!*&gt;(FQ:5.M98.T&amp;A#!!!!!!!%!#!!Q`````Q!"!!!!!!!&amp;&lt;GF%45U!!!!!!!!%;!!!#3"YH,V74WT&lt;:"2`8_)Q*WP8T^X7.F/LO*6&gt;+C#!*M&amp;'+7(KX)F/J;TLSL:+`!FT2IN#-G*\9EBM3&amp;C6#NKJE!03*%\&gt;=9=?O!Y5!J)&amp;\-4%BB3V"]1.$H2&amp;9YZZHRX&lt;3=@#":&amp;)VB@\_\X@?_`X_ZY$%/_H@:%[8,3"U%V=4.G15#U#5%HTU0A-,Q'&gt;)X]"W:-E.BTCZ_DN3*XUW^#B7M0]E]IS`)[\H2_=]\"+"OA[&lt;HW)*D&amp;9QI9OV?I6DEIV+FXLFZ:D@F1""OBF5I_=EF*&lt;`'7TC)2ADL#LE#:V)-IARZF$R\+&amp;H#GRO`%UHX2$RGWACN6:EGI09U3E`MI.3=J1*6@^E)!B2W"N&lt;3U%#2ZIW%XD)-.5-?R.5G[$[6;M!5WK0?:C%CY'?7\[0%KKPM*S:[$NU.W+22'+O(@^EKM_FZ+S$\GYD9U.R/'VA6OQ99^5'_?4`,J[+^9Z9XY""%DF.&gt;[ZY^RB?'';K?$O\59BZ&amp;&amp;#:X%^;]-DJB6Z(4C@[QCMO$*QPARD+!-ZYMI16Z9JB$KM`)M/#F=:'DC=.T1^6R+,:]5T_;SGC?&gt;+C_?T?EZ5MXLW@I7?5[TY!69^)X0.!8O"AX%YV^TN)KSOLG)$]"J#-QDNF7I"4H#LU9//KW(('7P9O??R=]K@*^ZEX:.()[W?H1A^OR]^+^$N;&lt;6Y^KH`XL.0IZ5O&lt;P-MZ#%.\\@RXQ%0V/4:.+)SE'_$/9C93SW?43.0RO&gt;JZ^FH\P&gt;MWO=+0&amp;MOFVNQW0H2Q,-=):ZHT2P/0?=?=_Z&gt;ZSY]#_]V/8=/Z7$DYD?-XY`R7XQ[R8Q[,&gt;7/+^&gt;3-[&amp;2#4&lt;`F@&lt;.&amp;S0AG888:%(43]&lt;&lt;O9,_1E(.?VJ1W?0(L3#.%%R8$O2"WB@^Y`%`UHZCQT2/7JYK.=&lt;J_8&amp;XI_GM+4P=&lt;4NN/+:937TS3^ZR=MU4AZ01!QO?&amp;%)+&gt;U=9CSPQRYSAM@[)4]:4$=*0&lt;:B2,4'_8[LN&lt;$K:-&gt;D&amp;1PE[/Y+LM_-Y'"SP8JXG)(S!\J&lt;9H&gt;;C)FZ2UP6%A[&lt;$BO-Y&lt;*(F57`9&gt;$#7+0,MAYZ'\Y+%IW)FZ6GN&amp;X;9)FO9AW\HBBI6'0S16]]`&lt;H8T;&gt;E;F)O/HF7M'-YBYO6RH?8"Y&lt;=(/HV2.R^U'%ZMGU0MMS]]$1_?1X.Y'O44)-]$4K.@'P&gt;2R&amp;/?C&amp;V?,LT8?1Y.FM!Y0^X['2-QFDZX/]0Z]GV^'UKZ^5X4ONKU`NKLG5W^[0"36([V3S:279L+*S/R\W,@BZG&gt;&lt;M\M6Y;A]`DE3RO/#HV-UELSQ\(RT',A9X%BCU\/C56$(XNC0#/7=LJ2+GBCVH_A,W2V]5,2%!U.@R4&amp;2262CW=PY)/=W"2HM3"G]XF2-^\1=O]9XJXAI9KP%HSVP$QJHM&amp;.WO0R0HZ?(O6QA'.BUUWDDL^&gt;.9,`*0SMJ$DN.N!*L/W3$8O6Q^XL&gt;",LI`QE0U'PY'M!XQA^`"6_M`*D]+_F-B5AT2PQ2`1N[&lt;-5X`=X-&gt;/7#1!!!!1!!!!Q!!!!"!!!!!!!!!!-!!&amp;#2%B1!!!!!!!$!!!!9A!!!(*YH'.A9-A4E'$[RV$XFY&amp;*Y#O1)@W8A6H1D`%X!Q/HH]"B)-UI)!E5FPX,Q#[I$2&lt;70K,,Q1!&amp;KGS-(*)=BQ5ZQ$)=,2I-````Z`B[Z"J=R2%@/&amp;.FFDS("!!59BE!!!!!!!!%!!!!"Q!!!RA!!!!(!!!!)6^O;6^-98.U3WZP&gt;WZ0&gt;WZJ&lt;G&gt;-6E.M98.T1WRV=X2F=A!!!)Y7!)!!!!!!!1!)!$$`````!!%!!!!!!()!!!!$!"Y!.`````]!!R9!A!!!!!!"!!1!!!!"!!!!!!!!!$:!=!!0"7ZJ2%V.!!%!!!!!&amp;A#!!!!!!!%!"!!!!!%!!!!!!!!/37ZT&gt;(*V&lt;76O&gt;%BO:'Q!!":!5!!"!!%.&lt;GF%45UO&lt;(:D&lt;'&amp;T=Q!"!!)!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962B9E^S:'6S!!!!+29!A!!!!!!#!!5!"Q!!$!"!!!(`````!!!!!1!"!!!!!1!!!!!!!!!!!!!!'UR71WRB=X.1=GFW982F2'&amp;U962J&lt;76T&gt;'&amp;N=!!!!"E7!)!!!!!!!1!&amp;!!=!!!%!!.9HLLA!!!!!!!!!*ER71WRB=X.1=GFW982F2'&amp;U95RB=X2"=("M;76E6'FN:8.U97VQ!!!!'29!A!!!!!!"!!5!"Q!!!1!!VC?OO!!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6(FQ:52F=W-!!!#/&amp;A#!!!!!!!%!#!!Q`````Q!"!!!!!!"S!!!!!Q!?!$@`````!!-7!)!!!!!!!1!%!!!!!1!!!!!!!!!W1(!!$Q6O;52.41!"!!!!!"9!A!!!!!!"!!1!!!!"!!!!!!!!$EFO=X2S&gt;7VF&lt;H2)&lt;G2M!!!71&amp;!!!1!"$7ZJ2%V.,GRW9WRB=X-!!1!#!!!!!!!!!"Z-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U96.J?G5!!!!:&amp;A#!!!!!!!%!"1!$!!!"!!!!!!!%!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U91!!!(Y7!)!!!!!!!Q!?!$@`````!!-7!)!!!!!!!1!%!!!!!1!!!!!!!!!W1(!!$Q6O;52.41!"!!!!!"9!A!!!!!!"!!1!!!!"!!!!!!!!$EFO=X2S&gt;7VF&lt;H2)&lt;G2M!!!71&amp;!!!1!"$7ZJ2%V.,GRW9WRB=X-!!1!#!!!!!!!!!!!!!!!%!!1!#Q!!!!1!!!":!!!!+!!!!!)!!!1!!!!!#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%:!!!"[XC=D5_\4M.!%"TH('+4R!G01!7[+EA5.%D17A)B+#*&amp;&amp;.190Z#FQY\M=Z1S8U4.F`!&gt;]!5QO1OCA#)\UGJH&lt;W^W&amp;M!BDH(ZR9!YQ")-"[\*.C\#'1&lt;N)L_?4'TPP[HALKBVV&lt;SEB&lt;YN%A5]Y00^Z07.4[*L`J[JO=K@_D^VL++[^CW,N=*J/+77=X3FGFKHF3QT;5&lt;EL-LHE5ZF%OE)&lt;1,&lt;3,DX!Y*%L#H/3@SQ!Z'J:XDB5J4XWD&lt;3-M/1[A)&gt;?0"&amp;EW199Y/&lt;U$/?OE)P%G[:IM8=QXB$JSUQ&lt;;U3^4Q]5N$&amp;$75E"BD3TAI\ZC1,M=&lt;@TO`,LI%^W=%?K^DI7O-"F\L=W7?VDR'\)VY7%*T_"E*02ZI!!!!!!!#*!!%!!A!$!!9!!!"I!!]%!!!!!!]!W!$6!!!!=1!0"!!!!!!0!.A!V1!!!(I!$Q1!!!!!$Q$9!.5!!!#$A!#%!)!!!!]!W!$6!!!!B9!!B!!$[!!0!/U!XQ!!!)?!!)1!A!!!$Q$9!.5)5W6H&lt;W5A65E)5W6H&lt;W5A65E)5W6H&lt;W5A65E"-A%Q!4!!!!"35V*$$1I!!UR71U.-1F:8!!!5Z!!!"&amp;)!!!!A!!!5R!!!!!!!!!!!!!!!)!!!!$1!!!2)!!!!(5R*1EY!!!!!!!!"&lt;%R75V)!!!!!!!!"A&amp;*55U=!!!!!!!!"F%.$5V1!!!!!!!!"K%R*&gt;GE!!!!!!!!"P%.04F!!!!!!!!!"U&amp;2./$!!!!!!!!!"Z%2'2&amp;-!!!!!!!!"_%R*:(-!!!!!!!!#$&amp;:*1U1!!!!!!!!#)(:F=H-!!!!%!!!#.&amp;.$5V)!!!!!!!!#G%&gt;$5&amp;)!!!!!!!!#L%F$4UY!!!!!!!!#Q'FD&lt;$1!!!!!!!!#V'FD&lt;$A!!!!!!!!#[%R*:H!!!!!!!!!#`&amp;.55C!!!!!!!!!$%%:13')!!!!!!!!$*%:15U5!!!!!!!!$/&amp;:12&amp;!!!!!!!!!$4%R*9G1!!!!!!!!$9%*%3')!!!!!!!!$&gt;%*%5U5!!!!!!!!$C&amp;:*6&amp;-!!!!!!!!$H%253&amp;!!!!!!!!!$M%V6351!!!!!!!!$R%B*5V1!!!!!!!!$W&amp;:$6&amp;!!!!!!!!!$\%:515)!!!!!!!!%!!!!!!$`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*!!!!!!!!!!!`````Q!!!!!!!!$)!!!!!!!!!!$`````!!!!!!!!!.Q!!!!!!!!!!0````]!!!!!!!!!Z!!!!!!!!!!!`````Q!!!!!!!!%1!!!!!!!!!!$`````!!!!!!!!!2A!!!!!!!!!!0````]!!!!!!!!"2!!!!!!!!!!!`````Q!!!!!!!!'1!!!!!!!!!!$`````!!!!!!!!!;!!!!!!!!!!"0````]!!!!!!!!$/!!!!!!!!!!(`````Q!!!!!!!!.)!!!!!!!!!!D`````!!!!!!!!!VA!!!!!!!!!#@````]!!!!!!!!$;!!!!!!!!!!+`````Q!!!!!!!!.Y!!!!!!!!!!$`````!!!!!!!!!YA!!!!!!!!!!0````]!!!!!!!!$I!!!!!!!!!!!`````Q!!!!!!!!/U!!!!!!!!!!$`````!!!!!!!!"$A!!!!!!!!!!0````]!!!!!!!!'0!!!!!!!!!!!`````Q!!!!!!!!J!!!!!!!!!!!,`````!!!!!!!!#F!!!!!!!!!!!0````]!!!!!!!!+Q!!!!!!!!!!!`````Q!!!!!!!!]M!!!!!!!!!!$`````!!!!!!!!$T1!!!!!!!!!!0````]!!!!!!!!00!!!!!!!!!!!`````Q!!!!!!!!^-!!!!!!!!!!$`````!!!!!!!!$\1!!!!!!!!!!0````]!!!!!!!!0P!!!!!!!!!!!`````Q!!!!!!!",9!!!!!!!!!!$`````!!!!!!!!%O!!!!!!!!!!!0````]!!!!!!!!3[!!!!!!!!!!!`````Q!!!!!!!"-5!!!!!!!!!)$`````!!!!!!!!&amp;$1!!!!!#7ZJ2%V.,G.U&lt;!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!ANO;52.43ZM&gt;GRJ9AVO;52.43ZM&gt;G.M98.T5&amp;2)-!!!!!!!!!!!!!!!!!!#!!%!!!!!!!!"!!!!!Q!?!$@`````!!-7!)!!!!!!!1!%!!!!!1!!!!!!!!!W1(!!$Q6O;52.41!"!!!!!"9!A!!!!!!"!!1!!!!"!!!!!!!!$EFO=X2S&gt;7VF&lt;H2)&lt;G2M!!!71&amp;!!!1!"$7ZJ2%V.,GRW9WRB=X-!!1!#!!!!!!!!!!!!!!!!!AJ%45UO&lt;(:M;7*Q#U2.43ZM&gt;G.M98.T!&amp;"53$!!!!!!!!!!!!!7!)!!!!!!!!!!!!!!!!!!!1!!!!!!!!!!!!!$!"Y!.`````]!!R9!A!!!!!!"!!1!!!!"!!!!!!!!!$:!=!!0"7ZJ2%V.!!%!!!!!&amp;A#!!!!!!!%!"!!!!!%!!!!!!!!/37ZT&gt;(*V&lt;76O&gt;%BO:'Q!!":!5!!"!!%.&lt;GF%45UO&lt;(:D&lt;'&amp;T=Q!"!!)!!!!"`````A!!!!!!!!!!!AJ%45UO&lt;(:M;7*Q#U2.43ZM&gt;G.M98.T!&amp;"53$!!!!!!!!!!!!!7!)!!!!!!!!!!!!!!!1!!!#.%45V@4EEA5&amp;B*,41Q.T)O&lt;(:M;7)[&lt;GF%45UO&lt;(:D&lt;'&amp;T=Q</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">true</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"4!!!!!AJ%45UO&lt;(:M;7*Q#U2.43ZM&gt;G.M98.T!&amp;"53$!!!!!P!!%!#!!!!!!!$E2F&gt;C"$&lt;WVQ&lt;WZF&lt;H2T#E2.43ZM&gt;GRJ9H!,2%V.,GRW9WRB=X-!!!!!</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="niDMM.ctl" Type="Class Private Data" URL="niDMM.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Clusters" Type="Folder">
		<Item Name="SerialConfiguration --cluster.ctl" Type="VI" URL="../SerialConfiguration --cluster.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#&gt;!!!!"!!01!=!#5*B&gt;71A5G&amp;U:1!.1!9!"F"B=GFU?1!!$U!'!!F%982B)%*J&gt;(-!;A$R!!!!!!!!!!-,&lt;GF%45UO&lt;(:M;7).&lt;GF%45UO&lt;(:D&lt;'&amp;T=S&amp;4:8*J97R$&lt;WZG;7&gt;V=G&amp;U;7^O)#UN9WRV=X2F=CZD&gt;'Q!*E"1!!-!!!!"!!)55W6S;7&amp;M)%.P&lt;G:J:X6S982J&lt;WY!!!%!!Q!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
		</Item>
	</Item>
	<Item Name="Low Level Config" Type="Folder">
		<Item Name="ConfigureMultiPoint.vi" Type="VI" URL="../ConfigureMultiPoint.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(T!!!!$A!O1(!!(A!!'QNO;52.43ZM&gt;GRJ9AVO;52.43ZM&gt;G.M98.T!!BO;52.43"J&lt;A!!'5!$!"*1=G6U=GFH:W6S)&amp;.B&lt;8"M:8-!!'Y!]1!!!!!!!!!$#E2.43ZM&gt;GRJ9H!,2%V.,GRW9WRB=X-85W&amp;N='RF5W^V=G.F)#UN:7ZV&lt;3ZD&gt;'Q!.U!7!!).6(*J:W&gt;F=C"%:7RB?1^497VQ&lt;'5A37ZU:8*W97Q!$6.B&lt;8"M:3"4&lt;X6S9W5!"!!!!#Z!=!!?!!!&lt;#WZJ2%V.,GRW&lt;'FC$7ZJ2%V.,GRW9WRB=X-!#7ZJ2%V.)'^V&gt;!!41!-!$62S;7&gt;H:8)A1W^V&lt;H1!%U!$!!R497VQ&lt;'5A1W^V&lt;H1!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!#"!5!!$!!=!#!!*%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;5!+!!^497VQ&lt;'5A37ZU:8*W97Q!&amp;E"1!!-!"Q!)!!E*:8*S&lt;X)A&lt;X6U!'Q!]!!1!!!!!1!#!!-!"!!&amp;!!-!"A!$!!-!!Q!+!!M!!Q!$!!Q$!!%)!!#1!!!!#!!!!!A!!!!!!!!!D1!!!"!!!!!!!!!!%!!!!!!!!!!!!!!!!!!!!!I!!!!1!!!!!!!!!!!!!!!.#Q!!!!%!$1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
		<Item Name="ConfigureTrigger.vi" Type="VI" URL="../ConfigureTrigger.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!).!!!!$A!O1(!!(A!!'QNO;52.43ZM&gt;GRJ9AVO;52.43ZM&gt;G.M98.T!!BO;52.43"J&lt;A!!'5!+!"**&lt;H2F=GZB&lt;#"-:8:F&lt;#!I-#E!!"F!"A!45WRP='5A+$![)%ZF:W&amp;U;8:F+1!%!!!!,E"Q!"Y!!"M,&lt;GF%45UO&lt;(:M;7).&lt;GF%45UO&lt;(:D&lt;'&amp;T=Q!*&lt;GF%45UA&lt;X6U!']!]1!!!!!!!!!$#E2.43ZM&gt;GRJ9H!,2%V.,GRW9WRB=X-96(*J:W&gt;F=F.P&gt;8*D:3!N,76O&gt;7UO9X2M!$&gt;!&amp;A!$#5FN&lt;76E;7&amp;U:1B&amp;?(2F=GZB&lt;!B4&lt;W:U&gt;W&amp;S:1!/6(*J:W&gt;F=C"4&lt;X6S9W5!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!#"!5!!$!!9!"Q!)%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!)E!B(56O97*M:3""&gt;82P)%2F&lt;'&amp;Z)#B5/C"&amp;&lt;G&amp;C&lt;'5J!"F!#A!46(*J:W&gt;F=C"%:7RB?3!I=W6D+1!71&amp;!!!Q!'!!=!#!FF=H*P=C"P&gt;81!&lt;!$Q!"!!!!!"!!)!!Q!%!!5!!Q!$!!-!!Q!$!!E!#A!,!!-!$!-!!1A!!*!!!!!)!!!!#!!!!!!!!!#.!!!!#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!A!!!!)!!!!!!!!!!U,!!!!!1!.!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
		<Item Name="FetchMultiPoint.vi" Type="VI" URL="../FetchMultiPoint.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;_!!!!$1!O1(!!(A!!'QNO;52.43ZM&gt;GRJ9AVO;52.43ZM&gt;G.M98.T!!BO;52.43"J&lt;A!!"!!!!#Z!=!!?!!!&lt;#WZJ2%V.,GRW&lt;'FC$7ZJ2%V.,GRW9WRB=X-!#7ZJ2%V.)'^V&gt;!!61!-!$UZV&lt;7*F=C"U&lt;S"':82D;!!:1!-!%UVB?'FN&gt;7UA6'FN:3!I&lt;8.F9SE!"1!+!!!;1%!!!@````]!"1R.:7&amp;T&gt;8*F&lt;76O&gt;(-!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!#"!5!!$!!=!#!!*%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;E"1!!-!"Q!)!!E*:8*S&lt;X)A&lt;X6U!'Q!]!!1!!!!!1!"!!%!!A!$!!%!"!!'!!%!!1!+!!%!!1!"!!M$!!%)!!#1!!!!!!!!!!!!!!!!!!!!D1!!!!A!!!!!!!!!#!!!!!E!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!!!!!!!.#Q!!!!%!$!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
		<Item Name="InitiateMeasurement.vi" Type="VI" URL="../InitiateMeasurement.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%R!!!!#1!O1(!!(A!!'QNO;52.43ZM&gt;GRJ9AVO;52.43ZM&gt;G.M98.T!!BO;52.43"J&lt;A!!"!!!!#Z!=!!?!!!&lt;#WZJ2%V.,GRW&lt;'FC$7ZJ2%V.,GRW9WRB=X-!#7ZJ2%V.)'^V&gt;!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!A1&amp;!!!Q!$!!1!"2.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!":!5!!$!!-!"!!&amp;#76S=G^S)'^V&gt;!"M!0!!%!!!!!%!!1!"!!)!!1!"!!%!!1!"!!%!"A!"!!%!!1!(!Q!"#!!!E!!!!!!!!!!!!!!!!!!!!)U!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!!!!!!$1M!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
		<Item Name="SendSoftwareTrigger.vi" Type="VI" URL="../SendSoftwareTrigger.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%R!!!!#1!O1(!!(A!!'QNO;52.43ZM&gt;GRJ9AVO;52.43ZM&gt;G.M98.T!!BO;52.43"J&lt;A!!"!!!!#Z!=!!?!!!&lt;#WZJ2%V.,GRW&lt;'FC$7ZJ2%V.,GRW9WRB=X-!#7ZJ2%V.)'^V&gt;!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!A1&amp;!!!Q!$!!1!"2.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!":!5!!$!!-!"!!&amp;#76S=G^S)'^V&gt;!"M!0!!%!!!!!%!!1!"!!)!!1!"!!%!!1!"!!%!"A!"!!%!!1!(!Q!"#!!!E!!!!!!!!!!!!!!!!!!!!)U!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!!!!!!$1M!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
	</Item>
	<Item Name="Protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		<Item Name="niDMM_Initialize.vi" Type="VI" URL="../niDMM_Initialize.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(P!!!!%!!O1(!!(A!!'QNO;52.43ZM&gt;GRJ9AVO;52.43ZM&gt;G.M98.T!!BO;52.43"J&lt;A!!$U!(!!F#986E)&amp;*B&gt;'5!$5!'!!:198*J&gt;(E!!!^!"A!*2'&amp;U93"#;82T!'=!]1!!!!!!!!!$#E2.43ZM&gt;GRJ9H!,2%V.,GRW9WRB=X-B5W6S;7&amp;M1W^O:GFH&gt;8*B&gt;'FP&lt;C!N,7.M&gt;8.U:8)O9X2M!#:!5!!$!!%!!A!$&amp;&amp;.F=GFB&lt;#"$&lt;WZG;7&gt;V=G&amp;U;7^O!!!%!!!!,E"Q!"Y!!"M,&lt;GF%45UO&lt;(:M;7).&lt;GF%45UO&lt;(:D&lt;'&amp;T=Q!*&lt;GF%45UA&lt;X6U!"2!-0````]+37ZT&gt;(*V&lt;76O&gt;!!!$E!B#%F%)&amp;&amp;V:8*Z!!!+1#%&amp;5G6T:81!$%!B"H.U982V=Q!!#U!$!!2D&lt;W2F!!!11$$`````"H.P&gt;8*D:1!!)%"1!!-!#A!,!!Q4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71&amp;!!!Q!+!!M!$!FF=H*P=C"P&gt;81!&lt;!$Q!"!!!!!%!!5!"1!'!!=!"1!)!!5!#1!&amp;!!U!"1!&amp;!!5!$A-!!1A!!*)!!!!)!!!!!!!!!!!!!!#.!!!!%!!!!!!!!!!)!!!!!!!!!!A!!!!!!!!!#A!!!!!!!!!!!!!!!!!!!!U,!!!!!1!0!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082917392</Property>
		</Item>
	</Item>
	<Item Name="ConfigureAutoZero.vi" Type="VI" URL="../ConfigureAutoZero.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;!!!!!#A!O1(!!(A!!'QNO;52.43ZM&gt;GRJ9AVO;52.43ZM&gt;G.M98.T!!BO;52.43"J&lt;A!!"!!!!#Z!=!!?!!!&lt;#WZJ2%V.,GRW&lt;'FC$7ZJ2%V.,GRW9WRB=X-!#7ZJ2%V.)'^V&gt;!!01!9!#%&amp;V&gt;'^[:8*P!!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!A1&amp;!!!Q!%!!5!"B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!":!5!!$!!1!"1!'#76S=G^S)'^V&gt;!"M!0!!%!!!!!%!!1!"!!)!!Q!"!!%!!1!"!!%!"Q!"!!%!!1!)!Q!"#!!!E!!!!!!!!!!!!!!!!!!!!)U!!!!1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!!!!!!$1M!!!!"!!E!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
	</Item>
	<Item Name="ConfigureOffsetCompensation.vi" Type="VI" URL="../ConfigureOffsetCompensation.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'=!!!!#A!O1(!!(A!!'QNO;52.43ZM&gt;GRJ9AVO;52.43ZM&gt;G.M98.T!!BO;52.43"J&lt;A!!"!!!!#Z!=!!?!!!&lt;#WZJ2%V.,GRW&lt;'FC$7ZJ2%V.,GRW9WRB=X-!#7ZJ2%V.)'^V&gt;!"L!0%!!!!!!!!!!QJ%45UO&lt;(:M;7*Q#U2.43ZM&gt;G.M98.T)&amp;.F&gt;%^G:H.F&gt;%.P&lt;8"F&lt;H.B&gt;'FP&lt;C!N,76O&gt;7UO9X2M!#N!&amp;A!#!U^G:A*0&lt;A!!&amp;V.F&gt;#"0:G:T:81A1W^N='6O=W&amp;U;7^O!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!#"!5!!$!!1!"1!'%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;E"1!!-!"!!&amp;!!9*:8*S&lt;X)A&lt;X6U!'Q!]!!1!!!!!1!"!!%!!A!$!!%!!1!"!!%!!1!(!!%!!1!"!!A$!!%)!!#1!!!!!!!!!!!!!!!!!!!!D1!!!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!!!!!!!.#Q!!!!%!#1!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
	</Item>
	<Item Name="ConfigureMeasurement.vi" Type="VI" URL="../ConfigureMeasurement.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*E!!!!$1!O1(!!(A!!'QNO;52.43ZM&gt;GRJ9AVO;52.43ZM&gt;G.M98.T!!BO;52.43"J&lt;A!!"!!!!#Z!=!!?!!!&lt;#WZJ2%V.,GRW&lt;'FC$7ZJ2%V.,GRW9WRB=X-!#7ZJ2%V.)'^V&gt;!#=!0%!!!!!!!!!!QJ%45UO&lt;(:M;7*Q#U2.43ZM&gt;G.M98.T%U:V&lt;G.U;7^O)#UN:7ZV&lt;3ZD&gt;'Q!;5!7!!9+2%-A6G^M&gt;'&amp;H:1J"1S"7&lt;WRU97&gt;F#E2$)%.V=H*F&lt;H1+15-A1X6S=G6O&gt;"-S)#UA6WFS:3"3:8.J=X2B&lt;G.F%T1A,3"8;8*F)&amp;*F=WFT&gt;'&amp;O9W5!#%:V&lt;G.U;7^O!!"K!0%!!!!!!!!!!QJ%45UO&lt;(:M;7*Q#U2.43ZM&gt;G.M98.T&amp;6*F=W^M&gt;82J&lt;WYA,3VF&lt;H6N,G.U&lt;!!V1"9!"15T)$%P-A5U)$%P-A5V)$%P-A5W)$%P-A5X)$%P-A!+5G6T&lt;WRV&gt;'FP&lt;A!!#U!+!!6397ZH:1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!A1&amp;!!!Q!'!!=!#".F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#*!)2V&amp;&lt;G&amp;C&lt;'5A186U&lt;S"397ZH:3!I6$IA27ZB9GRF+1!71&amp;!!!Q!'!!=!#!FF=H*P=C"P&gt;81!&lt;!$Q!"!!!!!"!!%!!1!#!!-!!1!%!!%!"1!"!!E!#A!"!!%!#Q-!!1A!!*!!!!!!!!!!!!!!!!!!!!#.!!!!#!!!!!!!!!!)!!!!!!!!!!A!!!!!!!!!#A!!!!A!!!!!!!!!!!!!!!U,!!!!!1!-!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">3</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082929680</Property>
	</Item>
	<Item Name="Close.vi" Type="VI" URL="../Close.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%R!!!!#1!O1(!!(A!!'QNO;52.43ZM&gt;GRJ9AVO;52.43ZM&gt;G.M98.T!!BO;52.43"J&lt;A!!"!!!!#Z!=!!?!!!&lt;#WZJ2%V.,GRW&lt;'FC$7ZJ2%V.,GRW9WRB=X-!#7ZJ2%V.)'^V&gt;!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!A1&amp;!!!Q!$!!1!"2.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!":!5!!$!!-!"!!&amp;#76S=G^S)'^V&gt;!"M!0!!%!!!!!%!!1!"!!)!!1!"!!%!!1!"!!%!"A!"!!%!!1!(!Q!"#!!!E!!!!!!!!!!!!!!!!!!!!)U!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!!!!!!#1!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082143248</Property>
	</Item>
	<Item Name="NameAndVersionNumber.vi" Type="VI" URL="../NameAndVersionNumber.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;0!!!!#A!O1(!!(A!!'QNO;52.43ZM&gt;GRJ9AVO;52.43ZM&gt;G.M98.T!!BO;52.43"J&lt;A!!"!!!!#Z!=!!?!!!&lt;#WZJ2%V.,GRW&lt;'FC$7ZJ2%V.,GRW9WRB=X-!#7ZJ2%V.)'^V&gt;!!?1$$`````&amp;%ZB&lt;76"&lt;G27:8*T;7^O4H6N9G6S!!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!A1&amp;!!!Q!%!!5!"B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!":!5!!$!!1!"1!'#76S=G^S)'^V&gt;!"M!0!!%!!!!!%!!1!"!!)!!1!"!!%!!Q!"!!%!"Q!"!!%!!1!)!Q!"#!!!EA!!!!!!!!!!!!!!!!!!!)U!!!!!!!!!!!!!!!!!!!!*!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!!!!!!$1M!!!!"!!E!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082929680</Property>
	</Item>
	<Item Name="ReadMultiPoint.vi" Type="VI" URL="../ReadMultiPoint.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;_!!!!$1!O1(!!(A!!'QNO;52.43ZM&gt;GRJ9AVO;52.43ZM&gt;G.M98.T!!BO;52.43"J&lt;A!!"!!!!#Z!=!!?!!!&lt;#WZJ2%V.,GRW&lt;'FC$7ZJ2%V.,GRW9WRB=X-!#7ZJ2%V.)'^V&gt;!!61!-!$EZV&lt;7*F=C"U&lt;S"3:7&amp;E!!!&amp;!!I!!"J!1!!"`````Q!%$%VF98.V=G6N:7ZU=Q!!$%!B"H.U982V=Q!!#U!$!!2D&lt;W2F!!!11$$`````"H.P&gt;8*D:1!!)%"1!!-!"A!(!!A4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!:1!-!%UVB?'FN&gt;7UA6'FN:3!I&lt;8.F9SE!&amp;E"1!!-!"A!(!!A*:8*S&lt;X)A&lt;X6U!'Q!]!!1!!!!!1!"!!%!!A!$!!%!!1!&amp;!!%!!1!*!!I!!1!"!!M$!!%)!!#1!!!!!!!!!!!!!!!!!!!!D1!!!!A!!!!!!!!!!!!!!!E!!!!!!!!!!!!!!!I!!!!)!!!!!!!!!!!!!!!.#Q!!!!%!$!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
	</Item>
	<Item Name="ReadSinglePoint.vi" Type="VI" URL="../ReadSinglePoint.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;&lt;!!!!#Q!O1(!!(A!!'QNO;52.43ZM&gt;GRJ9AVO;52.43ZM&gt;G.M98.T!!BO;52.43"J&lt;A!!"!!!!#Z!=!!?!!!&lt;#WZJ2%V.,GRW&lt;'FC$7ZJ2%V.,GRW9WRB=X-!#7ZJ2%V.)'^V&gt;!!21!I!#UVF98.V=G6N:7ZU!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!#"!5!!$!!1!"1!'%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!'5!$!"..98BJ&lt;86N)&amp;2J&lt;75A+'VT:7-J!":!5!!$!!1!"1!'#76S=G^S)'^V&gt;!"M!0!!%!!!!!%!!1!"!!)!!1!"!!%!!Q!"!!%!"Q!)!!%!!1!*!Q!"#!!!E!!!!!!!!!!!!!!!!!!!!)U!!!!!!!!!!!!!!!!!!!!*!!!!!!!!!!!!!!!+!!!!%!!!!!!!!!!!!!!!$1M!!!!"!!I!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
	</Item>
</LVClass>
