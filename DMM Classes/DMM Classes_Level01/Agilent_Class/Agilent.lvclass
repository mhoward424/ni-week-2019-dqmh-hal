﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="16008000">
	<Property Name="EndevoGOOP_ClassItemIcon" Type="Str">CyanFull</Property>
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">11507131</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorProtected" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">12713864</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.ContainingLib" Type="Str">Agilent.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../Agilent.lvlib</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,_!!!*Q(C=T:3R&lt;B."%)&lt;H#%7%%-B01'1E#NQAJE/)SEU?Q#53V23]A+H)F8Y%JC?.Z3@!1I)3;6\"1GF"=IOR$JPPVGM\ZS2W!V*W-`&lt;?`]`/@N\&gt;H%BKRS*X:8GIT@;WWE]Z&gt;7&lt;&gt;V[0[I2[MRSFZZ;_G*4E.]N4N`.8ED:]H,C`0H]W;`HL^Z8+\`GT.NJ@`5(OSN_X[H=Y_PV0.K[JTEY^&lt;)T5SNHZW&gt;T)W`N:&gt;:?T[]Y;`O/J8WQK,R7*_D:]T&amp;IM&lt;@4+KR6Y`W\@0LRJ_69V'I_&lt;_T_=&lt;HZ]Z3KVZ@P/6@]H&gt;0@_5-2NN\3PX:^ZQL`KU3_[V@K-&gt;P.[]"]L$`Q4`NYE55L_)C##=-%+FNJ;"(OC"(OC"(OC/\OC/\OC/\OC'&lt;OC'&lt;OC'&lt;OC+LOC+LOC+LOCJIQO[I!M[OR)M(CQ5&amp;!U+"-GA30!2]!1]!5`!Q[-%0!&amp;0Q"0Q"$SE3-!4]!1]!5`!QT1*?!+?A#@A#8AIF5AC&gt;82Y!B\+C]0D]$A]$I`$QZ,C]$A!TG*/9;=)'/+9TM$B=8A=(I&lt;C]$A]$I`$Y`"AC]0D]$A]$I`$QZ3U+ZZI[IY/$W8%Y$&amp;Y$"[$R_#BN"A]"I`"9`!90#QH"I`"9U!9#RL&amp;12"DEJ&amp;A0"A]"A^@9P!90!;0Q70Q9+54ML1T.5X&gt;U?&amp;2?"1?B5@B58AI)1K0QK0Q+$Q+$W6&amp;Y6&amp;Y&amp;"[&amp;2_&amp;B+6&amp;Y&amp;"[&amp;2Q&amp;2&amp;G6Z59IJ%Z5E26"Y_%OX2&gt;-J?3,2V%8SZ:6]+36@.MG830,FE(TIEA^4]C&amp;*XHT*GSJZMS2PAO1@*RF;-ITE2320LB.FSO?%'".$9E$UC2\2*&gt;J%KZ\[DR/HU[F-*B-:D]=S(!ZF-"B)P^_88K]HX7Z8WOWWN&amp;IN7&lt;=4_KI6[`&gt;3S@D$DR?PT_]^0PX]^PGX][@NUY]`0ZX7_NJ0=?&gt;,52Y`+]K4_U6:(K%2&lt;RY5Z&lt;N82@F_5:2HV]2S?6%__P/\@0DLIDT[`L75-_;^,.;V&lt;]?\5?\)[HOT2X]"!$-W@A!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">369131520</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"I-5F.31QU+!!.-6E.$4%*76Q!!&amp;&lt;A!!!25!!!!)!!!&amp;:A!!!!C!!!!!AV":WFM:7ZU,GRW&lt;'FC$U&amp;H;7RF&lt;H1O&lt;(:D&lt;'&amp;T=Q!!!!!!I"9!A!!!-!!!#!!%!!!!!!1!!Q!]!,Q!(U#!!A!!!!!"!!%!"P````]!!!!!!!!!!!!!!!"&amp;$M`BGA"Z1I(P,9D8!RN`!!!!$!!!!"!!!!!!`&gt;0.N0Y)&gt;EW&gt;R/,R-%Y@6N1&gt;D.G0!,)%[9!*G/TY1HY!!"!!!!!!!.*\)NTPGG2!A$,"S@XDY\)"!!!!`````^1&gt;D.G0!,)%[9!*G/TY1HY!!!!1`A2L].W#.5.V*`=H&gt;_MFBQ!!!!1!!!!!!!!!*Q!"4&amp;:$1Q!!!!%!!F:*4%)!!!!!5&amp;2)-!!!!!5!!1!"!!!!!!)!!Q!!!!!#!!%!!!!!!#=!!!!E?*RDY'2A;G#YQ!$%D%$-V-$U!]D_!/)T#(#!3!9'!,%3#;%!!!!!3!!!!2BYH'.AQ!4`A1")-4)Q-&amp;U!UCRIYG!;RK9GQ'5O,LOAYMR1.\*#2"G"9ER\A!QGE"R5$&gt;104'?!_!3[/=R9T!9!@_EI)A!!!!Q!!6:*2&amp;-!!!!!!!-!!!'3!!!#Y(C=G]$)Q*"J&lt;'(W!%AT!\%91Q.$=HZ++B=$E-]!!3?9'%A'!6$^7GDCBA=/JQ'"(L^]#ZD@`);HWU6&amp;I,F'29+J6+4&lt;2U7EUU?&amp;J:.&amp;Z=7@````.R`B/&gt;TNE80=U1;ENJM$+(\=295$R!(3,#$[@W!'3"8-P!#A;2Q.&amp;=I-*3S'"[)/(W]Q9929$$-S#NV_!;!/G?Y1I-6.-E!8!ZWAU.GF!'2"(1+UC!&gt;K)&gt;BCM#GN"`CH(/$@&gt;G)(S(2_VY0.2Q3!4H!"G==".%(AY%/7\E9.I&amp;TP2"!*&amp;/,J$/'1//\#I3-'Z$/?[!1[PZ-(ZEM/O0P$1!;5K!BUGI!M:G'%7&gt;D.&gt;NR"!_Q-"R%)F1'B+C"5!9D;!@&lt;2%9[YQ`$Q8PP[XCZ1/#&amp;(I1%1A^4L-4!S-),F'"FKI8)W1$946!Q7&gt;S#W!$19B:$UQ'T21"+$"&lt;="V%[1T"OI/B$\%Z1.MJ].+G9,V$-"SH9"MB/A&lt;']A_Q'5(12E#U$:E5#W!C/%(1&gt;F/`O\O#+H-VC;"1!CI)**!!!!!!!-&amp;A#!%!!!"$%W,D!!!!!!$"9!A!!!!!1R.CYQ!!!!!!Q7!)!1!!!%-49O-!!!!!!-&amp;A#!!!!!"$%W,D!!!!!!$"9!A"!!!!1R.CYQ!!!!!"1"!!!!^6=VAHEGIIQO=V*/"EEZH1!!!!U!!!!!!!!!!!!!!!!!!!!!!!!!A0````_!!!!"G/I[8;5+)UG^;D,*J3IC3;4LOEG!!!!"`````Y!!!!'!!!!"A!,!!9!)-!'!)!Q"A)!$!9!!!Q'!9!Y"A(A_!9"_`A'!@`Y"A(`_!9"``A'!@`Y"A(`_!9$``_'!(`XZA!`XY9!$X]'!!(]"A!!]!9!!!!(`````!!!#!0`````````````````````]T-T-T-T-T-T-T-T-T-T0`-`]T0`]`0T-``T]T]``T`T]T]`-T0T]T0T-`]`-`-`]```0T`T]`-T`T0T`T0T0`0T0T]T]`0T-`-T]T]T]T`T]T]T``0T``0`]`-`-`-`]T-T-T-T-T-T-T-T-T-T0``````````````````````!!!!!!!!!!!!!!!!!!!!`Q!!!!!!!!$-!!!!!!!!!0]!!!!!!!$0X`Q!!!!!!!$`!!!!!!$0X-T0`!!!!!!!`Q!!!!$0X-T-T-`]!!!!!0]!!!!0X-T-T-T-T`!!!!$`!!!!$&gt;T-T-T-T-`Q!!!!`Q!!!!X`X-T-T-``U!!!!0]!!!!.```=T-```^!!!!$`!!!!$@```^`````1!!!!`Q!!!!X`````````U!!!!0]!!!!.`````````^!!!!$`!!!!$@`````````1!!!!`Q!!!!X`````````U!!!!0]!!!!.`````````^!!!!$`!!!!$````````````Q!!`Q!!!!$&gt;```````^````!0]!!!!!!.`````^````!!$`!!!!!!!!X``]````]!!!`Q!!!!!!!!$=````]!!!!0]!!!!!!!!!!!``]!!!!!$`!!!!!!!!!!!!!!!!!!!!``````````````````````!!!%!0````````````````````````````````````````````DY_0DY_0DY_0DY_0DY_0DY_0DY_0DY_0DY_0DY_0``_0D```DY_0````D`_0`Y_0D````Y``DY``D````Y```Y``DY``D`_0DY_0`Y``DY_0`Y_0D```D`_0D`_0D```D`````_0`Y```Y``D`_0DY```Y_0`Y```Y_0`Y_0``_0`Y_0`Y``DY``D`_0`Y_0D`_0DY``DY``DY``DY```Y``DY``DY````_0`Y````_0````D`_0D`_0D`_0D```DY_0DY_0DY_0DY_0DY_0DY_0DY_0DY_0DY_0DY_0````````````````````````````````````````````]G*C9G*C9G*C9G*C9G*C9G*C9G*C9G*C9G*C9G*C&lt;``S9G*C9G*C9G*C9G*C9G+SMG*C9G*C9G*C9G*C9G*P``*C9G*C9G*C9G*C9G+`T[`0QL*C9G*C9G*C9G*C9G``]G*C9G*C9G*C9G+`T[_0DY_0T]+S9G*C9G*C9G*C&lt;``S9G*C9G*C9G+`T[_0DY_0DY_0D]`#MG*C9G*C9G*P``*C9G*C9G*PT[_0DY_0DY_0DY_0DY`0QG*C9G*C9G``]G*C9G*C9G_PLY_0DY_0DY_0DY_0D_`#9G*C9G*C&lt;``S9G*C9G*C&lt;[`0T[_0DY_0DY_0D_`P\[*C9G*C9G*P``*C9G*C9G*PL]`0T]_PDY_0D_`P\_`PIG*C9G*C9G``]G*C9G*C9G_PT]`0T]`0L]`P\_`P\__C9G*C9G*C&lt;``S9G*C9G*C&lt;[`0T]`0T]`0\_`P\_`P\[*C9G*C9G*P``*C9G*C9G*PL]`0T]`0T]`P\_`P\_`PIG*C9G*C9G``]G*C9G*C9G_PT]`0T]`0T_`P\_`P\__C9G*C9G*C&lt;``S9G*C9G*C&lt;[`0T]`0T]`0\_`P\_`P\[*C9G*C9G*P``*C9G*C9G*PL]`0T]`0T]`P\_`P\_`PIG*C9G*C9G``]G*C9G*C9G`0T]`0T]`0T_`P\_`P\]`+SML#9G*C&lt;``S9G*C9G*C9G_PL]`0T]`0\_`P\]`0KML+SML+QG*P``*C9G*C9G*C9G*PL]`0T]`P\]`0KML+SML+QG*C9G``]G*C9G*C9G*C9G*C&lt;[`0T]`0CML+SML+SM*C9G*C&lt;``S9G*C9G*C9G*C9G*C9G_PCML+SML+SM*C9G*C9G*P``*C9G*C9G*C9G*C9G*C9G*C;ML+SM*C9G*C9G*C9G``]G*C9G*C9G*C9G*C9G*C9G*C9G*C9G*C9G*C9G*C&lt;```````````````````````````````````````````]!!!!-!!&amp;'5%B1!!!!!!!$!!!!K29!A!!!!!!"!!1!!!!"!!!!!!!$!!!!#W.M98.T5X2S;7ZH&amp;A#!!!!!!!%!#!!Q`````Q!"!!!!!!!30SI[/CB*4F.55HR44U.,261J!!!!!!!!!!VE;8.Q&lt;'&amp;Z2GFM&gt;'6S&amp;A#!!!!!!!%!"1!(!!!"!!!!!3!!!!!!!!!!!!FU?8"F1WRB=X-7!)!!!!!!!1!)!$$`````!!%!!!!!!!6*&lt;H.U=A!!!!!!!!!!!!5/!!!+KXC=L6:.4"RF'(ZHG,5$B8;7NM!;S(YFMUC56&gt;.%7R'RJ50DGK;UI;#1K"VW"NAYT/$]A"BN9\)B1&gt;-4SM'EC3=]^M$";T5LGGT5HGRM44:Q][1RI:D;98S`G:X&gt;:=N09^R.:L_&gt;?:`H`8G?`&lt;Y&amp;K0N+;''XY+I,D,#"C`-OV#FZ"C#8Z+(Y[JA(99DZ"ZCD-=;&amp;U`S1=)`&gt;9FJ&gt;K&amp;@S(@TTUA,]C&gt;(?T^Y-,,/4QBK'0C(%E+T/B=.+PDH[OFA1R*ONYE)E:)V#GX#&gt;W7,@&amp;//&lt;`07MA1EBWUGPU33T"9RUH//S\2&gt;F8=W+^'ZNEI`ZF,5O#&amp;+_Q21,4S%DJP\7JW489*8^+[1%J/S%F:76-CA;A$L]-EZ2T#L3XG(8^M!U)M93#VU_JM\(9*Y\92YJPL6);[?A;OA2-9^)B,U@&gt;LQ;JJ,C\GE@NL[_DD#]&amp;G'4,BQ6#XV]D&amp;^4\E9;,G7`"A;9X$O]&gt;^_\4`(2#V1%0\92&gt;5BU-])ALA&gt;&gt;?$K&lt;:[]!&amp;_9["YO_#FSI1A_KQ*TT6;C6&amp;A1IS\#YDQQ3FWNP/[MZFKW;R"AH;5WW,$*N:G:E7S7+&lt;-O"1%)CK!V2)(9SW%3C5L.8J(TN34I1GN_X#RQ$$PJAMH,_"CQP,_.-]&amp;K'^C+U73S5=&amp;'`1;WEA6,7A":3(O;L/%TJ\]M4&gt;+#*&lt;H;\C^]NO`A%ODAK6*?VT=5P`$]OXG&gt;),ULZNKN6RA9&amp;EO$M9&gt;+4!;D#W%F%^9+S"_959KZN-X93]`3'?@9S^EN38KBS&gt;D,-68,WUN,3.BS+U6VS.M=QA&lt;/TN\W(XE0K\Q@?!XA:T!J`$[&amp;#&gt;%`Z!`F&lt;E:_K52/K99:OFG\',Z8NT+)?)Y^BZU-JX&lt;*.:UL6\&gt;&gt;U2&gt;N@GM^=M("0Z!7J10-&amp;0DF3\*R7&gt;M!0/_C#AVY^)29'!JP\#E:A!*LA3D#0;"SD7:L&amp;H`+H.%&amp;R`1E@KYU8%X\OQIS3*Z4K9-5P*A+(E'IA(,98^9@N?2[3Y\8#=S,^PG.,YKW[9J*[&amp;W:RA]-=TQ3\1DX.Q7+7'.18JV9KNY&lt;EYI(;T8!A3_AC?^S@7XOR@I&gt;P$\L:-&gt;3P:VNIK6EUV9&gt;30I+\!R05=9P7Q?'\#2J#/4&gt;W]_.(6&lt;M$@4V:.O4OOQ0_:PI3)Z!9$?_B@"^,_2A;^8"1"R`-H%.&lt;V3((LX&gt;`Q_4/`*@_6,B1O-U@SC*O@F_R8KV9@R@U3`?BGI\ZGP[X38/#Y2)CFXC$D@Q9_;F=VFOFMH$L_JV#B&amp;&amp;]]IU,U^%7KG=OZL%4:YCJDKOGKK&gt;69BP%HF2*JG2K8",(5J]FZ$,?@S416#X$-@'\,E]BT#*J5]7^8+%Q_LSHLT?F:_S-L'5_5-FQKO?ZPN[!#[E-5_UC9`YH3=O;FN%H%)1EQ[EO-G=Y2&amp;?2#402BXP3'=2U^"#,O@&amp;]E&gt;.W:E&lt;6ZLK)J7JKWP&lt;RU[9[ET%=3ZP$I*"(K?JCX$3GAGB(UZ++-;M4H)4T;*ZJUZAQZ;EJW=\1#D(6&lt;!:&lt;+29[H"I]5]6M/$;NG'"Z5RF&gt;VOA:O&amp;N&lt;N'`,'&lt;05^RQKQY[%@FR'HX&lt;M%K7&amp;:9\A[.+4-EJ1I;/#JSS?OM-J#ZEH6%RLEL%Z7H%Q^M?O_&lt;`76&gt;P#DS;[/4QZU&lt;]8+AY5`N[K5`J\S!_+EL&gt;8A.#0$L\GQD(J&lt;//;E%)8#XS+\R&gt;OY0G,2X%4@Y0@S0V3_A/:/V^#:G_TT?R&amp;]9MYX`)P85]IL1!!!!!!"!!!!#]!!!!%!!!!!!!!!!Q!!5*%3&amp;!!!!!!!!-!!!"C!!!!=HC=9W"AS"/190L(50?8A5HA+Z!B`:?"7&gt;#0]4=$![?@Q'%AT3AA#237`=P!,KA.&amp;N9_IMP"!!7K&lt;)Q=EBS("4H!-BQN'AT````H_(LE'FT&amp;%2]Y5W770)=%!"2C'1!!!!!!!!1!!!!(!!!$'!!!!!=!!!!B8WZJ8URB=X2,&lt;G^X&lt;E^X&lt;GFO:UR71WRB=X.$&lt;(6T&gt;'6S!!!!DB9!A!!!!!!"!!A!-0````]!!1!!!!!!=A!!!!-!(A!X`````Q!%&amp;A#!!!!!!!%!"!!!!!%!!!!!!!!!.%"Q!!Y&amp;37ZT&gt;()!!1!!&amp;A#!!!!!!!%!"!!!!!%!!!!!!!!/37ZT&gt;(*V&lt;76O&gt;%BO:'Q!!"B!5!!"!!%017&gt;J&lt;'6O&gt;#ZM&gt;G.M98.T!!%!!A!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'&amp;C4X*E:8)!!!!J&amp;A#!!!!!!!)!"1!(!!!-!%!!!@````]!!!!"!!%!!!!"!!!!!!!!!!!!!!!&lt;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'FN:8.U97VQ!!!!'29!A!!!!!!"!!5!"Q!!!1!!VD..N!!!!!!!!!!G4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B4'&amp;T&gt;%&amp;Q='RJ:725;7VF=X2B&lt;8!!!!!:&amp;A#!!!!!!!%!"1!(!!!"!!$7-UWU!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5?8"F2'6T9Q!!!)Y7!)!!!!!!!1!)!$$`````!!%!!!!!!()!!!!$!"Y!.`````]!""9!A!!!!!!"!!1!!!!"!!!!!!!!!$2!=!!/"5FO=X2S!!%!!"9!A!!!!!!"!!1!!!!"!!!!!!!!$EFO=X2S&gt;7VF&lt;H2)&lt;G2M!!!91&amp;!!!1!"$U&amp;H;7RF&lt;H1O&lt;(:D&lt;'&amp;T=Q!"!!)!!!!!!!!!(ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B5WF[:1!!!"E7!)!!!!!!!1!&amp;!!-!!!%!!!!!!!1!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B!!!!@B9!A!!!!!!$!"Y!.`````]!""9!A!!!!!!"!!1!!!!"!!!!!!!!!$2!=!!/"5FO=X2S!!%!!"9!A!!!!!!"!!1!!!!"!!!!!!!!$EFO=X2S&gt;7VF&lt;H2)&lt;G2M!!!91&amp;!!!1!"$U&amp;H;7RF&lt;H1O&lt;(:D&lt;'&amp;T=Q!"!!)!!!!!!!!!!!!!!!1!"!!,!!!!"!!!!)%!!!!I!!!!!A!!"!!!!!!-!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!2A!!!(N?*S.4UV,QU!1@?GG.L6N7D_K*S5(]?$"3Q7P%5(U)"40(IT:J!47J#3&lt;UG.`F,`&amp;X['`1&amp;^W+TUI[$Q9ZMU-&lt;_9"/-1R,D]:=!_Q!M/";\+.CX!/PXW86\JM?L`N_':;PS3ZPMWF!B\R]4;Z@_6)$+ZGG?,A8#V5^DT=M&amp;B&amp;6&gt;8\ZL&amp;7/!OH6(3/LF6&gt;[;1-CD1Q3]']T";24A):[1BN!NO1P0Y/13,7&amp;"/3&lt;NC"3.5-8LA3R9/WD;2)-;+[1!=?OK+7+5\QJS`UT5=^I:?3.[:I-@&gt;R_M]`7W$;;B,V0$R2U-5.:1)--?)T$8;-)1ORRM`/:L*L9!U\W'-6'VX\O-_D,G]/7/VDT/[9THS#WV^NAEML!!!!&gt;Q!"!!)!!Q!&amp;!!!!7!!0"!!!!!!0!.A!V1!!!'%!$Q1!!!!!$Q$9!.5!!!"K!!]%!!!!!!]!W!$6!!!!=Y!!B!#!!!!0!.A!V1!!!(7!!)1!A!!!$Q$9!.5)5W6H&lt;W5A65E)5W6H&lt;W5A65E)5W6H&lt;W5A65E"-A%Q!&amp;*45E-.#A!$4&amp;:$1UR#6F=!!"7Y!!!%6!!!!#!!!"79!!!!!!!!!!!!!!!A!!!!.!!!"%A!!!!&gt;4%F#4A!!!!!!!!&amp;M4&amp;:45A!!!!!!!!'!5F242Q!!!!!!!!'51U.46!!!!!!!!!'I4%FW;1!!!!!!!!']1U^/5!!!!!!!!!(16%UY-!!!!!!!!!(E2%:%5Q!!!!!!!!(Y4%FE=Q!!!!!!!!)-6EF$2!!!!!!!!!)A&gt;G6S=Q!!!!1!!!)U5U.45A!!!!!!!!+92U.15A!!!!!!!!+M35.04A!!!!!!!!,!;7.M.!!!!!!!!!,5;7.M/!!!!!!!!!,I4%FG=!!!!!!!!!,]5V23)!!!!!!!!!-12F")9A!!!!!!!!-E2F"421!!!!!!!!-Y6F"%5!!!!!!!!!.-4%FC:!!!!!!!!!.A1E2)9A!!!!!!!!.U1E2421!!!!!!!!/)6EF55Q!!!!!!!!/=2&amp;2)5!!!!!!!!!/Q466*2!!!!!!!!!0%3%F46!!!!!!!!!096E.55!!!!!!!!!0M2F2"1A!!!!!!!!1!!!!!!0````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!$`````!!!!!!!!!-Q!!!!!!!!!!0````]!!!!!!!!!Y!!!!!!!!!!!`````Q!!!!!!!!$I!!!!!!!!!!$`````!!!!!!!!!21!!!!!!!!!!0````]!!!!!!!!"(!!!!!!!!!!!`````Q!!!!!!!!&amp;)!!!!!!!!!!$`````!!!!!!!!!:1!!!!!!!!!!0````]!!!!!!!!"J!!!!!!!!!!%`````Q!!!!!!!!-]!!!!!!!!!!@`````!!!!!!!!!UQ!!!!!!!!!#0````]!!!!!!!!$8!!!!!!!!!!*`````Q!!!!!!!!.M!!!!!!!!!!L`````!!!!!!!!!XQ!!!!!!!!!!0````]!!!!!!!!$D!!!!!!!!!!!`````Q!!!!!!!!/E!!!!!!!!!!$`````!!!!!!!!!\A!!!!!!!!!!0````]!!!!!!!!%0!!!!!!!!!!!`````Q!!!!!!!!:!!!!!!!!!!!$`````!!!!!!!!#E1!!!!!!!!!!P````]!!!!!!!!+6!!!!!!!!!!!`````Q!!!!!!!!M%!!!!!!!!!!$`````!!!!!!!!%"A!!!!!!!!!!0````]!!!!!!!!1)!!!!!!!!!!!`````Q!!!!!!!"!I!!!!!!!!!!$`````!!!!!!!!%$A!!!!!!!!!!0````]!!!!!!!!1I!!!!!!!!!!!`````Q!!!!!!!"#I!!!!!!!!!!$`````!!!!!!!!%]1!!!!!!!!!!0````]!!!!!!!!4T!!!!!!!!!!!`````Q!!!!!!!"05!!!!!!!!!!$`````!!!!!!!!&amp;!!!!!!!!!!!A0````]!!!!!!!!6(!!!!!!,17&gt;J&lt;'6O&gt;#ZD&gt;'Q!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!AV":WFM:7ZU,GRW&lt;'FC$U&amp;H;7RF&lt;H1O&lt;(:D&lt;'&amp;T=V"53$!!!!!!!!!!!!!!!!!!!A!"!!!!!!!!!1!!!!-!(A!X`````Q!%&amp;A#!!!!!!!%!"!!!!!%!!!!!!!!!.%"Q!!Y&amp;37ZT&gt;()!!1!!&amp;A#!!!!!!!%!"!!!!!%!!!!!!!!/37ZT&gt;(*V&lt;76O&gt;%BO:'Q!!"B!5!!"!!%017&gt;J&lt;'6O&gt;#ZM&gt;G.M98.T!!%!!A!!!!!!!!!!!!!!!!)+2%V.,GRW&lt;'FC=!N%45UO&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!!!!!!!!!&amp;A#!!!!!!!!!!!!!!!!!!!%!!!!!!!!!!!!!!Q!?!$@`````!!17!)!!!!!!!1!%!!!!!1!!!!!!!!!U1(!!$A6*&lt;H.U=A!"!!!7!)!!!!!!!1!%!!!!!1!!!!!!!!Z*&lt;H.U=H6N:7ZU3'ZE&lt;!!!'%"1!!%!!1^":WFM:7ZU,GRW9WRB=X-!!1!#!!!!!@````Y!!!!!!!!!!!)+2%V.,GRW&lt;'FC=!N%45UO&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!!!!!!!!!&amp;A#!!!!!!!!!!!!!!!%!!!!017&gt;J&lt;'6O&gt;#ZM&gt;G.M98.T</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">true</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"4!!!!!AJ%45UO&lt;(:M;7*Q#U2.43ZM&gt;G.M98.T!&amp;"53$!!!!!P!!%!#!!!!!!!$E2F&gt;C"$&lt;WVQ&lt;WZF&lt;H2T#E2.43ZM&gt;GRJ9H!,2%V.,GRW9WRB=X-!!!!!</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="Agilent.ctl" Type="Class Private Data" URL="Agilent.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Clusters" Type="Folder">
		<Item Name="SerialConfiguration --cluster.ctl" Type="VI" URL="../SerialConfiguration --cluster.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#J!!!!"!!01!=!#5*B&gt;71A5G&amp;U:1!.1!9!"F"B=GFU?1!!$U!'!!F%982B)%*J&gt;(-!&gt;A$R!!!!!!!!!!-22%V.8U&amp;H;7RF&lt;H1O&lt;(:M;7)42%V.8U&amp;H;7RF&lt;H1O&lt;(:D&lt;'&amp;T=S&amp;4:8*J97R$&lt;WZG;7&gt;V=G&amp;U;7^O)#UN9WRV=X2F=CZD&gt;'Q!*E"1!!-!!!!"!!)55W6S;7&amp;M)%.P&lt;G:J:X6S982J&lt;WY!!!%!!Q!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
		</Item>
	</Item>
	<Item Name="Low Level Config" Type="Folder">
		<Item Name="ConfigureMultiPoint.vi" Type="VI" URL="../ConfigureMultiPoint.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(`!!!!$A!U1(!!(A!!(QV":WFM:7ZU,GRW&lt;'FC$U&amp;H;7RF&lt;H1O&lt;(:D&lt;'&amp;T=Q!+17&gt;J&lt;'6O&gt;#"J&lt;A!!'5!$!"*1=G6U=GFH:W6S)&amp;.B&lt;8"M:8-!!'Y!]1!!!!!!!!!$#E2.43ZM&gt;GRJ9H!,2%V.,GRW9WRB=X-85W&amp;N='RF5W^V=G.F)#UN:7ZV&lt;3ZD&gt;'Q!.U!7!!).6(*J:W&gt;F=C"%:7RB?1^497VQ&lt;'5A37ZU:8*W97Q!$6.B&lt;8"M:3"4&lt;X6S9W5!"!!!!$2!=!!?!!!@$5&amp;H;7RF&lt;H1O&lt;(:M;7)017&gt;J&lt;'6O&gt;#ZM&gt;G.M98.T!!N":WFM:7ZU)'^V&gt;!!41!-!$62S;7&gt;H:8)A1W^V&lt;H1!%U!$!!R497VQ&lt;'5A1W^V&lt;H1!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!#"!5!!$!!=!#!!*%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;5!+!!^497VQ&lt;'5A37ZU:8*W97Q!&amp;E"1!!-!"Q!)!!E*:8*S&lt;X)A&lt;X6U!'Q!]!!1!!!!!1!#!!-!"!!&amp;!!-!"A!$!!-!!Q!+!!M!!Q!$!!Q$!!%)!!#3!!!!#!!!!!A!!!!!!!!!D1!!!"!!!!!!!!!!%!!!!!!!!!!!!!!!!!!!!!I!!!!1!!!!!!!!!!!!!!!.#Q!!!!%!$1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">33554432</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342972432</Property>
		</Item>
		<Item Name="ConfigureTrigger.vi" Type="VI" URL="../ConfigureTrigger.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!):!!!!$A!U1(!!(A!!(QV":WFM:7ZU,GRW&lt;'FC$U&amp;H;7RF&lt;H1O&lt;(:D&lt;'&amp;T=Q!+17&gt;J&lt;'6O&gt;#"J&lt;A!!'5!+!"**&lt;H2F=GZB&lt;#"-:8:F&lt;#!I-#E!!"F!"A!45WRP='5A+$![)%ZF:W&amp;U;8:F+1!%!!!!.%"Q!"Y!!"].17&gt;J&lt;'6O&gt;#ZM&gt;GRJ9A^":WFM:7ZU,GRW9WRB=X-!#U&amp;H;7RF&lt;H1A&lt;X6U!']!]1!!!!!!!!!$#E2.43ZM&gt;GRJ9H!,2%V.,GRW9WRB=X-96(*J:W&gt;F=F.P&gt;8*D:3!N,76O&gt;7UO9X2M!$&gt;!&amp;A!$#5FN&lt;76E;7&amp;U:1B&amp;?(2F=GZB&lt;!B4&lt;W:U&gt;W&amp;S:1!/6(*J:W&gt;F=C"4&lt;X6S9W5!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!#"!5!!$!!9!"Q!)%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!)E!B(56O97*M:3""&gt;82P)%2F&lt;'&amp;Z)#B5/C"&amp;&lt;G&amp;C&lt;'5J!"F!#A!46(*J:W&gt;F=C"%:7RB?3!I=W6D+1!71&amp;!!!Q!'!!=!#!FF=H*P=C"P&gt;81!&lt;!$Q!"!!!!!"!!)!!Q!%!!5!!Q!$!!-!!Q!$!!E!#A!,!!-!$!-!!1A!!*)!!!!)!!!!#!!!!!!!!!#.!!!!#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!A!!!!)!!!!!!!!!!U,!!!!!1!.!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">33554432</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342972432</Property>
		</Item>
		<Item Name="FetchMultiPoint.vi" Type="VI" URL="../FetchMultiPoint.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'+!!!!$1!U1(!!(A!!(QV":WFM:7ZU,GRW&lt;'FC$U&amp;H;7RF&lt;H1O&lt;(:D&lt;'&amp;T=Q!+17&gt;J&lt;'6O&gt;#"J&lt;A!!"!!!!$2!=!!?!!!@$5&amp;H;7RF&lt;H1O&lt;(:M;7)017&gt;J&lt;'6O&gt;#ZM&gt;G.M98.T!!N":WFM:7ZU)'^V&gt;!!61!-!$UZV&lt;7*F=C"U&lt;S"':82D;!!:1!-!%UVB?'FN&gt;7UA6'FN:3!I&lt;8.F9SE!"1!+!!!;1%!!!@````]!"1R.:7&amp;T&gt;8*F&lt;76O&gt;(-!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!#"!5!!$!!=!#!!*%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;E"1!!-!"Q!)!!E*:8*S&lt;X)A&lt;X6U!'Q!]!!1!!!!!1!"!!%!!A!$!!%!"!!'!!%!!1!+!!%!!1!"!!M$!!%)!!#3!!!!!!!!!!!!!!!!!!!!D1!!!!A!!!!!!!!!#!!!!!E!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!!!!!!!.#Q!!!!%!$!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">33554432</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342972432</Property>
		</Item>
		<Item Name="InitiateMeasurement.vi" Type="VI" URL="../InitiateMeasurement.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%^!!!!#1!U1(!!(A!!(QV":WFM:7ZU,GRW&lt;'FC$U&amp;H;7RF&lt;H1O&lt;(:D&lt;'&amp;T=Q!+17&gt;J&lt;'6O&gt;#"J&lt;A!!"!!!!$2!=!!?!!!@$5&amp;H;7RF&lt;H1O&lt;(:M;7)017&gt;J&lt;'6O&gt;#ZM&gt;G.M98.T!!N":WFM:7ZU)'^V&gt;!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!A1&amp;!!!Q!$!!1!"2.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!":!5!!$!!-!"!!&amp;#76S=G^S)'^V&gt;!"M!0!!%!!!!!%!!1!"!!)!!1!"!!%!!1!"!!%!"A!"!!%!!1!(!Q!"#!!!E!!!!!!!!!!!!!!!!!!!!)U!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!!!!!!$1M!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">33554432</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342972432</Property>
		</Item>
		<Item Name="SendSoftwareTrigger.vi" Type="VI" URL="../SendSoftwareTrigger.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%^!!!!#1!U1(!!(A!!(QV":WFM:7ZU,GRW&lt;'FC$U&amp;H;7RF&lt;H1O&lt;(:D&lt;'&amp;T=Q!+17&gt;J&lt;'6O&gt;#"J&lt;A!!"!!!!$2!=!!?!!!@$5&amp;H;7RF&lt;H1O&lt;(:M;7)017&gt;J&lt;'6O&gt;#ZM&gt;G.M98.T!!N":WFM:7ZU)'^V&gt;!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!A1&amp;!!!Q!$!!1!"2.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!":!5!!$!!-!"!!&amp;#76S=G^S)'^V&gt;!"M!0!!%!!!!!%!!1!"!!)!!1!"!!%!!1!"!!%!"A!"!!%!!1!(!Q!"#!!!E!!!!!!!!!!!!!!!!!!!!)U!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!!!!!!$1M!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
	</Item>
	<Item Name="Protected" Type="Folder">
		<Item Name="Agilent_Initialize.vi" Type="VI" URL="../Agilent_Initialize.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(\!!!!%!!U1(!!(A!!(QV":WFM:7ZU,GRW&lt;'FC$U&amp;H;7RF&lt;H1O&lt;(:D&lt;'&amp;T=Q!+17&gt;J&lt;'6O&gt;#"J&lt;A!!$U!(!!F#986E)&amp;*B&gt;'5!$5!'!!:198*J&gt;(E!!!^!"A!*2'&amp;U93"#;82T!'=!]1!!!!!!!!!$#E2.43ZM&gt;GRJ9H!,2%V.,GRW9WRB=X-B5W6S;7&amp;M1W^O:GFH&gt;8*B&gt;'FP&lt;C!N,7.M&gt;8.U:8)O9X2M!#:!5!!$!!%!!A!$&amp;&amp;.F=GFB&lt;#"$&lt;WZG;7&gt;V=G&amp;U;7^O!!!%!!!!.%"Q!"Y!!"].17&gt;J&lt;'6O&gt;#ZM&gt;GRJ9A^":WFM:7ZU,GRW9WRB=X-!#U&amp;H;7RF&lt;H1A&lt;X6U!"2!-0````]+37ZT&gt;(*V&lt;76O&gt;!!!$E!B#%F%)&amp;&amp;V:8*Z!!!+1#%&amp;5G6T:81!$%!B"H.U982V=Q!!#U!$!!2D&lt;W2F!!!11$$`````"H.P&gt;8*D:1!!)%"1!!-!#A!,!!Q4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71&amp;!!!Q!+!!M!$!FF=H*P=C"P&gt;81!&lt;!$Q!"!!!!!%!!5!"1!'!!=!"1!)!!5!#1!&amp;!!U!"1!&amp;!!5!$A-!!1A!!*)!!!!)!!!!!!!!!!!!!!#.!!!!%!!!!!!!!!!)!!!!!!!!!!A!!!!!!!!!#A!!!!!!!!!!!!!!!!!!!!U,!!!!!1!0!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143248</Property>
		</Item>
	</Item>
	<Item Name="ConfigureAutoZero.vi" Type="VI" URL="../ConfigureAutoZero.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;-!!!!#A!U1(!!(A!!(QV":WFM:7ZU,GRW&lt;'FC$U&amp;H;7RF&lt;H1O&lt;(:D&lt;'&amp;T=Q!+17&gt;J&lt;'6O&gt;#"J&lt;A!!"!!!!$2!=!!?!!!@$5&amp;H;7RF&lt;H1O&lt;(:M;7)017&gt;J&lt;'6O&gt;#ZM&gt;G.M98.T!!N":WFM:7ZU)'^V&gt;!!01!9!#%&amp;V&gt;'^[:8*P!!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!A1&amp;!!!Q!%!!5!"B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!":!5!!$!!1!"1!'#76S=G^S)'^V&gt;!"M!0!!%!!!!!%!!1!"!!)!!Q!"!!%!!1!"!!%!"Q!"!!%!!1!)!Q!"#!!!EA!!!!!!!!!!!!!!!!!!!)U!!!!1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!!!!!!$1M!!!!"!!E!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
	</Item>
	<Item Name="ConfigureOffsetCompensation.vi" Type="VI" URL="../ConfigureOffsetCompensation.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'I!!!!#A!U1(!!(A!!(QV":WFM:7ZU,GRW&lt;'FC$U&amp;H;7RF&lt;H1O&lt;(:D&lt;'&amp;T=Q!+17&gt;J&lt;'6O&gt;#"J&lt;A!!"!!!!$2!=!!?!!!@$5&amp;H;7RF&lt;H1O&lt;(:M;7)017&gt;J&lt;'6O&gt;#ZM&gt;G.M98.T!!N":WFM:7ZU)'^V&gt;!"L!0%!!!!!!!!!!QJ%45UO&lt;(:M;7*Q#U2.43ZM&gt;G.M98.T)&amp;.F&gt;%^G:H.F&gt;%.P&lt;8"F&lt;H.B&gt;'FP&lt;C!N,76O&gt;7UO9X2M!#N!&amp;A!#!U^G:A*0&lt;A!!&amp;V.F&gt;#"0:G:T:81A1W^N='6O=W&amp;U;7^O!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!#"!5!!$!!1!"1!'%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;E"1!!-!"!!&amp;!!9*:8*S&lt;X)A&lt;X6U!'Q!]!!1!!!!!1!"!!%!!A!$!!%!!1!"!!%!!1!(!!%!!1!"!!A$!!%)!!#3!!!!!!!!!!!!!!!!!!!!D1!!!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!!!!!!!.#Q!!!!%!#1!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
	</Item>
	<Item Name="ConfigureMeasurement.vi" Type="VI" URL="../ConfigureMeasurement.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*[!!!!$1!U1(!!(A!!(QV":WFM:7ZU,GRW&lt;'FC$U&amp;H;7RF&lt;H1O&lt;(:D&lt;'&amp;T=Q!+17&gt;J&lt;'6O&gt;#"J&lt;A!!"!!!!$2!=!!?!!!@$5&amp;H;7RF&lt;H1O&lt;(:M;7)017&gt;J&lt;'6O&gt;#ZM&gt;G.M98.T!!N":WFM:7ZU)'^V&gt;!#=!0%!!!!!!!!!!QJ%45UO&lt;(:M;7*Q#U2.43ZM&gt;G.M98.T%U:V&lt;G.U;7^O)#UN:7ZV&lt;3ZD&gt;'Q!;5!7!!9+2%-A6G^M&gt;'&amp;H:1J"1S"7&lt;WRU97&gt;F#E2$)%.V=H*F&lt;H1+15-A1X6S=G6O&gt;"-S)#UA6WFS:3"3:8.J=X2B&lt;G.F%T1A,3"8;8*F)&amp;*F=WFT&gt;'&amp;O9W5!#%:V&lt;G.U;7^O!!"U!0%!!!!!!!!!!QJ%45UO&lt;(:M;7*Q#U2.43ZM&gt;G.M98.T&amp;6*F=W^M&gt;82J&lt;WYA,3VF&lt;H6N,G.U&lt;!!`1"9!"15T)$%P-A5U)$%P-A5V)$%P-A5W)$%P-A5X)$%P-A!55G6T&lt;WRV&gt;'FP&lt;C"J&lt;C"%;7&gt;J&gt;(-!!!N!#A!&amp;5G&amp;O:W5!$%!B"H.U982V=Q!!#U!$!!2D&lt;W2F!!!11$$`````"H.P&gt;8*D:1!!)%"1!!-!"A!(!!A4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!C1#%&gt;27ZB9GRF)%&amp;V&gt;']A5G&amp;O:W5A+&amp;1[)%6O97*M:3E!&amp;E"1!!-!"A!(!!A*:8*S&lt;X)A&lt;X6U!'Q!]!!1!!!!!1!"!!%!!A!$!!%!"!!"!!5!!1!*!!I!!1!"!!M$!!%)!!#3!!!!!!!!!!!!!!!!!!!!D1!!!!A!!!!!!!!!#!!!!!!!!!!+!!!!!!!!!!I!!!!)!!!!!!!!!!!!!!!.#Q!!!!%!$!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">33554432</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
	</Item>
	<Item Name="Close.vi" Type="VI" URL="../Close.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%^!!!!#1!U1(!!(A!!(QV":WFM:7ZU,GRW&lt;'FC$U&amp;H;7RF&lt;H1O&lt;(:D&lt;'&amp;T=Q!+17&gt;J&lt;'6O&gt;#"J&lt;A!!"!!!!$2!=!!?!!!@$5&amp;H;7RF&lt;H1O&lt;(:M;7)017&gt;J&lt;'6O&gt;#ZM&gt;G.M98.T!!N":WFM:7ZU)'^V&gt;!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!A1&amp;!!!Q!$!!1!"2.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!":!5!!$!!-!"!!&amp;#76S=G^S)'^V&gt;!"M!0!!%!!!!!%!!1!"!!)!!1!"!!%!!1!"!!%!"A!"!!%!!1!(!Q!"#!!!E!!!!!!!!!!!!!!!!!!!!)U!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!!!!!!#1!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082143248</Property>
	</Item>
	<Item Name="NameAndVersionNumber.vi" Type="VI" URL="../NameAndVersionNumber.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;&lt;!!!!#A!U1(!!(A!!(QV":WFM:7ZU,GRW&lt;'FC$U&amp;H;7RF&lt;H1O&lt;(:D&lt;'&amp;T=Q!+17&gt;J&lt;'6O&gt;#"J&lt;A!!"!!!!$2!=!!?!!!@$5&amp;H;7RF&lt;H1O&lt;(:M;7)017&gt;J&lt;'6O&gt;#ZM&gt;G.M98.T!!N":WFM:7ZU)'^V&gt;!!?1$$`````&amp;%ZB&lt;76"&lt;G27:8*T;7^O4H6N9G6S!!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!A1&amp;!!!Q!%!!5!"B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!":!5!!$!!1!"1!'#76S=G^S)'^V&gt;!"M!0!!%!!!!!%!!1!"!!)!!1!"!!%!!Q!"!!%!"Q!"!!%!!1!)!Q!"#!!!EA!!!!!!!!!!!!!!!!!!!)U!!!!!!!!!!!!!!!!!!!!*!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!!!!!!$1M!!!!"!!E!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">33554432</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
	</Item>
	<Item Name="ReadMultiPoint.vi" Type="VI" URL="../ReadMultiPoint.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'+!!!!$1!U1(!!(A!!(QV":WFM:7ZU,GRW&lt;'FC$U&amp;H;7RF&lt;H1O&lt;(:D&lt;'&amp;T=Q!+17&gt;J&lt;'6O&gt;#"J&lt;A!!"!!!!$2!=!!?!!!@$5&amp;H;7RF&lt;H1O&lt;(:M;7)017&gt;J&lt;'6O&gt;#ZM&gt;G.M98.T!!N":WFM:7ZU)'^V&gt;!!61!-!$EZV&lt;7*F=C"U&lt;S"3:7&amp;E!!!&amp;!!I!!"J!1!!"`````Q!%$%VF98.V=G6N:7ZU=Q!!$%!B"H.U982V=Q!!#U!$!!2D&lt;W2F!!!11$$`````"H.P&gt;8*D:1!!)%"1!!-!"A!(!!A4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!:1!-!%UVB?'FN&gt;7UA6'FN:3!I&lt;8.F9SE!&amp;E"1!!-!"A!(!!A*:8*S&lt;X)A&lt;X6U!'Q!]!!1!!!!!1!"!!%!!A!$!!%!!1!&amp;!!%!!1!*!!I!!1!"!!M$!!%)!!#3!!!!!!!!!!!!!!!!!!!!D1!!!!A!!!!!!!!!!!!!!!E!!!!!!!!!!!!!!!I!!!!)!!!!!!!!!!!!!!!.#Q!!!!%!$!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342972432</Property>
	</Item>
	<Item Name="ReadSinglePoint.vi" Type="VI" URL="../ReadSinglePoint.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;H!!!!#Q!U1(!!(A!!(QV":WFM:7ZU,GRW&lt;'FC$U&amp;H;7RF&lt;H1O&lt;(:D&lt;'&amp;T=Q!+17&gt;J&lt;'6O&gt;#"J&lt;A!!"!!!!$2!=!!?!!!@$5&amp;H;7RF&lt;H1O&lt;(:M;7)017&gt;J&lt;'6O&gt;#ZM&gt;G.M98.T!!N":WFM:7ZU)'^V&gt;!!21!I!#UVF98.V=G6N:7ZU!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!#"!5!!$!!1!"1!'%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!'5!$!"..98BJ&lt;86N)&amp;2J&lt;75A+'VT:7-J!":!5!!$!!1!"1!'#76S=G^S)'^V&gt;!"M!0!!%!!!!!%!!1!"!!)!!1!"!!%!!Q!"!!%!"Q!)!!%!!1!*!Q!"#!!!EA!!!!!!!!!!!!!!!!!!!)U!!!!!!!!!!!!!!!!!!!!*!!!!!!!!!!!!!!!+!!!!%!!!!!!!!!!!!!!!$1M!!!!"!!I!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
	</Item>
</LVClass>
