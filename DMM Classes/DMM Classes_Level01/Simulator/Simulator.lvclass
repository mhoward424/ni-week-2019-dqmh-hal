﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="16008000">
	<Property Name="EndevoGOOP_ClassItemIcon" Type="Str">BurgundyCircle</Property>
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">10386594</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorProtected" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">16767648</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">16448250</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.ContainingLib" Type="Str">DMM_Simulator.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../DMM_Simulator.lvlib</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,2!!!*Q(C=T:3^&lt;2N"%)8@71;M1)$"#GSI!AG4'H$#&amp;JB&gt;0#UQV)6K96IAW)%3/:]7"--.%%Z.(%B`N\=326)3%RP1,O&gt;_XJO&gt;`&lt;C\J&amp;4;O@22WV/N@&lt;0B5[E&gt;?DN?[_-A%NPN]$;)Y_@2(Z^;F@'DK0&lt;1,_0&lt;2\=^]MNY;4&gt;J/R;4HN=`Q8_KH&lt;`:$PXL[\@][X\&gt;^^?P_&lt;A$UF\'TK`O1=;4PX0(D%.`P?&gt;PDPV_6W'TW;R@]'P':P/K4U;`?&gt;/P^PPT_TW`\Z@,Z@\[L^&gt;00F^T7&gt;L_`KV(`ZF\O0]FIVXO\+0TM^ZTDXX;-`&gt;&amp;@[_&gt;0.\]7,L40Y,`W[2'QR]2E5114FDZ&gt;7]40&gt;%40&gt;%40&gt;%$0&gt;!$0&gt;!$0&gt;!&gt;X&gt;%&gt;X&gt;%&gt;X&gt;%.X&gt;!.X&gt;!.X&gt;",2R?[U)8/KC34*R-F2:-#34)I3CY*4]+4]#1]P#LB38A3HI1HY3&amp;&amp;#5`#E`!E0!E0QZ4Q*$Q*4]+4]&amp;#KE'4J[0!E0*28Q"0Q"$Q"4]$$F!JY!I"AMK"Q5!1-"7&lt;Q%0!%0!%0DQJY!J[!*_!*?,!6]!1]!5`!%`!QJ+R+&amp;*KBI].$'4E]$I`$Y`!Y0*37Q_0Q/$Q/D]0$&gt;(*Y("Y(QJH1+1[#H%&amp;/AP0C]$A]X/4Q/$Q/D]0D]'#6(@+S-A0.U.(B-8A-(I0(Y$&amp;Y+#'$R_!R?!Q?AY?S-HA-(I0(Y$&amp;YG%I'D]&amp;D]"AARK2-,[/9-&gt;")-A3$BU]Z,6:W+1K*F3\6Q[N[+&amp;50G_IB5DU=KJOOOJGKG[3[_+K,KLJ9KIOA_O65I66B6#&gt;2(4QE;M8VA&lt;AD&amp;M1N-3&gt;GR*3Y*#&lt;$U(_=O&amp;KN^0$QI,O\/SU7#^X?XGI_HWMWGWE[H?LS]F+4S530\3N^&lt;-XD`V,(]WORZZ`&gt;.^X&amp;6&gt;.&gt;844&gt;`&gt;E9PT]X8@_^I&gt;"L];O\W@\JPH$`^0.(V^R1[^N4\@@RX[A0'O^0;`18!C:2)A!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">369131520</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.2</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!":'5F.31QU+!!.-6E.$4%*76Q!!%B!!!!1W!!!!)!!!%@!!!!!K!!!!!B.%45V@5WFN&gt;7RB&gt;'^S,GRW&lt;'FC%6.J&lt;86M982P=CZM&gt;G.M98.T!!!!!!#A&amp;A#!!!!Q!!!)!!1!!!!!"!!$!$Q!P!!@1)!#!!!!!!%!!1!'`````Q!!!!!!!!!!!!!!!)$`&gt;A*Q*_F,C#0"^?+9A2!!!!!-!!!!%!!!!!$*!T#)L0&gt;,4*!3H]'24EZ+V"W-W9]!MA4JA!G9\0B#@A!!%!!!!!!!&gt;UHR!A,`\%;:/-':&lt;+N-8A%!!!$`````V"W-W9]!MA4JA!G9\0B#@A!!!"$XZ_.[7N#X\[GST/&amp;H0(A5!!!!"!!!!!!!!!!H!!&amp;-6E.$!!!!!1!#6EF-1A!!!!"16%AQ!!!!"1!"!!%!!!!!!A!$!!!!!!)!!1!!!!!!*Q!!!#2YH'0A:'"K9,D!!-3-1-T5Q01$S0Y!YD-)=)")"A9!M2)*I1!!!!")!!!"'(C=9W$!"0_"!%AR-D!Q81$3,'DC9"L'JC&lt;!:3YOO[$CT&amp;!XME*%'9&amp;C4(O!$#;1(&amp;1.V!^-:Y$Y",IZT&amp;D-"A"`[3AC!!!!$!!"6EF%5Q!!!!!!!Q!!!9=!!!,1?*RL9'2AS$3W-,M!J*G"7)SBA3%Z0S76CQ():Y#!(5Q-*)-!K(YN.(($!Y@4A%#08\Y&amp;T'^_Q^0NIC,18+-CQ61KUOWD)N,JI],3S;,SYM````_&lt;D`!=\P&lt;)/?ZI!V,&lt;T1%50_[CQA(C!'E7%0U`-!/E#G:?!.!UDI9+:993&amp;M-$59?0.ZAQ1CS''2G&amp;&lt;D](5!@)9J\/2BGAC\N$6#1[*SK!7!T&gt;8#C/_=()0_5A`\;4`#YH/E]U(_%!7AL7\[-C=0!B3X?D"N#]XIEA%GR=#)@%=2=/(4%AH`&amp;%*^#ZH4QQ8X(!X2M'-K"%2;$4"/1T&amp;J"DQ7K[W9Y\;)$^[3!#I4)A6!7%+A"($.A(2TDC$M0$&gt;_XL?\N!Y9)=:1:!$&amp;+PR]$)Q!C79W3IB=L:!.F-5$&amp;98)(9$."A?YGE:TN58A.*4)5291@)(*$-(;A[%0M2F!WSHQUKJAP5-Q(+"E60!J2N$72@A,+&gt;A'Q"+.M4R';%M0WA&lt;'&gt;`&amp;V@E&gt;!6,IQ"ACI%D!!!!!!Q7!)!1!!!%-49O-!!!!!!-&amp;A#!!!!!"$%W,D!!!!!!$"9!A"!!!!1R.CYQ!!!!!!Q7!)!!!!!%-49O-!!!!!!-&amp;A#!%!!!"$%W,D!!!!!!&amp;!%!!!$V6T7#?3;CD#ZT5EY'34G&gt;!!!!$1!!!!!!!!!!!!!!!!!!!!!!!!#!`````Y!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!(`````A!!!!9!!!!'!!]!"A!QQ!9!Q$!'!Q!-"A-!$!9$Q$Q'!`$]"A0``!9$``Q'!``]"A0``!9$``Q'!``]"A0``Y9"```G!(``BA!@@Q9!"@Q'!!$Q"A!!!!@````]!!!)!``````````````````````X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;`^!!U.$&gt;U.$&gt;$1X&gt;!!U!$@].X&gt;$1$1$1X1U.X&gt;$&gt;$&gt;$`U!X1U.$1U.U.$&gt;X1X1!.`^X1U.$&gt;U.$&gt;$1X&gt;U.U.U0]!$&gt;$1X&gt;$&gt;!.U!$&gt;$&gt;$&gt;$`X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;``````````````````````]!!!!!!!!!!!!!!!!!!!$`!!!!!!!!!-Q!!!!!!!!!`Q!!!!!!!-`@`!!!!!!!!0]!!!!!!-`&gt;X&gt;`]!!!!!!$`!!!!!-`&gt;X&gt;X&gt;X`Q!!!!!`Q!!!!`&gt;X&gt;X&gt;X&gt;X@]!!!!0]!!!!.X&gt;X&gt;X&gt;X&gt;X`!!!!$`!!!!$@`&gt;X&gt;X&gt;X``1!!!!`Q!!!!X``^X&gt;X```U!!!!0]!!!!.````X````^!!!!$`!!!!$@`````````1!!!!`Q!!!!X`````````U!!!!0]!!!!.`````````^!!!!$`!!!!$@`````````1!!!!`Q!!!!X`````````U!!!!0]!!!!0````````````!!$`!!!!!.X```````X```]!`Q!!!!!!X`````X```]!!0]!!!!!!!$@``X````Q!!$`!!!!!!!!!.X````Q!!!!`Q!!!!!!!!!!$``Q!!!!!0]!!!!!!!!!!!!!!!!!!!$`````````````````````]!!!1!````````````````````````````````````````````8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=``^=!!!!8!"=!&amp;R=8!"=!&amp;R=!&amp;Q!8&amp;R=!!!!8!!!!&amp;T``Q"=8&amp;R=!&amp;Q!!&amp;Q!!&amp;Q!8&amp;Q!8!"=8&amp;R=!&amp;R=!&amp;R=!0``8!!!8&amp;Q!8!"=!&amp;Q!8!"=8!"=!&amp;R=8&amp;Q!8&amp;Q!!!"=``^=8&amp;Q!8!"=!&amp;R=8!"=!&amp;R=!&amp;Q!8&amp;R=8!"=8!"=8!$``Q!!!&amp;R=!&amp;Q!8&amp;R=!&amp;R=!!"=8!!!!&amp;R=!&amp;R=!&amp;R=!0``8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=`````````````````````````````````````````````QA)#!A)#!A)#!A)#!A)#!A)#!A)#!A)#!A)#!A)#0``#!A)#!A)#!A)#!A)#!AR-1A)#!A)#!A)#!A)#!A)``])#!A)#!A)#!A)#!AR`0L]`$%)#!A)#!A)#!A)#!D``QA)#!A)#!A)#!AR`0J=8&amp;R=`0QR#!A)#!A)#!A)#0``#!A)#!A)#!AR`0J=8&amp;R=8&amp;R=80T]-1A)#!A)#!A)``])#!A)#!A)`0J=8&amp;R=8&amp;R=8&amp;R=8&amp;T]`!A)#!A)#!D``QA)#!A)#!D[_FR=8&amp;R=8&amp;R=8&amp;R=80\]#!A)#!A)#0``#!A)#!A)#0L]`0J=8&amp;R=8&amp;R=80\_`PI)#!A)#!A)``])#!A)#!A)_PT]`0T[8&amp;R=80\_`P\__AA)#!A)#!D``QA)#!A)#!D[`0T]`0T]_PT_`P\_`P\[#!A)#!A)#0``#!A)#!A)#0L]`0T]`0T]`P\_`P\_`PI)#!A)#!A)``])#!A)#!A)_PT]`0T]`0T_`P\_`P\__AA)#!A)#!D``QA)#!A)#!D[`0T]`0T]`0\_`P\_`P\[#!A)#!A)#0``#!A)#!A)#0L]`0T]`0T]`P\_`P\_`PI)#!A)#!A)``])#!A)#!A)_PT]`0T]`0T_`P\_`P\__AA)#!A)#!D``QA)#!A)#!D]`0T]`0T]`0\_`P\_`PT]L+SM#!A)#0``#!A)#!A)#!D[_PT]`0T]`P\_`PT]_KSML+SML!A)``])#!A)#!A)#!A)_PT]`0T_`PT]_KSML+SML!A)#!D``QA)#!A)#!A)#!A)#0L]`0T]8+SML+SML+Q)#!A)#0``#!A)#!A)#!A)#!A)#!D[8+SML+SML+Q)#!A)#!A)``])#!A)#!A)#!A)#!A)#!A)#+SML+Q)#!A)#!A)#!D``QA)#!A)#!A)#!A)#!A)#!A)#!A)#!A)#!A)#!A)#0```````````````````````````````````````````Q!!!!Q!!5:13&amp;!!!!!!!!-!!!-(!!!'3(C=L:204"."'-7`76MS29GTS,]V9#P:)D%3U52"2+1SI"AA3KU2$GBDCZ+A.3U94X$:G($A)I'$#4&gt;#YME$)&gt;Z.YW5PH$321Y7TC3:+**(N_MUOWWU"[]5?*JNGXP&gt;GXPPN!J2Q6C.F9=I!QL&lt;QI=_!UJB/!.*.&amp;(:`$3_"2=BP)"5+-;#42NC[F#7V"BS*[1WUG=`!&gt;^RN@D&amp;@Q4QB&lt;!/XFD!&amp;BZ5;=$3G6]MXV1R4X^;K-VZHKARV&lt;*:EJ8OK`R?&gt;V2*I#&amp;KD7/5GEA8#4XI]7PWN[./YJIJ`@5V5M5&lt;[$'"=,UOKG6-Y%;X@7S0*##S2B$-3='1DL+SMO#,:&amp;D69RWA6GC5=OUJ'CGD+O6[85D.H,%WJJ5'@6=?(_\.TYOR#N&amp;&gt;[D/M-J;B\Y6RZS@(C@K04UGVO&lt;K)/VVX&gt;9Q-KV-QVKN#.W'&gt;PW7XN(2!A[@P5`'3_%8JZ1,2A\3X()I*NB)8R/7T!;5W8(I$(]2K'/;M'DV.$/^:!?KQ;@(S'A&gt;P$X$^[Y*ZU@6X8_'2K)JY-*%9$$]?DK64A78,M?81C(IB&amp;*[,\'\L#&gt;6_,O,UQM_#!3P$!8&lt;C2HX9#FJ?8-1"=87E(3KP64%YH7\@JSS5?=R-8LGZS6T%ZPHXHE5APW#96-HP7:@9]-COTP==K90&lt;#`W@W)K)UN9&gt;:Y"##3"(_7GR2(L-B6!U#,[*J2=VU!&lt;-B^"FU@)IR?WE`MS((+]@MQM*#A1[4&lt;]MR[S(%:F:&lt;-X@-(5(O4`-(2'!ID^Q)VM(VA%$1`)9?T?ABYP=[]8=&gt;S#L"`.X$T"^91+!%&lt;'"^S@AI\_]0DTX:8Q8H/DWH:M)Y85Q_!2*U10X@)DVM1,&gt;.=Q'3)6$T1Z4^?!'*+&lt;M2@R6C_^FE60(Z8@=?LCO8#V_.+P"#/`C&gt;OZHSA16BU.=R[/!1")?"&lt;Z?.W+!@1N$293$P`(4^QW4OWUX$+D?,&lt;7$&gt;/(T;A%L?6&lt;\"?P%?D0&lt;3&lt;L;)LQO_/66UE7[F0_;_\OG_H&amp;*&lt;AT!ZLL\WUZI`KW`(F!!!!!!%!!!!+!!!!!1!!!!!!!!!$!!"1E2)5!!!!!!!!Q!!!')!!!"S?*RD9'$)%Z"A_M&gt;1^Z?"3?!LE#(^FY&amp;:U)`R.Q-$JZ`!93$.+#!*&amp;*&lt;^S]!OK!U7VD[CS]%!";JMD"S3()=&amp;/=!S(#U;$0```_@Y?O1;8-52(TB4::9]BQ1!&amp;'):!!!!!!!!"!!!!!=!!!*3!!!!"Q!!!#&amp;@&lt;GF@4'&amp;T&gt;%NO&lt;X&gt;O4X&gt;O;7ZH4&amp;:$&lt;'&amp;T=U.M&gt;8.U:8)!!!!]&amp;A#!!!!!!!%!#!!Q`````Q!"!!!!!!!A!!!!!1!91&amp;!!!"&amp;4;7VV&lt;'&amp;U&lt;X)O&lt;(:D&lt;'&amp;T=Q!"!!!!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962B9E^S:'6S!!!!+29!A!!!!!!#!!5!"Q!!$!"!!!(`````!!!!!1!"!!!!!1!!!!!!!!!!!!!!'UR71WRB=X.1=GFW982F2'&amp;U962J&lt;76T&gt;'&amp;N=!!!!"E7!)!!!!!!!1!&amp;!!=!!!%!!.DE:(-!!!!!!!!!*ER71WRB=X.1=GFW982F2'&amp;U95RB=X2"=("M;76E6'FN:8.U97VQ!!!!'29!A!!!!!!"!!5!"Q!!!1!!W/2E=Q!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6(FQ:52F=W-!!!"5&amp;A#!!!!!!!%!#!!Q`````Q!"!!!!!!!Y!!!!!A!71(!!#!!!!!)!!!FS:7:%45V4;7U!'E"1!!%!!"&amp;4;7VV&lt;'&amp;U&lt;X)O&lt;(:D&lt;'&amp;T=Q!"!!%!!!!!!!!!(ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B5WF[:1!!!"E7!)!!!!!!!1!&amp;!!-!!!%!!!!!!!1!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B!!!!2"9!A!!!!!!#!":!=!!)!!!!!A!!#8*F:E2.46.J&lt;1!;1&amp;!!!1!!%6.J&lt;86M982P=CZM&gt;G.M98.T!!%!!1!!!!!!!!!!!!!!!!!%!!-!#Q!!!!1!!!"(!!!!+!!!!!)!!!1!!!!!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%!!!!"I8C=D9]^4M.!%)5`:W0CB!13%N)BO;+AI/%#FE"UE3)Y!"D`)%O,(.HLC**D=A!/!$@AY13#2)/?&gt;H@GG^'&lt;(7$/0&amp;I2!"XI6VF_N6D=&amp;E`E@,S_J&lt;7YG1L&gt;C45W&gt;G6V&lt;N?W?*D]TB-&lt;V`6I2R*H/9O7?("S;:P;:669ZG(&lt;&amp;K[K9BW\,%RD&amp;_.,$%D"?^=(@-QWZ9)O`;C(S?UD1@2CSBOX!6G:-Z;\99]?A7H3H#%`/T"M*Q_-?U\FN24LM]`J0`]D"V`'ON1&gt;=#`$,N?S#4HA5/5PD=7_V&gt;HK,^F6*KUWCXE=+5J;8U](2HK."AY64:G*T$B70)*0\_&amp;#L!!!!'5!!1!#!!-!"!!!!%A!$Q1!!!!!$Q$9!.5!!!"2!!]%!!!!!!]!W!$6!!!!7A!0"!!!!!!0!.A!V1!!!'/!!)1!A!!!$Q$9!.5)5W6H&lt;W5A65E)5W6H&lt;W5A65E)5W6H&lt;W5A65E"-!!!!&amp;*45E-.#A!$4&amp;:$1UR#6F=!!")1!!!%.A!!!#!!!"(Q!!!!!!!!!!!!!!!A!!!!.!!!"#A!!!!=4%F#4A!!!!!!!!&amp;A4&amp;:45A!!!!!!!!&amp;U5F242Q!!!!!!!!')1U.46!!!!!!!!!'=4%FW;1!!!!!!!!'Q1U^/5!!!!!!!!!(%6%UY-!!!!!!!!!(92%:%5Q!!!!!!!!(M4%FE=Q!!!!!!!!)!6EF$2!!!!!!!!!)5&gt;G6S=Q!!!!1!!!)I5U.45A!!!!!!!!+-2U.15A!!!!!!!!+A35.04A!!!!!!!!+U;7.M.!!!!!!!!!,);7.M/!!!!!!!!!,=4%FG=!!!!!!!!!,Q2F")9A!!!!!!!!-%2F"421!!!!!!!!-96F"%5!!!!!!!!!-M4%FC:!!!!!!!!!.!1E2)9A!!!!!!!!.51E2421!!!!!!!!.I6EF55Q!!!!!!!!.]2&amp;2)5!!!!!!!!!/1466*2!!!!!!!!!/E3%F46!!!!!!!!!/Y6E.55!!!!!!!!!0-2F2"1A!!!!!!!!0A!!!!!0````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Q!!!!!!!!!!$`````!!!!!!!!!.1!!!!!!!!!!0````]!!!!!!!!![!!!!!!!!!!!`````Q!!!!!!!!$Q!!!!!!!!!!$`````!!!!!!!!!2Q!!!!!!!!!!0````]!!!!!!!!"*!!!!!!!!!!!`````Q!!!!!!!!&amp;1!!!!!!!!!!$`````!!!!!!!!!:Q!!!!!!!!!!0````]!!!!!!!!"L!!!!!!!!!!%`````Q!!!!!!!!-Y!!!!!!!!!!@`````!!!!!!!!!UA!!!!!!!!!#0````]!!!!!!!!$7!!!!!!!!!!*`````Q!!!!!!!!.I!!!!!!!!!!L`````!!!!!!!!!XA!!!!!!!!!!0````]!!!!!!!!$C!!!!!!!!!!!`````Q!!!!!!!!/A!!!!!!!!!!$`````!!!!!!!!!\1!!!!!!!!!!0````]!!!!!!!!%/!!!!!!!!!!!`````Q!!!!!!!!9]!!!!!!!!!!$`````!!!!!!!!#E!!!!!!!!!!!0````]!!!!!!!!+5!!!!!!!!!!!`````Q!!!!!!!!V=!!!!!!!!!!$`````!!!!!!!!$71!!!!!!!!!!0````]!!!!!!!!.&lt;!!!!!!!!!!!`````Q!!!!!!!!V]!!!!!!!!!!$`````!!!!!!!!$?1!!!!!!!!!!0````]!!!!!!!!.\!!!!!!!!!!!`````Q!!!!!!!""%!!!!!!!!!!$`````!!!!!!!!%%Q!!!!!!!!!!0````]!!!!!!!!16!!!!!!!!!!!`````Q!!!!!!!"#!!!!!!!!!!)$`````!!!!!!!!%91!!!!!$6.J&lt;86M982P=CZD&gt;'Q!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!B.%45V@5WFN&gt;7RB&gt;'^S,GRW&lt;'FC%6.J&lt;86M982P=CZM&gt;G.M98.T5&amp;2)-!!!!!!!!!!!!!!!!!!$!!%!!!!!!!!!!!!!!1!91&amp;!!!"&amp;4;7VV&lt;'&amp;U&lt;X)O&lt;(:D&lt;'&amp;T=Q!"!!!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!&amp;A#!!!!!!!!!!!,``Q!!!!%!!!!!!!%!!!!!!1!91&amp;!!!"&amp;4;7VV&lt;'&amp;U&lt;X)O&lt;(:D&lt;'&amp;T=Q!"!!!!!!!"`````A!!!!!!!!)+2%V.,GRW&lt;'FC=!N%45UO&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!!!!!!!!!&amp;A#!!!!!!!!!!!!!!!!!!!%!!!!!!!)!!!!!!A!71(!!#!!!!!)!!!FS:7:%45V4;7U!:A$RW/2E=Q!!!!-42%V.8V.J&lt;86M982P=CZM&gt;GRJ9B&amp;4;7VV&lt;'&amp;U&lt;X)O&lt;(:D&lt;'&amp;T=QV4;7VV&lt;'&amp;U&lt;X)O9X2M!#J!5!!"!!!&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!"!!!!!@````]!!!!!!!!!!!!!!AJ%45UO&lt;(:M;7*Q#U2.43ZM&gt;G.M98.T!&amp;"53$!!!!!!!!!!!!!7!)!!!!!!!!!!!!!!!!</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">true</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.1</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"4!!!!!AJ%45UO&lt;(:M;7*Q#U2.43ZM&gt;G.M98.T!&amp;"53$!!!!!P!!%!#!!!!!!!$E2F&gt;C"$&lt;WVQ&lt;WZF&lt;H2T#E2.43ZM&gt;GRJ9H!,2%V.,GRW9WRB=X-!!!!!</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="Simulator.ctl" Type="Class Private Data" URL="Simulator.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Initialize.vi" Type="VI" URL="../Initialize.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)0!!!!%!!_1(!!(A!!*R.%45V@5WFN&gt;7RB&gt;'^S,GRW&lt;'FC%6.J&lt;86M982P=CZM&gt;G.M98.T!!R4;7VV&lt;'&amp;U&lt;X)A;7Y!!!^!"Q!*1G&amp;V:#"3982F!!V!"A!'5'&amp;S;82Z!!!01!9!#52B&gt;'%A1GFU=Q"H!0%!!!!!!!!!!QJ%45UO&lt;(:M;7*Q#U2.43ZM&gt;G.M98.T)6.F=GFB&lt;%.P&lt;G:J:X6S982J&lt;WYA,3VD&lt;(6T&gt;'6S,G.U&lt;!!G1&amp;!!!Q!"!!)!!R24:8*J97QA1W^O:GFH&gt;8*B&gt;'FP&lt;A!!"!!!!$Z!=!!?!!!H%U2.46^4;7VV&lt;'&amp;U&lt;X)O&lt;(:M;7)25WFN&gt;7RB&gt;'^S,GRW9WRB=X-!$6.J&lt;86M982P=C"P&gt;81!&amp;%!Q`````QJ*&lt;H.U=H6N:7ZU!!!/1#%)351A586F=HE!!!J!)163:8.F&gt;!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!A1&amp;!!!Q!+!!M!$".F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!":!5!!$!!I!#Q!-#76S=G^S)'^V&gt;!"M!0!!%!!!!!1!"1!&amp;!!9!"Q!&amp;!!A!"1!*!!5!$1!&amp;!!5!"1!/!Q!"#!!!EA!!!!A!!!!!!!!!!!!!!)U!!!)1!!!!!!!!!!A!!!!!!!!!#!!!!!!!!!!+!!!!!!!!!!!!!!!!!!!!$1M!!!!"!!]!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
	</Item>
	<Item Name="Close.vi" Type="VI" URL="../Close.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;2!!!!#1!_1(!!(A!!*R.%45V@5WFN&gt;7RB&gt;'^S,GRW&lt;'FC%6.J&lt;86M982P=CZM&gt;G.M98.T!!R4;7VV&lt;'&amp;U&lt;X)A;7Y!!!1!!!!_1(!!(A!!*R.%45V@5WFN&gt;7RB&gt;'^S,GRW&lt;'FC%6.J&lt;86M982P=CZM&gt;G.M98.T!!V4;7VV&lt;'&amp;U&lt;X)A&lt;X6U!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!#"!5!!$!!-!"!!&amp;%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;E"1!!-!!Q!%!!5*:8*S&lt;X)A&lt;X6U!'Q!]!!1!!!!!1!"!!%!!A!"!!%!!1!"!!%!!1!'!!%!!1!"!!=$!!%)!!#1!!!!!!!!!!!!!!!!!!!!D1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!!!!!!!.#Q!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
	</Item>
	<Item Name="ConfigureAutoZero.vi" Type="VI" URL="../ConfigureAutoZero.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;A!!!!#A!_1(!!(A!!*R.%45V@5WFN&gt;7RB&gt;'^S,GRW&lt;'FC%6.J&lt;86M982P=CZM&gt;G.M98.T!!R4;7VV&lt;'&amp;U&lt;X)A;7Y!!!1!!!!_1(!!(A!!*R.%45V@5WFN&gt;7RB&gt;'^S,GRW&lt;'FC%6.J&lt;86M982P=CZM&gt;G.M98.T!!V4;7VV&lt;'&amp;U&lt;X)A&lt;X6U!!^!"A!)186U&lt;XJF=G]!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!#"!5!!$!!1!"1!'%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;E"1!!-!"!!&amp;!!9*:8*S&lt;X)A&lt;X6U!'Q!]!!1!!!!!1!"!!%!!A!$!!%!!1!"!!%!!1!(!!%!!1!"!!A$!!%)!!#1!!!!!!!!!!!!!!!!!!!!D1!!!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!!!!!!!.#Q!!!!%!#1!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
	</Item>
	<Item Name="ConfigureMeasurement.vi" Type="VI" URL="../ConfigureMeasurement.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+/!!!!$1!_1(!!(A!!*R.%45V@5WFN&gt;7RB&gt;'^S,GRW&lt;'FC%6.J&lt;86M982P=CZM&gt;G.M98.T!!R4;7VV&lt;'&amp;U&lt;X)A;7Y!!!1!!!!_1(!!(A!!*R.%45V@5WFN&gt;7RB&gt;'^S,GRW&lt;'FC%6.J&lt;86M982P=CZM&gt;G.M98.T!!V4;7VV&lt;'&amp;U&lt;X)A&lt;X6U!*Q!]1!!!!!!!!!$#E2.43ZM&gt;GRJ9H!,2%V.,GRW9WRB=X-42H6O9X2J&lt;WYA,3VF&lt;H6N,G.U&lt;!"J1"9!"AJ%1S"7&lt;WRU97&gt;F#E&amp;$)&amp;:P&lt;(2B:W5+2%-A1X6S=G6O&gt;!J"1S"$&gt;8*S:7ZU%T)A,3"8;8*F)&amp;*F=WFT&gt;'&amp;O9W54.#!N)&amp;&gt;J=G5A5G6T;8.U97ZD:1!)2H6O9X2J&lt;WY!!(1!]1!!!!!!!!!$#E2.43ZM&gt;GRJ9H!,2%V.,GRW9WRB=X-65G6T&lt;WRV&gt;'FP&lt;C!N,76O&gt;7UO9X2M!$^!&amp;A!&amp;"4-A-3]S"41A-3]S"45A-3]S"49A-3]S"4=A-3]S!"23:8.P&lt;(6U;7^O)'FO)%2J:WFU=Q!!#U!+!!6397ZH:1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!A1&amp;!!!Q!'!!=!#".F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#*!)2V&amp;&lt;G&amp;C&lt;'5A186U&lt;S"397ZH:3!I6$IA27ZB9GRF+1!71&amp;!!!Q!'!!=!#!FF=H*P=C"P&gt;81!&lt;!$Q!"!!!!!"!!%!!1!#!!-!!1!%!!%!"1!"!!E!#A!"!!%!#Q-!!1A!!*!!!!!!!!!!!!!!!!!!!!#.!!!!#!!!!!!!!!!)!!!!!!!!!!A!!!!!!!!!#A!!!!A!!!!!!!!!!!!!!!U,!!!!!1!-!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
	</Item>
	<Item Name="ConfigureMultiPoint.vi" Type="VI" URL="../ConfigureMultiPoint.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)4!!!!$A!_1(!!(A!!*R.%45V@5WFN&gt;7RB&gt;'^S,GRW&lt;'FC%6.J&lt;86M982P=CZM&gt;G.M98.T!!R4;7VV&lt;'&amp;U&lt;X)A;7Y!!"F!!Q!35(*F&gt;(*J:W&gt;F=C"497VQ&lt;'6T!!"O!0%!!!!!!!!!!QJ%45UO&lt;(:M;7*Q#U2.43ZM&gt;G.M98.T&amp;V.B&lt;8"M:6.P&gt;8*D:3!N,76O&gt;7UO9X2M!$&gt;!&amp;A!#$62S;7&gt;H:8)A2'6M98E05W&amp;N='RF)%FO&gt;'6S&gt;G&amp;M!!V497VQ&lt;'5A5W^V=G.F!!1!!!!_1(!!(A!!*R.%45V@5WFN&gt;7RB&gt;'^S,GRW&lt;'FC%6.J&lt;86M982P=CZM&gt;G.M98.T!!V4;7VV&lt;'&amp;U&lt;X)A&lt;X6U!".!!Q!.6(*J:W&gt;F=C"$&lt;X6O&gt;!!41!-!$&amp;.B&lt;8"M:3"$&lt;X6O&gt;!!!$%!B"H.U982V=Q!!#U!$!!2D&lt;W2F!!!11$$`````"H.P&gt;8*D:1!!)%"1!!-!"Q!)!!E4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!61!I!$V.B&lt;8"M:3"*&lt;H2F=H:B&lt;!!71&amp;!!!Q!(!!A!#1FF=H*P=C"P&gt;81!&lt;!$Q!"!!!!!"!!)!!Q!%!!5!!Q!'!!-!!Q!$!!I!#Q!$!!-!$!-!!1A!!*!!!!!)!!!!#!!!!!!!!!#.!!!!%!!!!!!!!!!1!!!!!!!!!!!!!!!!!!!!#A!!!"!!!!!!!!!!!!!!!!U,!!!!!1!.!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
	</Item>
	<Item Name="ConfigureOffsetCompensation.vi" Type="VI" URL="../ConfigureOffsetCompensation.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!']!!!!#A!_1(!!(A!!*R.%45V@5WFN&gt;7RB&gt;'^S,GRW&lt;'FC%6.J&lt;86M982P=CZM&gt;G.M98.T!!R4;7VV&lt;'&amp;U&lt;X)A;7Y!!!1!!!!_1(!!(A!!*R.%45V@5WFN&gt;7RB&gt;'^S,GRW&lt;'FC%6.J&lt;86M982P=CZM&gt;G.M98.T!!V4;7VV&lt;'&amp;U&lt;X)A&lt;X6U!'M!]1!!!!!!!!!$#E2.43ZM&gt;GRJ9H!,2%V.,GRW9WRB=X-A5W6U4W:G=W6U1W^N='6O=W&amp;U;7^O)#UN:7ZV&lt;3ZD&gt;'Q!+U!7!!)$4W:G!E^O!!!85W6U)%^G:H.F&gt;#"$&lt;WVQ:7ZT982J&lt;WY!$%!B"H.U982V=Q!!#U!$!!2D&lt;W2F!!!11$$`````"H.P&gt;8*D:1!!)%"1!!-!"!!&amp;!!94:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71&amp;!!!Q!%!!5!"AFF=H*P=C"P&gt;81!&lt;!$Q!"!!!!!"!!%!!1!#!!-!!1!"!!%!!1!"!!=!!1!"!!%!#!-!!1A!!*!!!!!!!!!!!!!!!!!!!!#.!!!!%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!!!!!!!U,!!!!!1!*!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074274832</Property>
	</Item>
	<Item Name="ConfigureTrigger.vi" Type="VI" URL="../ConfigureTrigger.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)N!!!!$A!_1(!!(A!!*R.%45V@5WFN&gt;7RB&gt;'^S,GRW&lt;'FC%6.J&lt;86M982P=CZM&gt;G.M98.T!!R4;7VV&lt;'&amp;U&lt;X)A;7Y!!"F!#A!337ZU:8*O97QA4'6W:7QA+$!J!!!:1!9!%V.M&lt;X"F)#AQ/C"/:7&gt;B&gt;'FW:3E!"!!!!$Z!=!!?!!!H%U2.46^4;7VV&lt;'&amp;U&lt;X)O&lt;(:M;7)25WFN&gt;7RB&gt;'^S,GRW9WRB=X-!$6.J&lt;86M982P=C"P&gt;81!&lt;Q$R!!!!!!!!!!-+2%V.,GRW&lt;'FC=!N%45UO&lt;(:D&lt;'&amp;T=RB5=GFH:W6S5W^V=G.F)#UN:7ZV&lt;3ZD&gt;'Q!.U!7!!-*37VN:72J982F#%6Y&gt;'6S&lt;G&amp;M#&amp;.P:H2X98*F!!Z5=GFH:W6S)&amp;.P&gt;8*D:1!!$%!B"H.U982V=Q!!#U!$!!2D&lt;W2F!!!11$$`````"H.P&gt;8*D:1!!)%"1!!-!"A!(!!A4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!C1#%&gt;27ZB9GRF)%&amp;V&gt;']A2'6M98EA+&amp;1[)%6O97*M:3E!'5!+!".5=GFH:W6S)%2F&lt;'&amp;Z)#BT:7-J!":!5!!$!!9!"Q!)#76S=G^S)'^V&gt;!"M!0!!%!!!!!%!!A!$!!1!"1!$!!-!!Q!$!!-!#1!+!!M!!Q!-!Q!"#!!!E!!!!!A!!!!)!!!!!!!!!)U!!!!)!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!+!!!!#!!!!!A!!!!!!!!!$1M!!!!"!!U!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
	</Item>
	<Item Name="NameAndVersionNumber.vi" Type="VI" URL="../NameAndVersionNumber.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;P!!!!#A!_1(!!(A!!*R.%45V@5WFN&gt;7RB&gt;'^S,GRW&lt;'FC%6.J&lt;86M982P=CZM&gt;G.M98.T!!R4;7VV&lt;'&amp;U&lt;X)A;7Y!!!1!!!!_1(!!(A!!*R.%45V@5WFN&gt;7RB&gt;'^S,GRW&lt;'FC%6.J&lt;86M982P=CZM&gt;G.M98.T!!V4;7VV&lt;'&amp;U&lt;X)A&lt;X6U!"Z!-0````]54G&amp;N:5&amp;O:&amp;:F=H.J&lt;WZ/&gt;7VC:8)!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!#"!5!!$!!1!"1!'%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;E"1!!-!"!!&amp;!!9*:8*S&lt;X)A&lt;X6U!'Q!]!!1!!!!!1!"!!%!!A!"!!%!!1!$!!%!!1!(!!%!!1!"!!A$!!%)!!#3!!!!!!!!!!!!!!!!!!!!D1!!!!!!!!!!!!!!!!!!!!E!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!!!!!!!.#Q!!!!%!#1!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342972432</Property>
	</Item>
	<Item Name="ReadMultiPoint.vi" Type="VI" URL="../ReadMultiPoint.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'?!!!!$1!_1(!!(A!!*R.%45V@5WFN&gt;7RB&gt;'^S,GRW&lt;'FC%6.J&lt;86M982P=CZM&gt;G.M98.T!!R4;7VV&lt;'&amp;U&lt;X)A;7Y!!!1!!!!_1(!!(A!!*R.%45V@5WFN&gt;7RB&gt;'^S,GRW&lt;'FC%6.J&lt;86M982P=CZM&gt;G.M98.T!!V4;7VV&lt;'&amp;U&lt;X)A&lt;X6U!"6!!Q!/4H6N9G6S)(2P)&amp;*F971!!!5!#A!!'E"!!!(`````!!1-476B=X6S:7VF&lt;H2T!!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!A1&amp;!!!Q!'!!=!#".F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"F!!Q!447&amp;Y;7VV&lt;3"5;7VF)#BN=W6D+1!71&amp;!!!Q!'!!=!#!FF=H*P=C"P&gt;81!&lt;!$Q!"!!!!!"!!%!!1!#!!-!!1!"!!5!!1!"!!E!#A!"!!%!#Q-!!1A!!*!!!!!!!!!!!!!!!!!!!!#.!!!!#A!!!!!!!!!!!!!!#1!!!!!!!!!!!!!!#A!!!!A!!!!!!!!!!!!!!!U,!!!!!1!-!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
	</Item>
	<Item Name="ReadSinglePoint.vi" Type="VI" URL="../ReadSinglePoint.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;\!!!!#Q!_1(!!(A!!*R.%45V@5WFN&gt;7RB&gt;'^S,GRW&lt;'FC%6.J&lt;86M982P=CZM&gt;G.M98.T!!R4;7VV&lt;'&amp;U&lt;X)A;7Y!!!1!!!!_1(!!(A!!*R.%45V@5WFN&gt;7RB&gt;'^S,GRW&lt;'FC%6.J&lt;86M982P=CZM&gt;G.M98.T!!V4;7VV&lt;'&amp;U&lt;X)A&lt;X6U!"&amp;!#A!,476B=X6S:7VF&lt;H1!$%!B"H.U982V=Q!!#U!$!!2D&lt;W2F!!!11$$`````"H.P&gt;8*D:1!!)%"1!!-!"!!&amp;!!94:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!:1!-!%UVB?'FN&gt;7UA6'FN:3!I&lt;8.F9SE!&amp;E"1!!-!"!!&amp;!!9*:8*S&lt;X)A&lt;X6U!'Q!]!!1!!!!!1!"!!%!!A!"!!%!!1!$!!%!!1!(!!A!!1!"!!E$!!%)!!#1!!!!!!!!!!!!!!!!!!!!D1!!!!!!!!!!!!!!!!!!!!E!!!!!!!!!!!!!!!I!!!!1!!!!!!!!!!!!!!!.#Q!!!!%!#A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
	</Item>
	<Item Name="Generate Random Reading.vi" Type="VI" URL="../Generate Random Reading.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#Z!!!!"1!01!I!#6*B&lt;G&gt;F)%VJ&lt;A!%!!!!)U!+!"R397ZE&lt;WUA=G6B:'FO:S"X;82I;7YA=G&amp;O:W5A!!!01!I!#6*B&lt;G&gt;F)%VB?!"M!0!!%!!!!!%!!1!"!!)!!Q!"!!%!!1!"!!%!!1!"!!%!!1!"!A!"#!!!%A!!!!!!!!!!!!!!!!!!!!U!!!!1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"!!1!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Get Range Min and Max from Simulated Panel.vi" Type="VI" URL="../Get Range Min and Max from Simulated Panel.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%&amp;!!!!#A!31(!!#!!!!!)!!!6733"J&lt;A!%!!!!$U!+!!F397ZH:3".;7Y!$U!+!!F397ZH:3".98A!$%!B"H.U982V=Q!!#U!$!!2D&lt;W2F!!!11$$`````"H.P&gt;8*D:1!!)%"1!!-!"!!&amp;!!94:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71&amp;!!!Q!%!!5!"AFF=H*P=C"P&gt;81!&lt;!$Q!"!!!!!"!!%!!1!#!!%!!Q!"!!%!!1!"!!=!!1!"!!%!#!-!!1A!!")!!!!!!!!!!!!!!!!!!!!*!!!!!!!!!!E!!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!!!!!!!E!!!!!!1!*!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
</LVClass>
