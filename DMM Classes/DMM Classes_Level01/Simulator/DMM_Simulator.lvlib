﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="16008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">10386594</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">16767648</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">16448250</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)J!!!*Q(C=\&gt;8"=&gt;MQ%)8BFUQ/PKK$D&amp;L9&amp;N3#&lt;DRP#\L[K";W"&lt;7A&amp;FY,;E%N-$`AN5&lt;WD+.,EMFY$")S_1!#HU#+FG:ZEHZI@637XZ:VF:;&amp;H@LS/9LU?EL\/,T&lt;FVPXM8@\X&gt;9&gt;_P3V`?XYS`XY\_&lt;P77\ND`Q0CL\+6`H=Z@HBD_#P`Q[_D4@"3D7VK%G._?N?47ZSEZP=Z#9P]C)P]C)P]C*0]C20]C20]C10]C!0]C!0]C#@'\H)23ZS3':S-Z%:V!RA/E/2_4!?YT%?Y_&amp;5RG-]RG-]RE-8'9`R')`R'!_8S8C-RXC-RXA9;EI].X)]RM0Q+DS&amp;J`!5HM,$F#I]";#9L"CY'!3'CM&lt;CI0!5HM,$I1J0Y3E]B;@QU+T#5XA+4_%J0&amp;QS6[7G:GTE?"B'C3@R**\%EXA97IEH]33?R*.YG%[**`%EC'4#:(!)3CZ+/C1HC3@R]%?**`%EHM34?'C;&gt;SDHSAT.W-DR"*\!%XA#4_"B#!7?Q".Y!E`A96A&amp;HM!4?!*0Y'%K":\!%XA#3$!JUSM9,,AQ["1%A9&gt;^0CUR\V*.3=R.[I&gt;8`6#K(T&lt;V1[2_/.1X88UTV4&gt;*P@DK265PFHI2V&amp;^/D6:DV*/I,RY&gt;&gt;?8T1DV44^1D^5$&gt;5X@5,85T,PX$(;`8KS[8C]\HMU[HEY\(IQ[(A`&lt;\P8;\H&lt;&lt;&lt;L4;&lt;T?UV]*0N^E*Y?3]^=`R2`1@`H`_$&gt;[/_[XY/VOA8*@U\BQ!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">369131520</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="DMMSimulator.vi" Type="VI" URL="../DMMSimulator.vi"/>
	<Item Name="Simulator.lvclass" Type="LVClass" URL="../Simulator.lvclass"/>
</Library>
