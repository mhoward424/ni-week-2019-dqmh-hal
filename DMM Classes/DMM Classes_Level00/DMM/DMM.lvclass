﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="16008000">
	<Property Name="EndevoGOOP_ClassItemIcon" Type="Str">CyanFrame</Property>
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">15007601</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorProtected" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">14399157</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.ContainingLib" Type="Str">DMM_DMM.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../DMM_DMM.lvlib</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,5!!!*Q(C=T:1R&lt;F."%)&lt;`ZV#E1##@A#B.7K2J%65O%%POK?9+JL$)FDE!)-U6,.]A$@2T!W3B3.2JA1,-^^9&lt;*X;=O!'*8=^\@P]`/`NZ&gt;`WEWA[F*VLO;S?0NL6@-`O(:&lt;NO_^7Z]^HJX_G\`9@L\ZH`1@Z^\=?D&lt;&gt;M@$"\T"^02&gt;$JYS-@NE49S&lt;PXG&lt;G7M`6NXF&lt;(NDT&lt;]_8V`?FNB0J_0&gt;PAN9TZ`U#&gt;D/H`5&lt;`&lt;`ZU]X`(L@80`2;/XT-V&gt;JG`MX7PFXX/X^LREH]VP\XPE:&lt;&lt;DX@&gt;I&gt;&gt;[?`U@9?&lt;^Y$:@_@Y.]WK60`)C+3#-)*5W]N%TX2%TX2%TX2!TX1!TX1!TX1(&gt;X2(&gt;X2(&gt;X2$&gt;X1$&gt;X1$&gt;X1;U=8ON#&amp;TKIEES=4*574!EES+%IO#5`#E`!E0$QKY5FY%J[%*_%B21F0QJ0Q*$Q*$]/5]#1]#5`#E`"1KJ*E\?DQ*$S56]!4]!1]!5`!QZ1+?!+!9,+A=&amp;!%$!6G]#8A#8A#(LYKY!FY!J[!*_$"6M!4]!1]!5`!QZ#[+F&amp;J_IY/$W8E]$A]$I`$Y`"17A[0Q_0Q/$Q/$^0*Y8&amp;Y(!BH1K=Y#()'/1H/A]0D](#4Q_0Q/$Q/D]/$68@)[]LU.(V(B]@A-8A-(I0(Y+'%$"[$R_!R?!Q?SMLA-8A-(I0(Y'%K'4Q'D]&amp;DA"C4-LW-9M:!)]E1$"Y_^&lt;29X;7I*&amp;;\V![PWK&amp;5/WRKBUDN=+BNONJGKGW3WO+L,;L;9KENANK05Y.7AV'&lt;2'VQH[BLLAPCEJA2&amp;]3%'"/HR$%R\)@_Z=4L[WMN&amp;AN&gt;8FZK.JPJYO*#E]F%Y`&amp;9J[?H/DY_VH!YV%U\IK^;&gt;`.?+HQPX][[]P&amp;,6T[-O`,_&lt;07-PP&lt;\'(TKSO(,LBQ^\5IZ1#0?0/P+W^&gt;&gt;?@?\+_=\9LG]+C^_`3T0PV_6A[_@C]Y:^WJ&gt;_`^Y.WKAV8W^2H]!DJZGYA!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">369131520</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.2</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"5E5F.31QU+!!.-6E.$4%*76Q!!%01!!!1Q!!!!)!!!%.1!!!!?!!!!!AV%45V@2%V.,GRW&lt;'FC#U2.43ZM&gt;G.M98.T!!!!!!#A&amp;A#!!!!Q!!!)!!1!!!!!"!!$!$Q!P!!@1)!#!!!!!!%!!1!'`````Q!!!!!!!!!!!!!!!)E(XLV0:I",F/S%V.8?.#E!!!!-!!!!%!!!!!$+CVX=^6442JJGM9J&gt;&gt;_M`V"W-W9]!MA4JA!G9\0B#@A!!%!!!!!!!/TT"-/PFO5K@JBRP*9D7A!%!!!$`````V"W-W9]!MA4JA!G9\0B#@A!!!"$MI^R8Q#&gt;;@:%!_2IPHHCN!!!!"!!!!!!!!!!H!!&amp;-6E.$!!!!!1!#6EF-1A!!!!"16%AQ!!!!"1!"!!%!!!!!!A!$!!!!!!)!!1!!!!!!)A!!!"RYH'.A9W"K9,D!!-3-1-T5Q01$S0Y!YD-!!'A"#$9!!!!!!%5!!!%9?*RD9-!%`Y%!3$%S-$#&gt;!.)M;/*A'M;G*M"F,C[\I/,-5$?S1E1:A7*-?Y!-*J!=KF[1@]"_1A&amp;M7-Q'!(&lt;V+"5!!!!!!!!-!!&amp;73524!!!!!!!$!!!"71!!!IRYH!NA:'$).,9Q7Q#EG9&amp;9D+'")4E`*:7,!=BHA)!3*A;311"5PR;;O/'"QWF!I-=PXQ,G.\`B[8:2%7CO5:&amp;A+B8J^F%2[@229?FE58HRZ````]V(?!ZX?_1=&gt;\1"K?XG!)I@&gt;V(B!('!.!O)`B_9!6)&amp;-S]!;"J(1Y5S1QG,Y9'IQ]=&lt;4"AB&amp;M/-D%+V@Q&gt;)_D#S?TC!DB!Y_*#FOV%$S/_&gt;##+"1DS&gt;)2Q3RVUY&gt;-3!@-94H5$L/XFALO3!WR]'-K"%2;$4"/23&amp;J$J9$8&gt;&lt;-=&gt;.-$O&gt;B#"5"E1KA*#&amp;9!&gt;!X&lt;"%9[YQ`$Q7PP[XCZ1/#*(A1%1A^4L-4!S-),F'"FKI8)W1$946!Q7^C$W"3B&lt;!UH0&amp;S4T1(J!-GOA9C$W*CA&lt;:"=&lt;6/QPE*Y!:9.]FQ"F=Q0:#["M)3"&lt;!-K7",)@1.FS5,;TPYML=JK!J3]!)3FM%Q!!!!!!!!Q7!)!1!!!%-49O-!!!!!!-&amp;A#!!!!!"$%W,D!!!!!!$"9!A"!!!!1R.CYQ!!!!!!Q7!)!!!!!%-49O-!!!!!!-&amp;A#!%!!!"$%W,D!!!!!!&amp;!%!!!$V6T7#?3;CD#ZT5EY'34G&gt;!!!!$1!!!!!!!!!!!!!!!!!!!!!!!!#!`````Y!!!!'!Z&amp;%"A*&lt;&lt;!9#661'!F&amp;%"A/22!9!!!!(`````A!!!!9!!!!'!!M!"A!AQ!9!A$!'!A!-"A!!$!9"A$A'!?$Y"A(\_!9"``A'!@`Y"A(`_!9"``A'!@`Y"A0``Y9!@`@G!$`@BA!0@Q9!!@Q'!!$Q"A!!!!@````]!!!)!``````````````````````!!!!!!!!!!!!!!!!!!!!`Q!!!!``!0!!]0!!]!!!!0]!!!!0!0$`$`$`$`!!!!$`!!!!$Q$Q]0$Q]0$Q!!!!`Q!!!!]!]0!!]0!!]!!!!0]!!!!0`Q$Q!0$Q!0!!!!$`!!!!!!!!!!!!!!!!!!!!```````````````````````-T-T-T-T-T-T-T-T-T-T`T-T-T-T-T"(-T-T-T-T-`]T-T-T-T"O&lt;M=T-T-T-T0`-T-T-T"O1!!ORT-T-T-T`T-T-T"O1!!!!#\(-T-T-`]T-T-O1!!!!!!!,P-T-T0`-T-T*E!!!!!!!$\T-T-T`T-T-S&lt;O1!!!!$`_=T-T-`]T-T-G\OZ!!$```H-T-T0`-T-T*O\O\G````ZT-T-T`T-T-S&lt;O\O\````_=T-T-`]T-T-G\O\O`````H-T-T0`-T-T*O\O\P````ZT-T-T`T-T-S&lt;O\O\````_=T-T-`]T-T-G\O\O`````H-T-T0`-T-T,O\O\P```_\``T-T`T-T-T*G\O\``_\H````-`]T-T-T-G\O`_\H````-T0`-T-T-T-S&lt;O\$````]T-T`T-T-T-T-T*$````]T-T-`]T-T-T-T-T-T``]T-T-T0`-T-T-T-T-T-T-T-T-T-T`````````````````````]!!!1!````````````````````````````````````````````*S=H*S=H*S=H*S=H*S=H*S=H*S=H*S=H*S=H*S=H``]H*S=H*S=H````*S@`*S=H`S@`*S=H`S=H*S=H*S@``S=H*S=H*S@`*S@`*```*```*```*```*S=H*S=H*```*S=H*S=H*`]H*`]H`S@`*`]H`S@`*`]H*S=H*S=H``]H*S=H*S=H`S=H`S@`*S=H`S@`*S=H`S=H*S=H*S@``S=H*S=H*S@```]H*`]H*S@`*`]H*S@`*S=H*S=H*```*S=H*S=H*S=H*S=H*S=H*S=H*S=H*S=H*S=H*S=H``````````````````````````````````````````````DY_0DY_0DY_0DY_0DY_0DY_0DY_0DY_0DY_0DY_0``_0DY_0DY_0DY_0DY_0A#!PDY_0DY_0DY_0DY_0DY```Y_0DY_0DY_0DY_0A#@&amp;*]@!,Y_0DY_0DY_0DY_0D```DY_0DY_0DY_0A#@&amp;)H*S=H@(Q#_0DY_0DY_0DY_0``_0DY_0DY_0A#@&amp;)H*S=H*S=H*XR]!PDY_0DY_0DY```Y_0DY_0DY@&amp;)H*S=H*S=H*S=H*S&gt;]@0DY_0DY_0D```DY_0DY_0B35C=H*S=H*S=H*S=H*[R]_0DY_0DY_0``_0DY_0DY_&amp;*]@&amp;)H*S=H*S=H*[SML&amp;,Y_0DY_0DY```Y_0DY_0DY5HR]@(R3*S=H*[SML+SM5PDY_0DY_0D```DY_0DY_0B3@(R]@(R]5HSML+SML+R3_0DY_0DY_0``_0DY_0DY_&amp;*]@(R]@(R]L+SML+SML&amp;,Y_0DY_0DY```Y_0DY_0DY5HR]@(R]@(SML+SML+SM5PDY_0DY_0D```DY_0DY_0B3@(R]@(R]@+SML+SML+R3_0DY_0DY_0``_0DY_0DY_&amp;*]@(R]@(R]L+SML+SML&amp;,Y_0DY_0DY```Y_0DY_0DY5HR]@(R]@(SML+SML+SM5PDY_0DY_0D```DY_0DY_0B]@(R]@(R]@+SML+SML(R]L+SM_0DY_0``_0DY_0DY_0B35HR]@(R]L+SML(R]5KSML+SML0DY```Y_0DY_0DY_0DY5HR]@(SML(R]5KSML+SML0DY_0D```DY_0DY_0DY_0DY_&amp;*]@(R]*[SML+SML+TY_0DY_0``_0DY_0DY_0DY_0DY_0B3*[SML+SML+TY_0DY_0DY```Y_0DY_0DY_0DY_0DY_0DY_+SML+TY_0DY_0DY_0D```DY_0DY_0DY_0DY_0DY_0DY_0DY_0DY_0DY_0DY_0```````````````````````````````````````````Q!!!!Q!!5:13&amp;!!!!!!!!-!!!*[!!!%[8C=L:20;"."'-7`#6O:")OTM&lt;5..#37&lt;3TCAAL;'D37&gt;FKIF+,%AD=.&lt;PQ$R5C3&amp;E`N:3PEE&amp;.A$UKP]?AB"_]3=NG$/6GQ9%D!OZ?C9$@RGUVXNUERP:D$M)4Z@7`GP==!3$`9O+].7R91&gt;I!@+R9%.*-!6&amp;5+2\`97W"LZ!_1E2#R9)[OM8V@GUR9=&amp;9T9`1K,]"0X.XZUNG%-AGT*GY^QU)Y,'$"/=U=E_]J$;:]H&amp;!+1]Z5'=+M3.K_2ULE&amp;SXK'21%@6KMMEL;10B&amp;3&gt;)H\[&gt;?J86&amp;`/N8;=A?[&lt;?!=8-YKT1OY535`GS0*!&lt;5S!&gt;H*/$);;B5+BYE&gt;['9@9R:Q&gt;2Q\"YR"D""&lt;I:T3O/+T12M"H8W("U?;:@%W185DZ\H*E-5O4@/F7O/&amp;I^9=T&lt;8;L71Q`7)?W("C.+9JS(;V,Y.$4`10Q%"5HUML(UP?(F6J'$P$7)15X(#EPC&gt;N/#S&lt;PK?A/2I,5(*DE&amp;S9LC.-:!F/Q9`,T$Q=CC&gt;EA/8KJ0BB@7.8$[&gt;D7;?2:_OJX+Z[/PMS]V50BX65PH5S94O=.-`)WYPR/RSQ#B)-!_&amp;YWZHI&amp;QOIQ'Y?GA#U4'FY8+S@:OC[\DG/3Z50?@OIH0]^]0HQLWJO-`J,(BNP9ZNF6H`A8L;?O0`N`5GFGCLL[WQ!SI-;NZ-&amp;TL76B7J"/Q-9';2W?ZJKYI[#4"/&lt;_ONEWV6(3WXL9:B^($I?&gt;RNKU2)N[V[P809/=4/ILU38)/#X6HQLRY\/&gt;WP&lt;&lt;C0#UUKP%.$`^\!&amp;F&amp;MWY*2PB"MMG7=ROAS873\G#I_2R@I,DWI@H7@H_K+3_JVK*0PSLM)(@],#GBJ^!!!!!!!"!!!!#!!!!!%!!!!!!!!!!Q!!5*%3&amp;!!!!!!!!-!!!"C!!!!=HC=9W"AS"/190L(50?8A5HA+Z!B`:?"7&gt;#0]4=$![?@Q'%AT3AA#237`=P!,KA.&amp;N9_IMP"!!7K&lt;)Q=EBS("4H!-BQN'AT````H_(LE'FT&amp;%2]Y5W770)=%!"2C'1!!!!!!!!1!!!!(!!!#)!!!!!9!!!!B8WZJ8URB=X2,&lt;G^X&lt;E^X&lt;GFO:UR71WRB=X.$&lt;(6T&gt;'6S!!!!EB9!A!!!!!!"!!A!-0````]!!1!!!!!!&gt;A!!!!-!(A!X`````Q!$&amp;A#!!!!!!!%!"!!!!!%!!!!!!!!!0%"Q!!]&amp;&lt;GF%45U!!1!!!!!7!)!!!!!!!1!%!!!!!1!!!!!!!"6*&lt;H.U=H6N:7ZU)%2F=W.S;8"U&lt;X)!&amp;%"1!!%!!1N%45UO&lt;(:D&lt;'&amp;T=Q!"!!)!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962B9E^S:'6S!!!!*29!A!!!!!!#!!5!"Q!!$!"!!!(`````!!!!!1!"!!!!!!!!!!!!!!!&lt;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'FN:8.U97VQ!!!!'29!A!!!!!!"!!5!"Q!!!1!!VD1&lt;G1!!!!!!!!!G4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B4'&amp;T&gt;%&amp;Q='RJ:725;7VF=X2B&lt;8!!!!!:&amp;A#!!!!!!!%!"1!(!!!"!!$7."O:!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5?8"F2'6T9Q!!!$97!)!!!!!!!1!)!$$`````!!%!!!!!!"I!!!!"!"*!5!!!#U2.43ZM&gt;G.M98.T!!%!!!!!!!!!!!!?4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'&amp;4;8JF!!!!'29!A!!!!!!"!!5!!Q!!!1!!!!!!!!!!!!!!!!!%!!)!#!!!!!1!!!"!!!!!+!!!!!)!!!1!!!!!"1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$@!!!"68C=D9^"4M*!')7`9&gt;!#&amp;5&amp;='J.:O($F2A\12///B(!",&lt;1V**05U)'Y^"J?D(0I$8RN-3\9G*@Z]`\XT``?$($*AO`^^/I4M-0(W?R:Z]\P`(I:NWTFU[K+;LY+HNNE$N=0@FO&amp;@/0+QD6D^\::\^+1OSQ.+3=#!T)Q8RAV^N"S,^J0)GTB8_EF(\:=B&amp;&lt;)SY+RP+WOHR,:&lt;69QK,.M?-_U0;&gt;$DTYX`]LPU.CI',4WIOAO4T*RH$'57/.=9&lt;]Q"RQL@Z.2A`9DBL%=U9.17&amp;&gt;KT!54V@A(\^5T&gt;!!!!!"F!!%!!A!$!!1!!!")!!]%!!!!!!]!W!$6!!!!51!0"!!!!!!0!.A!V1!!!&amp;I!$Q1!!!!!$Q$9!.5!!!"DA!#%!)!!!!]!W!$6#&amp;.F:W^F)&amp;6*#&amp;.F:W^F)&amp;6*#&amp;.F:W^F)&amp;6*!4)!!!"35V*$$1I!!UR71U.-1F:8!!!1^!!!"$!!!!!A!!!1V!!!!!!!!!!!!!!!)!!!!$1!!!1I!!!!(%R*1EY!!!!!!!!"9%R75V)!!!!!!!!"&gt;&amp;*55U=!!!!!!!!"C%.$5V1!!!!!!!!"H%R*&gt;GE!!!!!!!!"M%.04F!!!!!!!!!"R&amp;2./$!!!!!!!!!"W%2'2&amp;-!!!!!!!!"\%R*:(-!!!!!!!!#!&amp;:*1U1!!!!!!!!#&amp;(:F=H-!!!!%!!!#+&amp;.$5V)!!!!!!!!#D%&gt;$5&amp;)!!!!!!!!#I%F$4UY!!!!!!!!#N'FD&lt;$1!!!!!!!!#S'FD&lt;$A!!!!!!!!#X%R*:H!!!!!!!!!#]%:13')!!!!!!!!$"%:15U5!!!!!!!!$'&amp;:12&amp;!!!!!!!!!$,%R*9G1!!!!!!!!$1%*%3')!!!!!!!!$6%*%5U5!!!!!!!!$;&amp;:*6&amp;-!!!!!!!!$@%253&amp;!!!!!!!!!$E%V6351!!!!!!!!$J%B*5V1!!!!!!!!$O&amp;:$6&amp;!!!!!!!!!$T%:515)!!!!!!!!$Y!!!!!$`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*!!!!!!!!!!!`````Q!!!!!!!!$)!!!!!!!!!!$`````!!!!!!!!!.Q!!!!!!!!!!0````]!!!!!!!!!Z!!!!!!!!!!!`````Q!!!!!!!!%1!!!!!!!!!!$`````!!!!!!!!!2A!!!!!!!!!!0````]!!!!!!!!"1!!!!!!!!!!!`````Q!!!!!!!!'-!!!!!!!!!!$`````!!!!!!!!!:Q!!!!!!!!!"0````]!!!!!!!!#`!!!!!!!!!!(`````Q!!!!!!!!--!!!!!!!!!!D`````!!!!!!!!!RQ!!!!!!!!!#@````]!!!!!!!!$,!!!!!!!!!!+`````Q!!!!!!!!-]!!!!!!!!!!$`````!!!!!!!!!UQ!!!!!!!!!!0````]!!!!!!!!$:!!!!!!!!!!!`````Q!!!!!!!!.Y!!!!!!!!!!$`````!!!!!!!!!`Q!!!!!!!!!!0````]!!!!!!!!'!!!!!!!!!!!!`````Q!!!!!!!!I%!!!!!!!!!!$`````!!!!!!!!#B1!!!!!!!!!!0````]!!!!!!!!-F!!!!!!!!!!!`````Q!!!!!!!!S=!!!!!!!!!!$`````!!!!!!!!$+1!!!!!!!!!!0````]!!!!!!!!-N!!!!!!!!!!!`````Q!!!!!!!!U=!!!!!!!!!!$`````!!!!!!!!$31!!!!!!!!!!0````]!!!!!!!!03!!!!!!!!!!!`````Q!!!!!!!!^1!!!!!!!!!!$`````!!!!!!!!$VA!!!!!!!!!!0````]!!!!!!!!0B!!!!!!!!!#!`````Q!!!!!!!""I!!!!!!&gt;%45UO9X2M!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!AV%45V@2%V.,GRW&lt;'FC#U2.43ZM&gt;G.M98.T5&amp;2)-!!!!!!!!!!!!!!!!!!'!!%!!!!!!!!"!!!!!1!31&amp;!!!!N%45UO&lt;(:D&lt;'&amp;T=Q!"!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"9!A!!!!!!!!!!!``]!!!!"!!!!!!!"!1!!!!%!%E"1!!!,2%V.,GRW9WRB=X-!!1!!!!!!!@````Y!!!!!!AV"1F^)15QO&lt;(:M;7*Q$E&amp;#8UB"4#ZM&gt;G.M98.T!&amp;"53$!!!!!!!!!!!!!7!)!!!!!!!!!!!0``!!%!!!!!!!)"!!!!!Q!?!$@`````!!-7!)!!!!!!!1!%!!!!!1!!!!!!!!!]1(!!$Q6O;52.41!"!!!!!"9!A!!!!!!"!!1!!!!"!!!!!!!!&amp;5FO=X2S&gt;7VF&lt;H1A2'6T9X*J=(2P=A"'!0(7)EW]!!!!!AN%45UO&lt;(:D&lt;'&amp;T=Q&gt;%45UO9X2M!#J!5!!"!!%&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!#!!!!!@````]!!!!%2'6W-1!!!!!!!!).15*@3%&amp;-,GRW&lt;'FC=!Z"1F^)15QO&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!!!!!!!!!&amp;A#!!!!!!!!!!!$``Q!"!!!!!!!!!!!!!!-!(A!X`````Q!$&amp;A#!!!!!!!%!"!!!!!%!!!!!!!!!0%"Q!!]&amp;&lt;GF%45U!!1!!!!!7!)!!!!!!!1!%!!!!!1!!!!!!!"6*&lt;H.U=H6N:7ZU)%2F=W.S;8"U&lt;X)!2A$RVC*.P!!!!!),2%V.,GRW9WRB=X-(2%V.,G.U&lt;!!K1&amp;!!!1!"(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!!A!!!!(````_!!!!"%2F&gt;D%!!!!!!!!#$5&amp;#8UB"4#ZM&gt;GRJ9H!/15*@3%&amp;-,GRW9WRB=X-!5&amp;2)-!!!!!!!!!!!!"9!A!!!!!!!!!!!``]!!1!!!!!!!1!!!!!"!&amp;)!]&gt;9U'ZE!!!!$$52.46^%45UO&lt;(:M;7),2%V.,GRW9WRB=X-(2%V.,G.U&lt;!!I1&amp;!!!"V$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!!!!!!!!!!!!!).15*@3%&amp;-,GRW&lt;'FC=!Z"1F^)15QO&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!!!!!!!!!&amp;A#!!!!!!!!!!!$``Q!"!!!!!!!#!!!!!!%!5A$RVD1&lt;G1!!!!-.2%V.8U2.43ZM&gt;GRJ9AN%45UO&lt;(:D&lt;'&amp;T=Q&gt;%45UO9X2M!#B!5!!!(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!!!!!!!(````_!!!!!!)33%&amp;-)&amp;6U;7RJ&gt;(EO&lt;(:M;7*Q%UB"4#"6&gt;'FM;82Z,GRW9WRB=X-!5&amp;2)-!!!!!!!!!!!!"9!A!!!!!!!!!!!!!!"!!!!#U2.43ZM&gt;G.M98.T</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">true</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.2</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"T!!!!!B*)15QA682J&lt;'FU?3ZM&gt;GRJ9H!43%&amp;-)&amp;6U;7RJ&gt;(EO&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!0Q!"!!A!!!!!!!Z%:89A1W^N='^O:7ZU=R*)15QA682J&lt;'FU?3ZM&gt;GRJ9H!43%&amp;-)&amp;6U;7RJ&gt;(EO&lt;(:D&lt;'&amp;T=Q!!!!!</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="DMM.ctl" Type="Class Private Data" URL="DMM.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Enums" Type="Folder">
		<Item Name="SetOffsetCompensation --enum.ctl" Type="VI" URL="../SetOffsetCompensation --enum.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"W!!!!!1"O!0%!!!!!!!!!!QV%45V@2%V.,GRW&lt;'FC#U2.43ZM&gt;G.M98.T)&amp;.F&gt;%^G:H.F&gt;%.P&lt;8"F&lt;H.B&gt;'FP&lt;C!N,76O&gt;7UO9X2M!#N!&amp;A!#!U^G:A*0&lt;A!!&amp;V.F&gt;#"0:G:T:81A1W^N='6O=W&amp;U;7^O!!%!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
		</Item>
		<Item Name="SampleSource --enum.ctl" Type="VI" URL="../SampleSource --enum.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"[!!!!!1"S!0%!!!!!!!!!!QV%45V@2%V.,GRW&lt;'FC#U2.43ZM&gt;G.M98.T'&amp;.B&lt;8"M:3"4&lt;X6S9W5A,3VF&lt;H6N,G.U&lt;!!X1"9!!AV5=GFH:W6S)%2F&lt;'&amp;Z$V.B&lt;8"M:3"*&lt;H2F=H:B&lt;!!.5W&amp;N='RF)&amp;.P&gt;8*D:1!"!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
		</Item>
		<Item Name="OffsetCompensation --enum.ctl" Type="VI" URL="../OffsetCompensation --enum.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!F!!!!!1!&gt;1!-!&amp;U^G:H.F&gt;#"$&lt;WVQ:7ZT982F:#"0;'VT!!%!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
		</Item>
		<Item Name="Resolution --enum.ctl" Type="VI" URL="../Resolution --enum.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#"!!!!!1"Z!0%!!!!!!!!!!QV%45V@2%V.,GRW&lt;'FC#U2.43ZM&gt;G.M98.T&amp;6*F=W^M&gt;82J&lt;WYA,3VF&lt;H6N,G.U&lt;!""1"9!"15T)$%P-A5U)$%P-A5V)$%P-A5W)$%P-A5X)$%P-A!75G6T&lt;WRV&gt;'FP&lt;C"J&lt;C"%;7&gt;J&gt;(-A-A!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
		</Item>
		<Item Name="Autozero --enum.ctl" Type="VI" URL="../Autozero --enum.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!8!!!!!1!01!9!#%&amp;V&gt;'^[:8*P!!!"!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
		</Item>
		<Item Name="TriggerSource --enum.ctl" Type="VI" URL="../TriggerSource --enum.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"[!!!!!1"S!0%!!!!!!!!!!QV%45V@2%V.,GRW&lt;'FC#U2.43ZM&gt;G.M98.T'&amp;2S;7&gt;H:8*4&lt;X6S9W5A,3VF&lt;H6N,G.U&lt;!!X1"9!!QF*&lt;7VF:'FB&gt;'5)28BU:8*O97Q)5W^G&gt;(&gt;B=G5!$F2S;7&gt;H:8)A5W^V=G.F!!!"!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
		</Item>
		<Item Name="Function --enum.ctl" Type="VI" URL="../Function --enum.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#H!!!!!1#@!0%!!!!!!!!!!QV%45V@2%V.,GRW&lt;'FC#U2.43ZM&gt;G.M98.T%U:V&lt;G.U;7^O)#UN:7ZV&lt;3ZD&gt;'Q!;5!7!!9+2%-A6G^M&gt;'&amp;H:1J"1S"7&lt;WRU97&gt;F#E2$)%.V=H*F&lt;H1+15-A1X6S=G6O&gt;"-S)#UA6WFS:3"3:8.J=X2B&lt;G.F%T1A,3"8;8*F)&amp;*F=WFT&gt;'&amp;O9W5!#%:V&lt;G.U;7^O!!!"!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
		</Item>
	</Item>
	<Item Name="Clusters" Type="Folder">
		<Item Name="SerialConfiguration --cluster.ctl" Type="VI" URL="../SerialConfiguration --cluster.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#;!!!!"!!01!=!#5*B&gt;71A5G&amp;U:1!.1!9!"F"B=GFU?1!!$U!'!!F%982B)%*J&gt;(-!:Q$R!!!!!!!!!!-.2%V.8U2.43ZM&gt;GRJ9AN%45UO&lt;(:D&lt;'&amp;T=RZ4:8*J97R$&lt;WZG;7&gt;V=G&amp;U;7^O)#UN:7ZV&lt;3ZD&gt;'Q!*E"1!!-!!!!"!!)55W6S;7&amp;M)%.P&lt;G:J:X6S982J&lt;WY!!!%!!Q!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074278912</Property>
		</Item>
	</Item>
	<Item Name="Low Level Config VIs" Type="Folder">
		<Item Name="FetchMultiPoint.vi" Type="VI" URL="../FetchMultiPoint.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;[!!!!$1!M1(!!(A!!'QV%45V@2%V.,GRW&lt;'FC#U2.43ZM&gt;G.M98.T!!:%45UA;7Y!!!1!!!!M1(!!(A!!'QV%45V@2%V.,GRW&lt;'FC#U2.43ZM&gt;G.M98.T!!&gt;%45UA&lt;X6U!"6!!Q!04H6N9G6S)(2P)%:F&gt;'.I!"F!!Q!447&amp;Y;7VV&lt;3"5;7VF)#BN=W6D+1!&amp;!!I!!"J!1!!"`````Q!&amp;$%VF98.V=G6N:7ZU=Q!!$%!B"H.U982V=Q!!#U!$!!2D&lt;W2F!!!11$$`````"H.P&gt;8*D:1!!)%"1!!-!"Q!)!!E4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71&amp;!!!Q!(!!A!#1FF=H*P=C"P&gt;81!&lt;!$Q!"!!!!!"!!%!!1!#!!-!!1!%!!9!!1!"!!I!!1!"!!%!#Q)!!1A!!*!!!!!!!!!!!!!!!!!!!!#.!!!!#!!!!!!!!!!)!!!!#1!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!!!!!!!U,!!!!!1!-!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
		</Item>
		<Item Name="SendSoftwareTrigger.vi" Type="VI" URL="../SendSoftwareTrigger.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%N!!!!#1!M1(!!(A!!'QV%45V@2%V.,GRW&lt;'FC#U2.43ZM&gt;G.M98.T!!:%45UA;7Y!!!1!!!!M1(!!(A!!'QV%45V@2%V.,GRW&lt;'FC#U2.43ZM&gt;G.M98.T!!&gt;%45UA&lt;X6U!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!#"!5!!$!!-!"!!&amp;%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;E"1!!-!!Q!%!!5*:8*S&lt;X)A&lt;X6U!'Q!]!!1!!!!!1!"!!%!!A!"!!%!!1!"!!%!!1!'!!%!!1!"!!=#!!%)!!#1!!!!!!!!!!!!!!!!!!!!D1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!!!!!!!.#Q!!!!%!#!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
		</Item>
		<Item Name="InitiateMeasurement.vi" Type="VI" URL="../InitiateMeasurement.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%N!!!!#1!M1(!!(A!!'QV%45V@2%V.,GRW&lt;'FC#U2.43ZM&gt;G.M98.T!!:%45UA;7Y!!!1!!!!M1(!!(A!!'QV%45V@2%V.,GRW&lt;'FC#U2.43ZM&gt;G.M98.T!!&gt;%45UA&lt;X6U!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!#"!5!!$!!-!"!!&amp;%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;E"1!!-!!Q!%!!5*:8*S&lt;X)A&lt;X6U!'Q!]!!1!!!!!1!"!!%!!A!"!!%!!1!"!!%!!1!'!!%!!1!"!!=#!!%)!!#1!!!!!!!!!!!!!!!!!!!!D1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!!!!!!!.#Q!!!!%!#!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
		</Item>
		<Item Name="ConfigureTrigger.vi" Type="VI" URL="../ConfigureTrigger.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)/!!!!$A!M1(!!(A!!'QV%45V@2%V.,GRW&lt;'FC#U2.43ZM&gt;G.M98.T!!:%45UA;7Y!!"F!#A!337ZU:8*O97QA4'6W:7QA+$!J!!!:1!9!%V.M&lt;X"F)#AQ/C"/:7&gt;B&gt;'FW:3E!"!!!!#R!=!!?!!!&lt;$52.46^%45UO&lt;(:M;7),2%V.,GRW9WRB=X-!"U2.43"P&gt;81!&gt;!$R!!!!!!!!!!-.2%V.8U2.43ZM&gt;GRJ9AN%45UO&lt;(:D&lt;'&amp;T=RB5=GFH:W6S5W^V=G.F)#UN:7ZV&lt;3ZD&gt;'Q!/5!7!!-*37VN:72J982F#%6Y&gt;'6S&lt;G&amp;M#&amp;.P:H2X98*F!""5=GFH:W6S)&amp;.P&gt;8*D:3!S!!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!A1&amp;!!!Q!'!!=!#".F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#*!)2V&amp;&lt;G&amp;C&lt;'5A186U&lt;S"%:7RB?3!I6$IA27ZB9GRF+1!:1!I!%V2S;7&gt;H:8)A2'6M98EA+(.F9SE!&amp;E"1!!-!"A!(!!A*:8*S&lt;X)A&lt;X6U!'Q!]!!1!!!!!1!#!!-!"!!&amp;!!-!!Q!$!!-!!Q!*!!I!#Q!$!!Q#!!%)!!#1!!!!#!!!!!A!!!!!!!!!D1!!!!A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!A!!!!)!!!!#!!!!!!!!!!.#Q!!!!%!$1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342972416</Property>
		</Item>
	</Item>
	<Item Name="Initialize.vi" Type="VI" URL="../Initialize.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(O!!!!%!!M1(!!(A!!'QV%45V@2%V.,GRW&lt;'FC#U2.43ZM&gt;G.M98.T!!:%45UA;7Y!!!^!"Q!*1G&amp;V:#"3982F!!V!"A!'5'&amp;S;82Z!!!01!9!#52B&gt;'%A1GFU=Q"K!0%!!!!!!!!!!QV%45V@2%V.,GRW&lt;'FC#U2.43ZM&gt;G.M98.T)6.F=GFB&lt;%.P&lt;G:J:X6S982J&lt;WYA,3VD&lt;(6T&gt;'6S,G.U&lt;!!G1&amp;!!!Q!"!!)!!R24:8*J97QA1W^O:GFH&gt;8*B&gt;'FP&lt;A!!"!!!!#R!=!!?!!!&lt;$52.46^%45UO&lt;(:M;7),2%V.,GRW9WRB=X-!"U2.43"P&gt;81!&amp;%!Q`````QJ*&lt;H.U=H6N:7ZU!!!/1#%)351A586F=HE!!!J!)163:8.F&gt;!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!A1&amp;!!!Q!+!!M!$".F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!":!5!!$!!I!#Q!-#76S=G^S)'^V&gt;!"M!0!!%!!!!!1!"1!&amp;!!9!"Q!&amp;!!A!"1!*!!5!$1!&amp;!!5!"1!/!A!"#!!!E!!!!!A!!!!!!!!!!!!!!)U!!!)1!!!!!!!!!!A!!!!!!!!!#!!!!!!!!!!)!!!!!!!!!!!!!!!!!!!!$1M!!!!"!!]!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.MustOverride" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350574592</Property>
	</Item>
	<Item Name="NameAndVersionNumber.vi" Type="VI" URL="../NameAndVersionNumber.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;,!!!!#A!M1(!!(A!!'QV%45V@2%V.,GRW&lt;'FC#U2.43ZM&gt;G.M98.T!!:%45UA;7Y!!!1!!!!M1(!!(A!!'QV%45V@2%V.,GRW&lt;'FC#U2.43ZM&gt;G.M98.T!!&gt;%45UA&lt;X6U!"Z!-0````]54G&amp;N:5&amp;O:&amp;:F=H.J&lt;WZ/&gt;7VC:8)!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!#"!5!!$!!1!"1!'%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;E"1!!-!"!!&amp;!!9*:8*S&lt;X)A&lt;X6U!'Q!]!!1!!!!!1!"!!%!!A!"!!%!!1!$!!%!!1!(!!%!!1!"!!A$!!%)!!#3!!!!!!!!!!!!!!!!!!!!D1!!!!!!!!!!!!!!!!!!!!E!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!!!!!!!.#Q!!!!%!#1!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">33554432</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.MustCallParent" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MustOverride" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342972432</Property>
	</Item>
	<Item Name="ConfigureMultiPoint.vi" Type="VI" URL="../ConfigureMultiPoint.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(S!!!!$A!M1(!!(A!!'QV%45V@2%V.,GRW&lt;'FC#U2.43ZM&gt;G.M98.T!!:%45UA;7Y!!"F!!Q!35(*F&gt;(*J:W&gt;F=C"497VQ&lt;'6T!!"R!0%!!!!!!!!!!QV%45V@2%V.,GRW&lt;'FC#U2.43ZM&gt;G.M98.T&amp;V.B&lt;8"M:6.P&gt;8*D:3!N,76O&gt;7UO9X2M!$&gt;!&amp;A!#$62S;7&gt;H:8)A2'6M98E05W&amp;N='RF)%FO&gt;'6S&gt;G&amp;M!!V497VQ&lt;'5A5W^V=G.F!!1!!!!M1(!!(A!!'QV%45V@2%V.,GRW&lt;'FC#U2.43ZM&gt;G.M98.T!!&gt;%45UA&lt;X6U!".!!Q!.6(*J:W&gt;F=C"$&lt;X6O&gt;!!41!-!$&amp;.B&lt;8"M:3"$&lt;X6O&gt;!!!$%!B"H.U982V=Q!!#U!$!!2D&lt;W2F!!!11$$`````"H.P&gt;8*D:1!!)%"1!!-!"Q!)!!E4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!61!I!$V.B&lt;8"M:3"*&lt;H2F=H:B&lt;!!71&amp;!!!Q!(!!A!#1FF=H*P=C"P&gt;81!&lt;!$Q!"!!!!!"!!)!!Q!%!!5!!Q!'!!-!!Q!$!!I!#Q!$!!-!$!)!!1A!!*!!!!!)!!!!#!!!!!!!!!#.!!!!%!!!!!!!!!!1!!!!!!!!!!!!!!!!!!!!#!!!!"!!!!!!!!!!!!!!!!U,!!!!!1!.!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350574592</Property>
	</Item>
	<Item Name="ConfigureMeasurement.vi" Type="VI" URL="../ConfigureMeasurement.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*Q!!!!$1!M1(!!(A!!'QV%45V@2%V.,GRW&lt;'FC#U2.43ZM&gt;G.M98.T!!:%45UA;7Y!!!1!!!!M1(!!(A!!'QV%45V@2%V.,GRW&lt;'FC#U2.43ZM&gt;G.M98.T!!&gt;%45UA&lt;X6U!*]!]1!!!!!!!!!$$52.46^%45UO&lt;(:M;7),2%V.,GRW9WRB=X-42H6O9X2J&lt;WYA,3VF&lt;H6N,G.U&lt;!"J1"9!"AJ%1S"7&lt;WRU97&gt;F#E&amp;$)&amp;:P&lt;(2B:W5+2%-A1X6S=G6O&gt;!J"1S"$&gt;8*S:7ZU%T)A,3"8;8*F)&amp;*F=WFT&gt;'&amp;O9W54.#!N)&amp;&gt;J=G5A5G6T;8.U97ZD:1!)2H6O9X2J&lt;WY!!(=!]1!!!!!!!!!$$52.46^%45UO&lt;(:M;7),2%V.,GRW9WRB=X-65G6T&lt;WRV&gt;'FP&lt;C!N,76O&gt;7UO9X2M!$^!&amp;A!&amp;"4-A-3]S"41A-3]S"45A-3]S"49A-3]S"4=A-3]S!"23:8.P&lt;(6U;7^O)'FO)%2J:WFU=Q!!#U!+!!6397ZH:1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!A1&amp;!!!Q!'!!=!#".F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#*!)2V&amp;&lt;G&amp;C&lt;'5A186U&lt;S"397ZH:3!I6$IA27ZB9GRF+1!71&amp;!!!Q!'!!=!#!FF=H*P=C"P&gt;81!&lt;!$Q!"!!!!!"!!%!!1!#!!-!!1!%!!%!"1!"!!E!#A!"!!%!#Q)!!1A!!*!!!!!!!!!!!!!!!!!!!!#.!!!!#!!!!!!!!!!)!!!!!!!!!!A!!!!!!!!!#!!!!!A!!!!!!!!!!!!!!!U,!!!!!1!-!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.MustOverride" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
	</Item>
	<Item Name="ConfigureAutoZero.vi" Type="VI" URL="../ConfigureAutoZero.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%]!!!!#A!M1(!!(A!!'QV%45V@2%V.,GRW&lt;'FC#U2.43ZM&gt;G.M98.T!!:%45UA;7Y!!!1!!!!M1(!!(A!!'QV%45V@2%V.,GRW&lt;'FC#U2.43ZM&gt;G.M98.T!!&gt;%45UA&lt;X6U!!^!"A!)186U&lt;XJF=G]!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!#"!5!!$!!1!"1!'%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;E"1!!-!"!!&amp;!!9*:8*S&lt;X)A&lt;X6U!'Q!]!!1!!!!!1!"!!%!!A!$!!%!!1!"!!%!!1!(!!%!!1!"!!A#!!%)!!#1!!!!!!!!!!!!!!!!!!!!D1!!!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!!!!!!!.#Q!!!!%!#1!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.MustOverride" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
	</Item>
	<Item Name="ConfigureOffsetCompensation.vi" Type="VI" URL="../ConfigureOffsetCompensation.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'&lt;!!!!#A!M1(!!(A!!'QV%45V@2%V.,GRW&lt;'FC#U2.43ZM&gt;G.M98.T!!:%45UA;7Y!!!1!!!!M1(!!(A!!'QV%45V@2%V.,GRW&lt;'FC#U2.43ZM&gt;G.M98.T!!&gt;%45UA&lt;X6U!'Y!]1!!!!!!!!!$$52.46^%45UO&lt;(:M;7),2%V.,GRW9WRB=X-A5W6U4W:G=W6U1W^N='6O=W&amp;U;7^O)#UN:7ZV&lt;3ZD&gt;'Q!+U!7!!)$4W:G!E^O!!!85W6U)%^G:H.F&gt;#"$&lt;WVQ:7ZT982J&lt;WY!$%!B"H.U982V=Q!!#U!$!!2D&lt;W2F!!!11$$`````"H.P&gt;8*D:1!!)%"1!!-!"!!&amp;!!94:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71&amp;!!!Q!%!!5!"AFF=H*P=C"P&gt;81!&lt;!$Q!"!!!!!"!!%!!1!#!!-!!1!"!!%!!1!"!!=!!1!"!!%!#!)!!1A!!*!!!!!!!!!!!!!!!!!!!!#.!!!!%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!!!!!!!U,!!!!!1!*!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.MustOverride" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350574592</Property>
	</Item>
	<Item Name="ReadSinglePoint.vi" Type="VI" URL="../ReadSinglePoint.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;8!!!!#Q!M1(!!(A!!'QV%45V@2%V.,GRW&lt;'FC#U2.43ZM&gt;G.M98.T!!:%45UA;7Y!!!1!!!!M1(!!(A!!'QV%45V@2%V.,GRW&lt;'FC#U2.43ZM&gt;G.M98.T!!&gt;%45UA&lt;X6U!"&amp;!#A!,476B=X6S:7VF&lt;H1!$%!B"H.U982V=Q!!#U!$!!2D&lt;W2F!!!11$$`````"H.P&gt;8*D:1!!)%"1!!-!"!!&amp;!!94:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!:1!-!%UVB?'FN&gt;7UA6'FN:3!I&lt;8.F9SE!&amp;E"1!!-!"!!&amp;!!9*:8*S&lt;X)A&lt;X6U!'Q!]!!1!!!!!1!"!!%!!A!"!!%!!1!$!!%!!1!(!!A!!1!"!!E#!!%)!!#1!!!!!!!!!!!!!!!!!!!!D1!!!!!!!!!!!!!!!!!!!!E!!!!!!!!!!!!!!!A!!!!1!!!!!!!!!!!!!!!.#Q!!!!%!#A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.MustOverride" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
	</Item>
	<Item Name="ReadMultiPoint.vi" Type="VI" URL="../ReadMultiPoint.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;[!!!!$1!M1(!!(A!!'QV%45V@2%V.,GRW&lt;'FC#U2.43ZM&gt;G.M98.T!!:%45UA;7Y!!!1!!!!M1(!!(A!!'QV%45V@2%V.,GRW&lt;'FC#U2.43ZM&gt;G.M98.T!!&gt;%45UA&lt;X6U!"6!!Q!/4H6N9G6S)(2P)&amp;*F971!!!5!#A!!'E"!!!(`````!!1-476B=X6S:7VF&lt;H2T!!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!A1&amp;!!!Q!'!!=!#".F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"F!!Q!447&amp;Y;7VV&lt;3"5;7VF)#BN=W6D+1!71&amp;!!!Q!'!!=!#!FF=H*P=C"P&gt;81!&lt;!$Q!"!!!!!"!!%!!1!#!!-!!1!"!!5!!1!"!!E!#A!"!!%!#Q)!!1A!!*!!!!!!!!!!!!!!!!!!!!#.!!!!#!!!!!!!!!!!!!!!#1!!!!!!!!!!!!!!#!!!!!A!!!!!!!!!!!!!!!U,!!!!!1!-!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.MustOverride" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
	</Item>
	<Item Name="Close.vi" Type="VI" URL="../Close.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%N!!!!#1!M1(!!(A!!'QV%45V@2%V.,GRW&lt;'FC#U2.43ZM&gt;G.M98.T!!:%45UA;7Y!!!1!!!!M1(!!(A!!'QV%45V@2%V.,GRW&lt;'FC#U2.43ZM&gt;G.M98.T!!&gt;%45UA&lt;X6U!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!#"!5!!$!!-!"!!&amp;%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;E"1!!-!!Q!%!!5*:8*S&lt;X)A&lt;X6U!'Q!]!!1!!!!!1!"!!%!!A!"!!%!!1!"!!%!!1!'!!%!!1!"!!=#!!%)!!#1!!!!!!!!!!!!!!!!!!!!D1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!!!!!!!.#Q!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.MustOverride" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
	</Item>
</LVClass>
