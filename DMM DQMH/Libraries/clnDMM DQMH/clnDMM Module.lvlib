﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="16008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">16754080</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16448250</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">11316396</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)N!!!*Q(C=\&gt;8"&lt;2J"&amp;-&lt;RDSC(H#,2151,LQ5;]'&amp;&lt;?#VQX3-&amp;**'_&amp;GC"&amp;FY(%&lt;?=;9(]&gt;`S%5'3(3WR:FG=9T(YT/`.D&gt;FF,IXS20ONSLXT^:\HWDZ(,Q;8@`_Y@04?P*`NP[N0^T]^`:`VH`8?+0MJ(?&gt;^FPPMD?0(@Q5L,AYB7../3&amp;FK7P22ZE2&gt;ZE2&gt;ZE:P=Z#9XO=F.HO2*HO2*HO2*(O2"(O2"(O2"0CKZS%5O=ED&amp;YM6#R;4&amp;"-6A+#L?#E`B+4S&amp;BU-6HM*4?!J0Y7')#E`B+4S&amp;J`"QGAJ0Y3E]B;@Q-.71V+DE?!I0U]NYD-&gt;YD-&gt;Y7&amp;,'9Q"G-4/RG13'4+@Z9$T'9TR]F0%9D`%9D`(1,?-R(O-R(O0BF,%L(JKFEO.B'C7?R*.Y%E`C97IFHM34?"*0YG%Z*:\%ES#3":0*)3AZ+2G1(#3?R--@*:\%EXA34_+B;VSB($OT;*:+DC@Q"*\!%XA#$V-I]!3?Q".Y!A`4+P!%HM!4?!)03SHQ"*\!%U##26F?Q74"C='A)!A]P-&lt;&gt;%O-K?5BC6+FP8P6.K&lt;\:V$?2_O:18X4VR62@*08GKT&gt;6P6HK46"`/46;D6%PIDZZ';AT\S@;E8;A\7E\WE4&lt;UD;U^8,K@RZY0J^V/JVU0"ZV/"SUX__VW_UU4:/WW[UWGYX7[`8V-@#.?HUA0$[8:D\0PR^7]]^@K`H(N*K`0TQ?E\`#`_=X]'T5*^WOQ2\^!;R5/Z]!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">369131520</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="NI_IconEditor" Type="Str">49 52 48 48 56 48 51 52 13 0 0 0 0 1 23 21 76 111 97 100 32 38 32 85 110 108 111 97 100 46 108 118 99 108 97 115 115 0 0 1 0 0 0 0 0 9 0 0 13 39 1 100 1 100 80 84 72 48 0 0 0 4 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 15 13 76 97 121 101 114 46 108 118 99 108 97 115 115 0 0 1 0 0 0 0 0 7 0 0 12 182 0 0 0 0 0 0 0 0 0 0 12 158 0 40 0 0 12 152 0 0 12 0 0 0 0 0 0 32 0 32 0 24 0 0 0 0 0 255 255 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 0 0 0 0 0 0 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 0 0 0 0 0 0 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 0 0 0 0 0 0 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 0 0 0 0 0 0 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 0 0 0 0 0 0 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 0 0 0 0 0 0 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 137 0 30 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 4 70 105 108 108 100 1 0 2 0 0 0 5 99 108 111 110 101 0 0 0 0 0 0 0 0 0 0 0 0 0 250 250 250 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 11 83 109 97 108 108 32 70 111 110 116 115 0 1 9 1 1

</Property>
	<Item Name="Testers" Type="Folder">
		<Item Name="DMM DQMH API Tester.vi" Type="VI" URL="../DMM DQMH API Tester.vi"/>
	</Item>
	<Item Name="Public API" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Arguments" Type="Folder">
			<Item Name="Request" Type="Folder">
				<Item Name="Stop Argument--cluster.ctl" Type="VI" URL="../Stop Argument--cluster.ctl"/>
				<Item Name="Show Panel Argument--cluster.ctl" Type="VI" URL="../Show Panel Argument--cluster.ctl"/>
				<Item Name="Hide Panel Argument--cluster.ctl" Type="VI" URL="../Hide Panel Argument--cluster.ctl"/>
				<Item Name="Show Diagram Argument--cluster.ctl" Type="VI" URL="../Show Diagram Argument--cluster.ctl"/>
				<Item Name="Get Module Execution Status Argument--cluster.ctl" Type="VI" URL="../Get Module Execution Status Argument--cluster.ctl"/>
				<Item Name="Load Class Object Argument--cluster.ctl" Type="VI" URL="../Load Class Object Argument--cluster.ctl"/>
				<Item Name="Initialize Device Argument--cluster.ctl" Type="VI" URL="../Initialize Device Argument--cluster.ctl"/>
				<Item Name="Close Device Argument--cluster.ctl" Type="VI" URL="../Close Device Argument--cluster.ctl"/>
				<Item Name="ConfigureMeasurement Argument--cluster.ctl" Type="VI" URL="../ConfigureMeasurement Argument--cluster.ctl"/>
				<Item Name="Set Auto Zero Argument--cluster.ctl" Type="VI" URL="../Set Auto Zero Argument--cluster.ctl"/>
				<Item Name="Set Offset Compensation Argument--cluster.ctl" Type="VI" URL="../Set Offset Compensation Argument--cluster.ctl"/>
				<Item Name="Configure Trigger Argument--cluster.ctl" Type="VI" URL="../Configure Trigger Argument--cluster.ctl"/>
				<Item Name="Configure Multipoint Reading Argument--cluster.ctl" Type="VI" URL="../Configure Multipoint Reading Argument--cluster.ctl"/>
				<Item Name="Initiate Measurement Argument--cluster.ctl" Type="VI" URL="../Initiate Measurement Argument--cluster.ctl"/>
				<Item Name="Fetch Measurement Argument--cluster.ctl" Type="VI" URL="../Fetch Measurement Argument--cluster.ctl"/>
				<Item Name="Fetch Measurement (Reply Payload)--cluster.ctl" Type="VI" URL="../Fetch Measurement (Reply Payload)--cluster.ctl"/>
				<Item Name="Read Single Point Measurement Argument--cluster.ctl" Type="VI" URL="../Read Single Point Measurement Argument--cluster.ctl"/>
				<Item Name="Read Single Point Measurement (Reply Payload)--cluster.ctl" Type="VI" URL="../Read Single Point Measurement (Reply Payload)--cluster.ctl"/>
				<Item Name="Read Multi Point Measurement Argument--cluster.ctl" Type="VI" URL="../Read Multi Point Measurement Argument--cluster.ctl"/>
				<Item Name="Read Multi Point Measurement (Reply Payload)--cluster.ctl" Type="VI" URL="../Read Multi Point Measurement (Reply Payload)--cluster.ctl"/>
				<Item Name="Get Name &amp; Version Argument--cluster.ctl" Type="VI" URL="../Get Name &amp; Version Argument--cluster.ctl"/>
				<Item Name="Get Name &amp; Version (Reply Payload)--cluster.ctl" Type="VI" URL="../Get Name &amp; Version (Reply Payload)--cluster.ctl"/>
				<Item Name="Read Error Argument--cluster.ctl" Type="VI" URL="../Read Error Argument--cluster.ctl"/>
				<Item Name="Read Error (Reply Payload)--cluster.ctl" Type="VI" URL="../Read Error (Reply Payload)--cluster.ctl"/>
				<Item Name="Clear Error Argument--cluster.ctl" Type="VI" URL="../Clear Error Argument--cluster.ctl"/>
			</Item>
			<Item Name="Broadcast" Type="Folder">
				<Item Name="Did Init Argument--cluster.ctl" Type="VI" URL="../Did Init Argument--cluster.ctl"/>
				<Item Name="Status Updated Argument--cluster.ctl" Type="VI" URL="../Status Updated Argument--cluster.ctl"/>
				<Item Name="Error Reported Argument--cluster.ctl" Type="VI" URL="../Error Reported Argument--cluster.ctl"/>
			</Item>
		</Item>
		<Item Name="Requests" Type="Folder">
			<Item Name="Show Panel.vi" Type="VI" URL="../Show Panel.vi"/>
			<Item Name="Hide Panel.vi" Type="VI" URL="../Hide Panel.vi"/>
			<Item Name="Stop Module.vi" Type="VI" URL="../Stop Module.vi"/>
			<Item Name="Show Diagram.vi" Type="VI" URL="../Show Diagram.vi"/>
			<Item Name="Load Class Object.vi" Type="VI" URL="../Load Class Object.vi"/>
			<Item Name="Initialize Device.vi" Type="VI" URL="../Initialize Device.vi"/>
			<Item Name="Close Device.vi" Type="VI" URL="../Close Device.vi"/>
			<Item Name="ConfigureMeasurement.vi" Type="VI" URL="../ConfigureMeasurement.vi"/>
			<Item Name="Set Auto Zero.vi" Type="VI" URL="../Set Auto Zero.vi"/>
			<Item Name="Set Offset Compensation.vi" Type="VI" URL="../Set Offset Compensation.vi"/>
			<Item Name="Configure Trigger.vi" Type="VI" URL="../Configure Trigger.vi"/>
			<Item Name="Configure Multipoint Reading.vi" Type="VI" URL="../Configure Multipoint Reading.vi"/>
			<Item Name="Initiate Measurement.vi" Type="VI" URL="../Initiate Measurement.vi"/>
			<Item Name="Fetch Measurement.vi" Type="VI" URL="../Fetch Measurement.vi"/>
			<Item Name="Read Single Point Measurement.vi" Type="VI" URL="../Read Single Point Measurement.vi"/>
			<Item Name="Read Multi Point Measurement.vi" Type="VI" URL="../Read Multi Point Measurement.vi"/>
			<Item Name="Get Name &amp; Version.vi" Type="VI" URL="../Get Name &amp; Version.vi"/>
			<Item Name="Read Error.vi" Type="VI" URL="../Read Error.vi"/>
			<Item Name="Clear Error.vi" Type="VI" URL="../Clear Error.vi"/>
		</Item>
		<Item Name="Start Module.vi" Type="VI" URL="../Start Module.vi"/>
		<Item Name="Synchronize Module Events.vi" Type="VI" URL="../Synchronize Module Events.vi"/>
		<Item Name="Obtain Broadcast Events for Registration.vi" Type="VI" URL="../Obtain Broadcast Events for Registration.vi"/>
	</Item>
	<Item Name="Broadcasts" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Broadcast Events--cluster.ctl" Type="VI" URL="../Broadcast Events--cluster.ctl"/>
		<Item Name="Obtain Broadcast Events.vi" Type="VI" URL="../Obtain Broadcast Events.vi"/>
		<Item Name="Destroy Broadcast Events.vi" Type="VI" URL="../Destroy Broadcast Events.vi"/>
		<Item Name="Module Did Init.vi" Type="VI" URL="../Module Did Init.vi"/>
		<Item Name="Status Updated.vi" Type="VI" URL="../Status Updated.vi"/>
		<Item Name="Error Reported.vi" Type="VI" URL="../Error Reported.vi"/>
		<Item Name="Module Did Stop.vi" Type="VI" URL="../Module Did Stop.vi"/>
		<Item Name="Update Module Execution Status.vi" Type="VI" URL="../Update Module Execution Status.vi"/>
	</Item>
	<Item Name="Requests" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Request Events--cluster.ctl" Type="VI" URL="../Request Events--cluster.ctl"/>
		<Item Name="Obtain Request Events.vi" Type="VI" URL="../Obtain Request Events.vi"/>
		<Item Name="Destroy Request Events.vi" Type="VI" URL="../Destroy Request Events.vi"/>
		<Item Name="Get Module Execution Status.vi" Type="VI" URL="../Get Module Execution Status.vi"/>
	</Item>
	<Item Name="Private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Support VI" Type="Folder">
			<Item Name="_Format Command Status Message.vi" Type="VI" URL="../_Format Command Status Message.vi"/>
		</Item>
		<Item Name="Init Module.vi" Type="VI" URL="../Init Module.vi"/>
		<Item Name="Handle Exit.vi" Type="VI" URL="../Handle Exit.vi"/>
		<Item Name="Close Module.vi" Type="VI" URL="../Close Module.vi"/>
		<Item Name="Module Data--cluster.ctl" Type="VI" URL="../Module Data--cluster.ctl"/>
		<Item Name="Module Name--constant.vi" Type="VI" URL="../Module Name--constant.vi"/>
		<Item Name="Module Timeout--constant.vi" Type="VI" URL="../Module Timeout--constant.vi"/>
		<Item Name="Module Not Running--error.vi" Type="VI" URL="../Module Not Running--error.vi"/>
		<Item Name="Module Not Synced--error.vi" Type="VI" URL="../Module Not Synced--error.vi"/>
		<Item Name="Module Not Stopped--error.vi" Type="VI" URL="../Module Not Stopped--error.vi"/>
		<Item Name="Module Running as Singleton--error.vi" Type="VI" URL="../Module Running as Singleton--error.vi"/>
		<Item Name="Module Running as Cloneable--error.vi" Type="VI" URL="../Module Running as Cloneable--error.vi"/>
		<Item Name="Open VI Panel.vi" Type="VI" URL="../Open VI Panel.vi"/>
		<Item Name="Hide VI Panel.vi" Type="VI" URL="../Hide VI Panel.vi"/>
	</Item>
	<Item Name="Module Sync" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Destroy Sync Refnums.vi" Type="VI" URL="../Destroy Sync Refnums.vi"/>
		<Item Name="Get Sync Refnums.vi" Type="VI" URL="../Get Sync Refnums.vi"/>
		<Item Name="Synchronize Caller Events.vi" Type="VI" URL="../Synchronize Caller Events.vi"/>
		<Item Name="Wait on Event Sync.vi" Type="VI" URL="../Wait on Event Sync.vi"/>
		<Item Name="Wait on Module Sync.vi" Type="VI" URL="../Wait on Module Sync.vi"/>
		<Item Name="Wait on Stop Sync.vi" Type="VI" URL="../Wait on Stop Sync.vi"/>
	</Item>
	<Item Name="Multiple Instances" Type="Folder">
		<Item Name="Module Ring" Type="Folder">
			<Item Name="Init Select Module Ring.vi" Type="VI" URL="../Init Select Module Ring.vi"/>
			<Item Name="Update Select Module Ring.vi" Type="VI" URL="../Update Select Module Ring.vi"/>
			<Item Name="Addressed to This Module.vi" Type="VI" URL="../Addressed to This Module.vi"/>
		</Item>
		<Item Name="Is Safe to Destroy Refnums.vi" Type="VI" URL="../Is Safe to Destroy Refnums.vi"/>
		<Item Name="Clone Registration.lvlib" Type="Library" URL="../Clone Registration/Clone Registration.lvlib"/>
		<Item Name="Test Clone Registration API.vi" Type="VI" URL="../Clone Registration/Test Clone Registration API.vi"/>
		<Item Name="Get Module Running State.vi" Type="VI" URL="../Get Module Running State.vi"/>
		<Item Name="Module Running State--enum.ctl" Type="VI" URL="../Module Running State--enum.ctl"/>
	</Item>
	<Item Name="Main.vi" Type="VI" URL="../Main.vi">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
</Library>
