# README #

This repository contains the code developed for the NIWeek 2019 presentation: Simplifying your HAL with LVOOP and DQMH
More details at http://bit.ly/NIWeek2019-DQMH-HAL

### How do I get set up? ###
#### Step 1) Install all dependencies
* All VI dependencies are in the  DQMH HAL Dependencies.vipc file in the repo, right-click on the file and select Apply Configuration.
* The vipc file will install DQMH
* You will need the instrument drivers for Agilent 34401 and NI DMM
  * NI DMM driver available here: http://www.ni.com/en-us/support/downloads/drivers/download.ni-dmm.html
  * Agilent driver available here: http://www.ni.com/downloads/instrument-drivers/


#### Step 2) Build PPLs
* Open <DQMH HAL clone>\HAL Utility Classes\HAL Utility Classes_Level00\HAL Utility Classes_Level00.lvproj
  * Open the build specificaton and verify that the destination folder is <Dev Components> and that the folder <Dev Components> is at the same level as your <DQMH HAL clone>
  * Build HAL Utility.lvlibp
* Open <DQMH HAL clone>\DMM Classes\DMM Classes_Level00\DMM Classes_Level00.lvproj
    * Open the build specificaton and verify that the destination folder is <Dev Components> and that the folder <Dev Components> is at the same level as your <DQMH HAL clone>
	* Build .lvlibp
* Open <DQMH HAL clone>\DMM Classes\DMM Classes_Level01\DMM Classes_Level01.lvproj
    * Open the build specificaton and verify that the destination folder is <Dev Components> and that the folder <Dev Components> is at the same level as your <DQMH HAL clone>
	* Build DMM_Agilent.lvlibp
	* Build DMM_niDMM.lvlibp
	* Build DMM_Simulator.lvlibp
* Open <DQMH HAL clone>\DMM Classes\DMM Classes_Level02
    * Open the build specificaton and verify that the destination folder is <Dev Components> and that the folder <Dev Components> is at the same level as your <DQMH HAL clone>
	* Build DMM_34410A.lvlibp
	* Build DMM_34401A.lvlibp
	* Build DMM_PXI-4072.lvlibp
	
#### Step 3) Demo 1 
* Open <DQMH HAL clone>\DMM Demo 1\DMM Demo 1.lvproj
* Open the Class Hierarchy and observe that all the classes are loaded 

#### Step 4) Demo 2 
* Open <DQMH HAL clone>\DMM Demo 2\DMM Demo 2.lvproj
  * Open the Class Hierarchy and observe that only  the HAL Utility and the DMM classes are loaded
  * Running the Demo 2.vi. Please note that for demonstration purposes the paths are hardcoded and point to C:\Dev Components. If you built your PPLs into another folder, change the paths.
  * As you run and load other devices, observe how they show in the VI Hierarchy.	

### Who do I talk to? ###

* Repo owner or admin
Michael Howard